/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * This module defines generic types for Physical Modeling.
 * (There should be no Festo specific references in here)
 * 
 */

package BeSpaceDIA.model

import BeSpaceDCore._
import BeSpaceDGestalt.core._


object PhysicalModel {

  val core = standardDefinitions; import core._
  
  // ---------------------------------------------------------------------------------- Object Identity
  
  trait PhysicalObject[+I]       extends Component[I] { override def toString = id.toString }
  
  trait Device[+I]               extends PhysicalObject[I]
  trait Part[+I]                 extends Device[I]
  trait Actuator[+I]             extends Device[I] //with FiniteStateAutomata.Input[ID]
  trait Sensor[+I]               extends Device[I] //with FiniteStateAutomata.Input[ID]
  
  trait Material[+I]             extends PhysicalObject[I]
  trait DiscreteMaterial[+I]     extends Material[I]
  trait AnalogMaterial[+I]       extends Material[I] { val unit: String; val quantity: Double; ; override def toString = s"$quantity$unit of {name.toString}" }
  
  // Note: Objects are modeled using the traits above. 
  //       Create your own type to represent the object identities (I).
  //       String is a good choice.
  //       Create a case class for each leaf object type above.
  //       Create value instances for each object in your specific domain.
  //       (See examples in the BeSpaceDFesto project)

  
    
  // ---------------------------------------------------------------------------------- Spatial Variation
  
  trait SpatialVariation extends Invariant with Jsonable
  {
    override def toString =
      {
        val fullName = this.getClass.getName
        val baseName = fullName.substring(fullName.lastIndexOf('.'))
        
        val lastChar = baseName.length - 1
        val secondLastDollar = baseName.lastIndexOf('$', lastChar - 1)
        
        baseName.substring(secondLastDollar + 1, lastChar)
      }
    
    override def toJson = s"""{"type": "${this.toString}"""
  }
  
  

  // ---------------------------------------------------------------------------------- State Types
  
  trait PhysicalState[+S]           extends ComponentState[S] { override def toString = s"$state" }
  
  trait DeviceState[+DS]            extends PhysicalState[DS]
  
  trait PartState[+PS]              extends DeviceState[PS]
  trait ActuatorState[+AS]          extends DeviceState[AS]
  trait SensorState[+SS]            extends DeviceState[SS]
  
  trait MaterialState[+MS]          extends PhysicalState[MS]
  
  trait DiscreteMaterialState[+DMS] extends MaterialState[DMS]
  trait AnalogMaterialState[+AMS]   extends MaterialState[AMS]

  // Note: State can be modeled using BeMaps between the 
  //       domain specific devices/materials and their respective states.
  //       Subclass device and material traits (above)
  //       to use as the keys of the BeMaps.
  //       Subclass state traits (above)
  //       to use as the values.
  //       Create your own types to represent the states (__S).
  //       Using case classes to implemented enumerations optionally with associated values is a good choice.

  type ObjectState[I, S]            = IMPLIES[PhysicalObject[I], PhysicalState[S]]
  
  type FactoryState[I, S]           = BeMap[PhysicalObject[I], PhysicalState[S]]
  type DevicesState[I, DS]          = BeMap[Device[I], DeviceState[DS]]
  type PartsState[I, DS]            = BeMap[Part[I], PartState[DS]]
  type ActuatorsState[I, DS]        = BeMap[Actuator[I], ActuatorState[DS]]
  type SensorsState[I, DS]          = BeMap[Sensor[I], SensorState[DS]]
  type MaterialsState[I, MS]        = BeMap[Material[I], MaterialState[MS]]
  
  // Example of state in time:
  val exampleSnapshot = TimePoint(new java.util.Date()) ==> BeMap()
  
  
  
  // ---------------------------------------------------------------------------------- Events
  
  abstract class PhysicalEvent[+I, S](component: PhysicalObject[I], timepoint: TimePoint[Long], state: PhysicalState[S])
  extends INSTATE[PhysicalObject[I], Long, PhysicalState[S]](component, timepoint, state)
  with BeSpaceDGestalt.core.Event[S]
    {
      override def toString = s"@$timepoint: $component in state $state"
      def name: String = s"${component.id.toString}:${state.state.toString}"
      
      // Gestalt Event
      override def id: String = component.id.toString
      override def timestamp: Long = timepoint.timepoint
    }
  
  abstract class DeviceEvent[+I, S](device: Device[I], timepoint: TimePoint[Long], state: DeviceState[S]) extends PhysicalEvent[I, S](device, timepoint, state) 

  //abstract class PartEvent[+I, S](part: Part[I], timepoint: TimePoint[Long], state: PartState[S]) extends DeviceEvent[I, S](part, timepoint, state) 
  abstract class ActuatorEvent[+I, S](actuator: Actuator[I], timepoint: TimePoint[Long], state: ActuatorState[S]) extends DeviceEvent[I, S](actuator, timepoint, state) 
  abstract class SensorEvent[+I, S](sensor: Sensor[I], timepoint: TimePoint[Long], state: SensorState[S]) extends DeviceEvent[I, S](sensor, timepoint, state) 
  
      
  
  // ---------------------------------------------------------------------------------- Relationships
  
//  abstract class RelationshipType[+R] (val relationshipType: R) extends Value[R](relationshipType)
//  
//  // Note: use edges annotated with a relationship type to represent a relationship proper.
//  // Example: type Relationship[N, R] = EdgeAnnotated[N, RelationshipType[R]]
//
//  class SpatialRelationshipType[+SR]  (override val relationshipType: SR) extends RelationshipType[SR](relationshipType)
//  object SpatialRelationshipType { def apply[SR](rt: SR) = new SpatialRelationshipType[SR](rt) }
//  
//  class TemporalRelationshipType[+TR] (override val relationshipType: TR) extends RelationshipType[TR](relationshipType)
//  object TemporalRelationshipType { def apply[TR](rt: TR) = new TemporalRelationshipType[TR](rt) }
//
//  class SpatioTemporalRelationshipType[+STR] (override val relationshipType: STR) extends SpatialRelationshipType[STR](relationshipType)
//  object SpatioTemporalRelationshipType { def apply[STR](rt: STR) = new SpatioTemporalRelationshipType[STR](rt) }
//
//  // Note: Logical Relationships are modeled in BeSpaceD itself
//  //       (e.g. AND, OR, IMPLIES, XOR etc.)
//  
//  class Relationship[+N, +R](source: N, target: N, annotation: RelationshipType[R]) extends EdgeAnnotated[N, RelationshipType[R]]( source,  target, Some(annotation))
//  class SpatialRelationship[+N, +SR](source: N, target: N, annotation: SpatialRelationshipType[SR]) extends EdgeAnnotated[N, SpatialRelationshipType[SR]](source,  target, Some(annotation))
//  class TemporalRelationship[+N, +TR](source: N, target: N, annotation: TemporalRelationshipType[TR]) extends EdgeAnnotated[N, TemporalRelationshipType[TR]](source,  target, Some(annotation))

  abstract trait Relationship
  abstract trait SpatialRelationship extends Relationship
  abstract trait TemporalRelationship extends Relationship
  abstract trait SpatioTemporalRelationship extends SpatialRelationship with TemporalRelationship


  
  // ---------------------------------------------------------------------------------- Topologies
  
  type Topology[+COM <: Component[Any], +REL <: Relationship] = BeGraphAnnotated[COM, REL]
  type SpatialTopology[+COM <: Component[Any], +SREL <: SpatialRelationship] = BeGraphAnnotated[COM, SREL]
  type TemporalTopology[+COM <: Component[Any], +TREL <: TemporalRelationship] = BeGraphAnnotated[COM, TREL]
  type SpatioTemporalTopology[+COM <: Component[Any], +STREL <: SpatioTemporalRelationship] = BeGraphAnnotated[COM, STREL]

  
  
  
  // ================================================================================== EXAMPLES OF MODEL TYPES
  
  // Note: Topology can be modeled using BeGraphs of the domain specific devices.
  //       You likely will need to annotate the edges and use BeGraphAnnotated.
  //       Subclass a relationship type (below) to use as the annotation type.
  //       Create your own types to represent the kind of relationship (R).
  //       Using case classes to implemented enumerations optionally with associated values is a good choice.
  
  
  
  // ---------------------------------------------------------------------------------- Functional Topology
  
  type FunctionalTopology[+I, +FR] = BeGraphAnnotated[Device[I], FR]
  
  
  
  // ---------------------------------------------------------------------------------- Spatial Topology
    
  type FactorySpatialTopology[+I, +SR] = BeGraphAnnotated[Device[I], SR]
  
  //type SpatialTopology[+I, +SR] = BeGraphAnnotated[Component[I], SR]
  
  type SensorsSpatialTopology[+I, +SR] = BeGraphAnnotated[Sensor[I], SR]

  
  
  // ---------------------------------------------------------------------------------- Stateful Topology
  
  // Note: A Factory with moving parts may lend itself to modeling the topology
  //       using the devices AND and state they are in.
  //       Use an IMPLIES from device to its state as the nodes in the graph.
  
  // DeviceInState types ...
  type DeviceInState[I, DS] = IMPLIES[Device[I], DeviceState[DS]]
  type SensorInState[I, SS] = IMPLIES[Sensor[I], SensorState[SS]]
  
  // Stateful Topology types ...
  type FactoryStatefulTopology[+I, +DS, +R] = BeGraphAnnotated[DeviceInState[I, DS], R]
  type SensorsStatefulTopology[+I, +SS, +R] = BeGraphAnnotated[SensorInState[I, SS], R]
  
  
  
  // ---------------------------------------------------------------------------------- Temporal System
  
  // Note: The behavior of a system can be modeled using BeGraphs of
  //       devices in states annotated with temporal relationships.
  
  type FactorySystem[+I, +TR, +DS] = BeGraphAnnotated[DeviceInState[I, DS], TemporalRelationship]
  
  type SensorsSystem[+I, +TR, +SS] = BeGraphAnnotated[SensorInState[I, SS], TemporalRelationship]


  
  // ---------------------------------------------------------------------------------- Dual Stream System
  
  // Note: The behavior of a system can be modeled using two streams of events:
  //       device events and changes to the topology.
  //       The updated topology could be sent with every event.
  
  // TODO ...

 }
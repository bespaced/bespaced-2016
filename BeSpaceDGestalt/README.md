# Gestalt #

![Gestalt Girl in a Tree](https://bitbucket.org/aicause/gestalt/raw/master/res/images/GestaltGirl.jpg)

_Gestalt_ is a framework written in Scala for the purpose of managing holistic decision making based on the input of real world events which vary in type and location. This has applications in the monitoring of remote complex real world sites and collaborative engineering.
This project was started by the Australia-India Centre for Automation Software Engineering ([AICAUSE](https://www.rmit.edu.au/research/research-institutes-centres-and-groups/multi-partner-collaborations/aicause/)) in August 2015.

### Release status ###
* This project is still in initial development. Its not even an Alpha release yet.
* We expect to have a basic usable framework by the end of 2016.

### How do I get set up? ###

* Download the source. It is an Scala IDE (Eclipse) project.
* You need to configure your Java Platform SDK. We used Java 1.7
* No library configuration is necessary as all are included in the project as jar files.
* Gestalt has dependencies on Jackson, Spray and BeSpaceD
* To test you can run some of the demonstrators which are Scala applications
* There is no deployment procedure as yet.

### Contribution guidelines ###

* If you want to contribute to this project contact the lead engineer to discuss and we may delegate some component to you.
* If you want to write tests, we are using ScalaTest. Place them in a scala class named the same as the class under test with a postfix of "Spec".

### Who do I talk to? ###

* Project Owner: [Jan Olaf Blech](mailto:Jan Olaf Blech<jan.blech@rmit.edu.au>), Research Fellow, RMIT University
* Lead Engineer: [Keith Foster](mailto:Keith Foster<keith.foster@rmit.edu.au>), Research Software Engineer, RMIT University

### Design Philosophy ###
Our design philosophy is to design for tomorrow and implement for today. In order not to over-engineer, generic interfaces which insulate from change are used to limit the cost of implementation to only what is needed today. In order to design for tomorrow those same interfaces and the architecture are designed to be highly reusable and extendable without incurring a high cost implementation today.

### Design Patterns ###
In this project we aim to use certain architectural and design patterns consistently across the code base. The following is a list of the key patterns / styles we aim to achieve and will likely mandate in the future:

1. Functional programming.
1. Immutable data structures.
1. Highly generic and reusable components.
1. Composable using functional combinators.

### Motivations and Requirements ###

The motivations for this project are to facilitate the infrastructure surrounding intelligent decision making components written in BeSpaceD (a model invariant solver using temporal and spacial logic).

The architectural requirements for this infrastructure is as follows :

1. Receive event data from various sources (local and remote) including serial ports and network services.
1. Insulation from change with the communications technology and protocols used for receiving events.
1. Transform the event data into a common format.
1. Provide serialisation and deserialisation services for transmission of events over networks.
1. Insulation from change to serialisation formats.
1. Provide routing and cloning services for the events to be processed by different decision making components (currently local only).
1. A flexible component architecture that allows for experimentation with the composition of these components such as direct linking, piping and/or publish and subscribe.
1. Provide a mechanism to transform the output of the components into various data formats (currently XML).
1. Insulation from change with the output data formats.
1. A test harness for simulating real world events aimed at verifying the decision making intelligence works well.
1. Allow for a future type system for “devices” that emit events. Typing could be based on similarity, frequency or temporal/spacial patterns of the events emitted.

The specific requirements for the inaugural version were as follows :

1. The ability to receive events via UDP.
1. The ability to implement decision makers (also known as “event specific code”) using BeSpaceD and Scala.
1. The ability to “output” XML documents representing the decision made.


### Naming and Metaphors ###

The name “Gestalt” was chosen because it makes a good metaphor to use to explain and name the various elements designed into the framework.


There are a few meanings of the word Gestalt including :

1. Literally in German, form or shape (as perceived from the composition of many parts).

1. In psychology, Gestalt Psychology refers to the ability of the human mind to perceive relevant information that is different from the sum of the parts of information being received (not greater as it is often mis-understood). Key terms are stimulus which refers to the physical signals from the real world and gestalt which refers to a piece of information that is perceived but not merely a result of addition or aggregation, but rater intelligent perception or pattern recognition.

1. In science fiction, Gestalt Intelligence, refers to the collective consciousness of many beings combining to form one mind (the minds are equal and not controlled by one external mind).


Also used by the framework is the word “Holism” (which is a form of anti-reductionism) from the greek meaning to look at information or a problem in its entirety rather than analysis through breaking it down into its substituent parts. This is closely associated with the three meanings of Gestalt above.

We make use of these words and meanings in constructing the metaphors used as the basis for key concepts and components of the framework.

### License ###

This software is licensed under the Apache 2 license, quoted below.

Copyright 2015 [AICAUSE](https://www.rmit.edu.au/research/research-institutes-centres-and-groups/multi-partner-collaborations/aicause/), RMIT University 

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
package BeSpaceDGestalt.demonstrator.support.crave

import BeSpaceDGestalt.core._

import BeSpaceDGestalt.crave.sites.rmit._

import BeSpaceDGestalt.demonstrator.support.geo.Locations._

/**
 * Created by keith on 14/09/15.
 */


object CommandSample {

  def GovLabEarth(geo: GeoLocation): GovLabEarth = { GovLabEarth(geo._1, geo._2, geo._3) }


  // ------------------------------------ Sample Commands for Testing
  // Profiles
  val GovLabMachine1 = GovLabDisplay(profile = "machineprofile1")
  val MiniWallAlice = MiniWallDisplay(profile = "alice")
  val GovLabAlice = GovLabDisplay(profile = "alice")
  val MiniWallCharles = MiniWallDisplay(profile = "charles")

  // Google Earth Commands
  val GovLabHazel = GovLabEarth(HazelPowerPlant)



  // Social Media Commands
  // TODO
  //       "<command device='socialmedia' type='tweeter|web' msg='node1'></command>" +

  // INCRC
  // TODO
  //       "<command device='incrc' type='event' catagory='correctcoverage' id='1001'></command>" +
  //       s"<node1> $eventInfo </node1>"


  // Code from CRAVE (for reference only)
  //
  //    val wallTest = "<output>" +
  //      // modifying output data in lieu of jar file modified test case
  //      //       "<command device='govlab' type='display' profile='machineprofile1'></command>" +
  //      "<command device='miniwall' type='display' profile='alice'></command>" +
  //      s"$entries" +
  //      //       "<command device='miniwall' type='display' profile='charles'></command>" +
  //      //       "<command device='incrc' type='event' catagory='correctcoverage' id='1001'></command>" +
  //      //       "<command device='auabb' type='view' image='substation9.jpg' rectx='300' recty='500' rectw='150' recth='100' text='hello world' txtx='100' txty='50'></command>" +
  //      //       "<command device='riotinto-pilbara' type='map' lat='-37.800114' long='144.9671113' zoom='15z'></command>" +
  //      //       "<command device='riotinto-mel' type='earth' lat='-37.800114' long='144.9671113' height='100m'></command>" +
  //      //       "<command device='socialmedia' type='tweeter|web' msg='node1'></command>" +
  //      //       s"<node1> $eventInfo </node1>" +
  //      "</output>"
  //
  //
  //    // Commands
  //    val aliceLogin = "<command device='govlab' type='display' profile='alice'></command>"
  //    val hazelPowerPlantMap = "<command device='govlab' type='earth' lat='-37.800114' long='144.9671113' height='100m'></command>"


}

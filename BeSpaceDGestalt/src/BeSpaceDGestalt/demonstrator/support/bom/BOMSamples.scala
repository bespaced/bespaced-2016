package BeSpaceDGestalt.demonstrator.support.bom

import BeSpaceDGestalt.core.Rectangle

/**
 * Created by keith on 7/09/15.
 */
object BOMSamples {

  val CroppingNiceCloudedRegionOn26Aug: Rectangle = Rectangle(150, 380, 100, 50)
  val CroppingNiceCloudedRegionOn5Sep: Rectangle = Rectangle(300, 180, 100, 50)

}

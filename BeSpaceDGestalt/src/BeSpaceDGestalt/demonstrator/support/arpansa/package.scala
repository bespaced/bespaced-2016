package BeSpaceDGestalt.demonstrator.support

/**
 * Created by keith on 12/10/15.
 */

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone}

import BeSpaceDGestalt.core._
import BeSpaceDGestalt.core.net._
import BeSpaceDGestalt.demonstrator.support.arpansa.UVMonitor.UVCity

import scala.xml.{Elem, Node}


package object arpansa {

  // Constants
  val ARPANSA_SERVER                         = "www.arpansa.gov.au"
  val ARPANSA_UVINDEX_REALTINE_XML_DIRECTORY = "uvindex/realtime/xml"
  val ARPANSA_UVINDEX_ALL                    = "uvvalues.xml"

  val LOCAL_DIRECTORY_ARPANSA                = "arpansa"
  val LOCAL_FILENAME_PREFIX_UV               = "UV"
  val LOCAL_EXT_UV                           = "xml"

  val FORMAT_DATETIME_ARPANSA                = "dd MMMM YYYY hh:mmaa"
  val FORMATTER_DATETIME_ARPANSA             = new SimpleDateFormat(FORMAT_DATETIME_ARPANSA)
  FORMATTER_DATETIME_ARPANSA.setTimeZone(TimeZone.getDefault)
  val FORMAT_DATETIME_UV_LOCAL_FILENAME      = "dd-MMMM-YYYY-hh-mmaa"
  val FORMATTER_DATETIME_UV_LOCAL_FILENAME   = new SimpleDateFormat(FORMAT_DATETIME_UV_LOCAL_FILENAME)
  FORMATTER_DATETIME_UV_LOCAL_FILENAME.setTimeZone(TimeZone.getDefault)


  // Functions
  def createLocalFilenameArpansa(cityId: String, timestamp: String): String =
  {
    createLocalFileName(LOCAL_DIRECTORY_ARPANSA, s"$LOCAL_FILENAME_PREFIX_UV-$cityId-$timestamp.$LOCAL_EXT_UV")
  }


  // Queries
  def latestUvIndex(now: Date)(cityId: String): Double = { latestUvData(now)(cityId).index }

  def latestUvData(now: Date)(cityId: String): UVCity =
  {
    val xml: Elem = if (playback)
    {
      val timestamp = FORMATTER_DATETIME_UV_LOCAL_FILENAME.format(now)
      val uvLocalFileName = createLocalFilenameArpansa(cityId, timestamp)

      readLocalFileXml(uvLocalFileName)
    }
    else
    {
      val uvHttpUrl = createHttpUrl(ARPANSA_SERVER, ARPANSA_UVINDEX_REALTINE_XML_DIRECTORY)(ARPANSA_UVINDEX_ALL)

      val xml = httpPullXml(uvHttpUrl)

      xml
    }
    
    parseUvXml(xml, cityId, record = true)
  }

  
  
  def parseUvXml(xml: scala.xml.Elem, cityId: String, record: Boolean = false): UVCity = {
    val locations: Seq[Node] = xml(0) \ "location"

    debugOff(s"locations $locations")

    val location: Option[Node] = locations find
      {
        location =>

          debugOff(s"location $location == $cityId ?")

          (location \@ "id") == cityId
      }

    debugOn(s"location = $location")
    
    if (location.nonEmpty)
    {
      val locationXml = location.get
      val uvCity = parseUvElement(locationXml)

      // Record the data (if necessary)
      if (record) recordUvFile(uvCity.date, uvCity.time, cityId, locationXml)
      
      uvCity
    }
    else
    {
      throw new Exception("ARPANSA UV Index Unavailable at this time")
    }
  }

  def parseUvElement(locationXml: scala.xml.Node): UVCity = 
  {
      val id       = locationXml \@ "id"

      val name     = locationXml \ "name"     text
      val index    = locationXml \ "index"    text
      val time     = locationXml \ "time"     text
      val date     = locationXml \ "date"     text
      val fulldate = locationXml \ "fulldate" text
      val status   = locationXml \ "status"   text

      testOff {
        println(s"\n$id")
        println(s"\t    name: $name")
        println(s"\t   index: $index")
        println(s"\t    time: $time")
        println(s"\t    date: $date")
        println(s"\tfulldate: $fulldate")
        println(s"\t  status: $status")
      }

      val city = new UVCity(
        id,
        name,
        if (index == null || index.isEmpty) 0 else Math.round(index.toFloat * 100),
        time,
        date,
        fulldate
      )

      return city
  }

  def recordUvFile(date: String, time: String, cityId: String, locationXml: scala.xml.Node) = 
  { 
    if ( ! playback )
    {
      val timeString = (date + " " + time) replaceAll("st","") replaceAll("nd","") replaceAll("rd","") replaceAll("th","")
      val snapshotDate = FORMATTER_DATETIME_ARPANSA.parse(timeString)
      val timestamp = FORMATTER_DATETIME_UV_LOCAL_FILENAME.format(snapshotDate)

      val uvLocalFileName = createLocalFilenameArpansa(cityId, timestamp)

      saveLocalFileXml(uvLocalFileName, locationXml)
    }
  }
}




/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDGestalt.core

/**
 * Created by keith on 21/09/15.
 */
case class EventType(id: String) {
  override def toString: String = s"Event Type $id"
}

class RefinedEventType[V, +SE](override val id: String, refiner: Refiner[V,SE] = IdentityRefiner) extends EventType(id)
{
  override def toString: String = s"Event Type $id"
}
object RefinedEventType { def apply[V, SE](id: String, refiner: Refiner[V,SE] = IdentityRefiner): RefinedEventType[V, SE] = new RefinedEventType(id, refiner) }


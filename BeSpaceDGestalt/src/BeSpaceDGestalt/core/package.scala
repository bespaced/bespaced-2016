/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDGestalt

import java.util.TimeZone

import scala.util.Either

package object core {

  // TODO Remove down to Union Types - as it is now duplicated in BeSpaceDCore package object.
  
  // Poor Man's Testing
  def testOn(block: => Unit) { block }
  def testOff(block: => Unit) {}
  def testOn(message: String*) { testOn { message foreach { println(_) } } }
  def testOff(message: String*) {}

  // Poor Man's Debugging
  def debugOn(block: => Unit) { block }
  def debugOff(block: => Unit) {}
  def debugOn(message: String*) { debugOn { message foreach { println(_) } } }
  def debugOff(message: String*) {}

  // Poor Man's DbC
  def check(assertion: Boolean, message: => Any) { require(assertion, s"CHECK: $message") }
  def ensure(assertion: Boolean, message: => Any) { require(assertion, s"ENSURE: $message") }

  // Poor Man's Logging
  private def logSeq(messages: Seq[String]) { messages foreach { println(_) } }
  def log(messages: String*) { logSeq(messages) }
  def logException(exception: Exception, messages: String*) {
    println("\n")
    logSeq(messages)
    println(s"Cause: ${exception.getMessage}\n")
  }

  // Union Types
  type or[L, R] = Either[L, R] // Move this to a common module
  implicit def l2Or[L, R](l: L): L or R = Left(l)
  implicit def r2Or[L, R](r: R): L or R = Right(r)

  // Events
  type Refiner[V, +SE] = (Event[V]) => SE
  val IdentityRefiner = { x: Event[Any] => x }

  // Matricies

  type Dim2 = (Int, Int)

  // Helpers

  def withinBounds(coordinate: Dim2, bounds: Dim2): Boolean =
    {
      coordinate._1 >= 0 && coordinate._1 < bounds._1 &&
        coordinate._2 >= 0 && coordinate._2 < bounds._2
    }

  // Geo Location
  type GeoLocation = (Double, Double, Int)

  // Time
  val UTC = TimeZone.getTimeZone("UTC")
  
  import Serialization._
}
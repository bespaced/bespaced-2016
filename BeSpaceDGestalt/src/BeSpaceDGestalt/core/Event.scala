/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDGestalt.core

import java.util.{Calendar, UUID, Date}

import Serialization._

/**
 * Created by keith on 21/09/15.
 */

trait KeyValueAccess[+V]
{
  def apply(key: String) : Option[V]
  def apply(index: Int) : Option[V]
}


trait Event[V] extends KeyValueAccess[V] with Serializable {
  def id: String
  def timestamp: Long
  def eventType: EventType

  // Timestamp
  def timestampDate = new Date(timestamp)
  def timeStampCalendar =
  {
    val calendar = Calendar.getInstance(UTC)
    calendar.setTimeInMillis(timestamp)

    calendar
  }
  
  def serialized: String = serializeToString(this)
      
}

object Event {

  def generateId: String = { UUID.randomUUID.toString }
}

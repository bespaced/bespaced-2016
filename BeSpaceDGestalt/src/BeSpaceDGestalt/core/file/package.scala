package BeSpaceDGestalt.core

import java.io.File

/**
 * Created by keith on 3/12/15.
 */
package object file {


  val LOCAL_DIRECTORY_BASE = "/Users/keith"   // Keith's Laptop

  //val LOCAL_DIRECTORY_BASE = "/home/ubuntu"    // NeCTAR Server

  def createDirectoryOnce(parentDirectory: String, directory: String)
  {
    val fullDirectoryPath: String = s"$LOCAL_DIRECTORY_BASE/$parentDirectory/$directory"

    val directoryFile: File = new File(fullDirectoryPath)

    // If the directory does not exist then create it.
    if ( ! directoryFile.exists )
    {
      debugOn(s"Creating directory: $fullDirectoryPath")
      directoryFile.mkdir
    }
  }

}

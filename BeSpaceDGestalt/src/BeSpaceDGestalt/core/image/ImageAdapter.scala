package BeSpaceDGestalt.core.image

/**
 * @author keith 
 */ 

// IO
import java.io.{File, InputStream}

import BeSpaceDGestalt.core._

// Images
import java.awt.image.BufferedImage



class ImageAdapter[E] (val image: BufferedImage, val pixelConverter: PixelConverter[E]) extends MatrixTrait[E] {

  // Constants
  private val dimensions: Dim2 = (image.getWidth, image.getHeight)


  def size = dimensions


  // -------------------------------------------------------- Apply
  // Mandatory implementation of `apply` in `IndexedSeq`
  def apply(coordinate: Dim2): E =
  {
    if (!withinBounds(coordinate, dimensions)) throw new IndexOutOfBoundsException

    pixelConverter(image.getRGB(coordinate._1, coordinate._2))
  }

}

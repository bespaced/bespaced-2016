/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDGestalt.core.net

import java.io.FileNotFoundException
import java.io.IOException
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.util.concurrent.TimeoutException

//import org.slf4j.Logger
//import org.slf4j.LoggerFactory

import scala.collection.mutable.Map

import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.AMQP
import com.rabbitmq.client.Channel

import BeSpaceDCore._
import BeSpaceDGestalt.core._



package object amqp {

  //type Channel = com.rabbitmq.client.Channel

  // Defaults
  val DEFAULT_EXCHANGE_NAME_TOPIC  = "amq.topic"
  val DEFAULT_CHANNEL_DISPLAY_NAME = "default"
  
  
  // ============================================================= Brokers
  case class AmqpBroker(host: String, port: Integer,
                       sslProtocol: String, virtualHost: String,
                       userName: String, userPassword: String)
  {
    override def toString: String = s"$host:$port"
    
    def defaultExchangeTopic = AmqpExchange(this)
    
    
    
    // ----------------------------------------------------------- Connecting
     def close: Boolean = 
     {
      if (connected)
      {
		    try {
			    _connection.get.close
		    } 
 		   catch {
	 	     case e: IOException =>
		      {
			      e.printStackTrace()
			      false
		      }
		      case e: TimeoutException => 
 		     {
	 		     e.printStackTrace()
		 	     false
		      }
		    }
 		   true
      }
      else true
	  }
	
	
	
	
    // ----------------------------------------------------------- Connection
	  def connected = _connection.isDefined
	  
    private var _connection: Option[Connection] = None
    def connection: Option[Connection] =
    {
      if (_connection.isDefined)
        return _connection 
      else 
        connect
        return _connection
    }
    
    private
    def connect: Unit =
    {
      if (factory.isDefined)
      {
	      try
	      {
			    val candidateConnection = factory.get.newConnection()
			    _connection = Some(candidateConnection)
		    }
	      catch 
	      {
	        case e: IOException =>
	          {
              e.printStackTrace();
			        _connection =  None;
		        }
	        case e: TimeoutException =>
	          {
              e.printStackTrace();
			        _connection =  None;
		        }
	      }
      }
    }
    
    // ----------------------------------------------------------- Factory
    private
    var _factory: Option[ConnectionFactory] = None
    private
    def factory: Option[ConnectionFactory] =
    {
      if (_factory.isDefined)
        return _factory 
      else 
        makeFactory
        return _factory
    }
    
    private
    def makeFactory: Unit =
    {
      	val candidateFactory = new ConnectionFactory()
		    candidateFactory.setHost(host)
		    candidateFactory.setPort(port)
		    candidateFactory.setVirtualHost(virtualHost)
		    candidateFactory.setUsername(userName)
        candidateFactory.setPassword(userPassword)
		
		    try
		    {
			    candidateFactory.useSslProtocol(sslProtocol)
			    _factory = Some(candidateFactory)
		    }
		    catch 
		    {
		    case e: KeyManagementException =>
		    case e: NoSuchAlgorithmException => 
		      {
		        e.printStackTrace()
			      _factory = None
		      }
		    }

    }
  }
  
  
  
  // ============================================================= Publishers
  abstract class Publisher
  {
    def publishTopology[V](toplology: String)
    def publishEvent[V](event: BeSpaceDGestalt.core.Event[V])
  }
  
  
  
  // ============================================================= Exchanges
  case class AmqpExchange(broker: AmqpBroker, name: String = DEFAULT_EXCHANGE_NAME_TOPIC)
  {
    var channelMap: Map[String, Channel] = Map[String, Channel]()
    
    override def toString: String = s"$broker/$name"
    
    val defaultChannel = AmqpChannel(this)(None)
    
    def channel(name: String) = AmqpChannel(this)(Some(name))
    
    def addChannel(channelName: String): Channel =
    {
      val channel = broker.connection.get.createChannel
      require (channel != null)
            
      channelMap(channelName) = channel
      
      return channel
    }
    
    def closeChannle(name: String) =
    {
      val c = channelMap(name)
      
      c.close
        
      channelMap.remove(name)
    }
    
    def closeAllChannels = 
    {
      for (c <- channelMap.values)  c.close
      
      channelMap.clear
    }
  }
    
  
  
  // ============================================================= Channels
  
  val plainText = (new AMQP.BasicProperties.Builder).contentType("text/plain").build()
	                  
  case class AmqpChannel(exchange: AmqpExchange)(name: Option[String])
  {
    override def toString: String = s"$exchange/${if (name.isDefined) name.get else DEFAULT_CHANNEL_DISPLAY_NAME}"
    
    def topic(topicName: String) =
      {
        val EXCHANGE_TYPE = "topic"
        
        openChannel
			  channel.get.exchangeDeclare(exchange.name, EXCHANGE_TYPE, true)

        AmqpTopic(this)(topicName)
      }
        
    
    // PRIVATE Implementation
    private
    var channel: Option[Channel] = None
    
    private
    def openChannel: Unit =
      {
        val connection = exchange.broker.connection

        require (connection.isDefined)
        //if (connection.isEmpty) return

        try
	        {
	          val channelName: String = if(name.isDefined) name.get else DEFAULT_CHANNEL_DISPLAY_NAME
	          
	          channel = Some(exchange.addChannel(channelName))
	          println(s"channel = $channel")
	        }
	        catch 
	        {
	          case e: IOException =>
	            {
                e.printStackTrace
			          channel = None
		          }
	          case e: TimeoutException =>
	            {
                e.printStackTrace
			          channel = None
		          }
	        }
      }
    
    def send(message: String, topic: String) =
    {
      try 
        {	        
        channel.get.basicPublish(exchange.name, topic, plainText, message.getBytes)
	        
	      debugOn (s"AmqpTopic::send - [$name]: '$message'")
	        
	      }
      catch 
        { 
          case e: FileNotFoundException => e.printStackTrace
          case e: IOException => e.printStackTrace
	      }

    }
  
  }
  
  // ============================================================= Topics
  
  case class AmqpTopic(channel: AmqpChannel)(name: String) extends Publisher
  {
  //val core = standardDefinitions; import core._
    
    override def toString: String = s"$channel/$name"
    
   // channel.channel.exchangeDeclare(channel.exchange.name.get, "topic", true)

    
    override
    def publishEvent[V](event: BeSpaceDGestalt.core.Event[V]) =
    {
      // Create a connection (if needed)
      val connection = channel.exchange.broker.connection
      //val message = event.serialized
      
      //val x = new CoreDefinitions
      val message = toJson(event)
      
      send(message)
    }    
  
    override
    def publishTopology[V](topology: String) =
    {
      // Create a connection (if needed)
      val connection = channel.exchange.broker.connection
      //val message = topology.serialized
      
      send(topology)
    }    
  
    private
    def send(message: String) =
	  {
		  channel.send(message, topic = name)
	  }
  
  }
  
  
  // TESTING
  def main()
  {
    
  }
  
}
  
  
  
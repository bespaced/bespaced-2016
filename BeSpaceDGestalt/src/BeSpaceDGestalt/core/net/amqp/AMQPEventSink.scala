/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDGestalt.core.net.amqp

import BeSpaceDGestalt.core._
import BeSpaceDGestalt.core.net._


class AMQPEventSink[V <: Serializable]() {
  
  val put: EventSink[V] = 
    (event: Event[V]) =>
      {
        // TODO: ...
      }
}
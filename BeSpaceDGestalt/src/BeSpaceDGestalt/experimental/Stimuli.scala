package BeSpaceDGestalt.experimental

import java.util.{UUID, Date}
import scala.collection.immutable.HashMap

/**
 * Created by keith on 5/08/15.
 */
object Stimuli {

  type KVTree[V] = Map[String, V]
  type HashKVTree[V] = HashMap[String, V]

  def generateId(): String = UUID.randomUUID.toString

  class Stimulus[V](val id: String, val birth: Date, val base: KVTree[V]) extends KVTree[V] {

    // CONVENIENCE CONSTRUCTORS
    private def this(base: KVTree[V]) { this(generateId(), new Date(), base) }


    // INTERFACE MAP
    override def +[B1 >: V](kv: (String, B1)): Stimulus[B1] = { Stimulus[B1](this) + kv }

    override def get(key: String): Option[V] = { base.get(key) }

    override def iterator: Iterator[(String, V)] = { base.iterator }

    override def -(key: String): Stimulus[V] = { Stimulus[V](this) - key }
  }

  object Stimulus {

    // CONVENIENCE CONSTRUCTORS

    // Stimulus based on a map
    def apply[V](base: KVTree[V]): Stimulus[V] = { new Stimulus[V](base) }

    // Empty Stimulus
    def apply[V](): Stimulus[V] = { new Stimulus[V](new HashMap[String, V]) }

  }

}

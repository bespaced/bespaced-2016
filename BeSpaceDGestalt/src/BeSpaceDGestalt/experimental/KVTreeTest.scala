
package BeSpaceDGestalt.experimental

import KVTree._

/**
 * Created by keith on 16/08/15.
 */
object KVTreeTest {

  private val aMap: KVMap[Int] =
    Map (
      "x" -> Value(1),
      "y" -> Value(2)
    )

  val aTree: KVTree[Int] = new KVTree[Int](aMap)

  def main(args: Array[String]) {

    println("Hello World!")
    println(aTree.toString(0))
  }
}

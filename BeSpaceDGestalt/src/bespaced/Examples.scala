/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package bespaced

/**
 * Created by keith on 7/09/15.
 */

import java.util.Date

import BeSpaceDCore._

import scala.collection.parallel.mutable


// BeSpaceD CODE EXAMPLES
object Examples {
  def bespacedExampleCode() {
    def addToList(tp: Int, x1: Int, y1: Int, restList: List[Invariant]): Invariant = {
      BIGAND(IMPLIES(AND(TimePoint(tp), Owner("Cloud")), OccupyBox(x1, y1, 10, 20)) :: restList)
    }

    //Specify the cloud movement
    BIGAND(
      IMPLIES(AND(TimePoint(1), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(2), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(3), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(4), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(5), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::

        IMPLIES(AND(TimePoint(2), Owner("Cloud")), OccupyBox(10, 20, 100, 220)) :: Nil)

        val rainRateFacts =
            IMPLIES (TimePoint(1),
            BIGAND(
              IMPLIES(Owner("RainRate +"),BIGAND(
                  OccupyPoint(10,9)::
                  OccupyPoint(10,10)::Nil
                ) 
              )::
              IMPLIES(Owner("RainRate -"),BIGAND(
                  OccupyPoint(10,11)::Nil
                ) 
              )::             
            Nil)
            )
            
    BIGAND(
      IMPLIES(AND(TimePoint(1), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(2), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(3), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(4), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(5), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::

        IMPLIES(AND(TimePoint(2), Owner("Cloud")), OccupyBox(10, 20, 100, 220)) :: Nil)

        val rainRateFacts2 =
            IMPLIES (TimePoint(1),
            BIGAND(
              IMPLIES(Owner("RainRate +"),BIGAND(
                  OccupyPoint(10,9)::
                  OccupyPoint(10,10)::Nil
                ) 
              )::
              IMPLIES(Owner("RainRate -"),BIGAND(
                  OccupyPoint(10,11)::Nil
                ) 
              )::             
            Nil)
            )
            
    BIGAND(
      IMPLIES(AND(TimePoint(1), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(2), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(3), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(4), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::
        IMPLIES(AND(TimePoint(5), Owner("Cloud")), OccupyBox(10, 20, 100, 200)) ::

        IMPLIES(AND(TimePoint(2), Owner("Cloud")), OccupyBox(10, 20, 100, 220)) :: Nil)

        val rainRateFacts3 =
            IMPLIES (TimePoint(1),
            BIGAND(
              IMPLIES(Owner("RainRate +"),BIGAND(
                  OccupyPoint(10,9)::
                  OccupyPoint(10,10)::Nil
                ) 
              )::
              IMPLIES(Owner("RainRate -"),BIGAND(
                  OccupyPoint(10,11)::Nil
                ) 
              )::             
            Nil)
            )
            
            // Query
            // Use regular Scala code to "query" the BeSpaceD fact base.
            //
            // Later we may use a real SMT solver to export the fact base to.
            // e.g. Z3
            //
            
    val BSC = new BeSpaceDCore.CoreDefinitions()
    BSC.collisionTestsBig(OccupyBox(10, 20, 100, 220) :: Nil, OccupyBox(10, 20, 100, 220) :: Nil)

    //var inv : Invariant = TRUE()

    (inv: Invariant) =>
      inv match {
        case IMPLIES(_, b) => b
        case _ => FALSE()
      }
      // (IMPLIES (AND(FALSE(),FALSE()),OccupyBox(1,1,1,1)))

      //
      // OccupyBox(1,1,1,1)


      var v: Int = 1

      TimePoint[Double](2.3)

      TimeInterval(10, 20)
      OccupyPoint(10, 23)
      TimePoint(v)
      Edge("PowerStation", "House") // Topological connection
      IMPLIES(Owner("PowerStation"), OccupyBox(23, 24, 30, 32))
      
      return
  }

  object BespacedSerialisation
  {
    private val now: Date = new Date()
    private val SECOND: Int = 1000
    private val MINUTE: Int = 60 * SECOND
    private val baseTime: Long = now.getTime - (1 * 24 * 60 * MINUTE)

    private val implication = IMPLIES(TimePoint(baseTime), AND(OccupyPoint(10, 20), OccupyPoint(23, 56)))
    val exampleData = BIGAND(List(implication, implication, implication))

    val exampleData2 =
      BIGAND(
        List(IMPLIES(AND(TimePoint(baseTime), Owner("Cloud")),
          BIGAND(List(OccupyPoint(10,20),OccupyPoint(23,56)))
        ),
          IMPLIES(
            AND(TimePoint(baseTime), Owner("Cloud")),
            BIGAND(List(OccupyPoint(15,21),OccupyPoint(33,106),OccupyPoint(234,1)))
          ),
          IMPLIES(
            AND(TimePoint(baseTime), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(7.1)
            )
          )
        )
      )

    //var points: List[OccupyPoint] = List()


    val rand = scala.util.Random

    def points(count: Int): List[OccupyPoint] =
    {
      val seq = for (i <- 1 to count) yield
      {
        OccupyPoint(rand.nextInt(511), rand.nextInt(511))
      }

      seq.toList
    }

    val exampleData500 =
      BIGAND(
        List(
          IMPLIES(AND(TimePoint(baseTime), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(4.2)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 6 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 6 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(4.3)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 12 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 12 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(4.4)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 18 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 18 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(4.9)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 24 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 24 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(5.1)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 30 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 30 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(5.2)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 36 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 36 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(5.7)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 42 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 42 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(6.0)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 48 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 48 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(6.5)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 54 * MINUTE), Owner("Cloud")),
            BIGAND(points(500))
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 54 * MINUTE), Owner("UV Index")),
            IMPLIES(
              OccupyBox(0,0,511,511),
              ComponentState(7.3)
            )
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 54 * MINUTE), Owner("Power")),
            OccupyBox(275,356,275,361)
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 54 * MINUTE), Owner("Power")),
            OccupyBox(56,406,61,406)
          ),
          IMPLIES(
            AND(TimePoint(baseTime + 54 * MINUTE), Owner("Power")),
            OccupyBox(456,106,461,106)
          )
        )
      )

    val serialisedData = exampleData.toString
    val serialisedData2 = exampleData2.toString
    val serialisedData500 = exampleData500.toString
    //val serialisedData256K = exampleData256K.toString
  }
}


/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.demonstrator.EStorEd

import java.util.Calendar
import java.util.GregorianCalendar
import BeSpaceDCore._
import BeSpaceDCore.Log._
import BeSpaceDGestalt.core.net.amqp._
import BeSpaceDFesto.model._
import BeSpaceDFesto.model.DeviceDescription._
import BeSpaceDFesto.model.DataMapping._
import BeSpaceDFesto.model.Sensors.CapDispenser._
import BeSpaceDFesto.model.topology.Process



object FestoEStorEdClient 
{
  val core = standardDefinitions; import core._

  def publishTopology()
  {
    val sensorsProcessTopologyJson_Station1: String  = toJson(Process.Static.sensorTopology)
    eStorEdPublisherStatic.publishTopology(sensorsProcessTopologyJson_Station1)
  }

  // ------------------------------------------- Initialisation
  
   private val (eStorEdPublisherDynamic, eStorEdPublisherStatic)  = connectToEStorEd()
      
   
  
  // ------------------------------------------- AMQP Connection
  
   /* 
    * The RabbitMQ management console is enabled for now, at http://118.138.244.110:15672 (user: rabbitmquser, password: rabbitmqpw).
    * This will allow viewing the messages received by the RabbitMQ broker.
    */
   
  private
  def connectToEStorEd(): (AmqpTopic, AmqpTopic) =
  {
    val ESTORED_HOST    = "118.138.244.110"
    val ESTORED_PORT    = 5671
    val ESTORED_SSL     = "tlsv1.2"
    val ESTORED_VIRTUAL = "/e-stored"
    
    val ESTORED_USERNAME = "e-stored-user"
    val ESTORED_PASSWORD = "e-stored-user-pw"
    
    val ESTORED_TOPIC_NAME_STATIC  = "festostructure"
    val ESTORED_TOPIC_NAME_DYNAMIC = "festosensors"
    
    val broker: AmqpBroker = AmqpBroker(ESTORED_HOST, ESTORED_PORT, ESTORED_SSL, ESTORED_VIRTUAL, ESTORED_USERNAME, ESTORED_PASSWORD)
    val channel = broker.defaultExchangeTopic.defaultChannel
    val topicDynamic = channel.topic(ESTORED_TOPIC_NAME_DYNAMIC)
    val topicStatic = channel.topic(ESTORED_TOPIC_NAME_STATIC)
            
    (topicDynamic, topicStatic)
  }

  // =========================================== Testing

  def main(args: Array[String])
  {
    testEStorEdAmqpCommunication()
    testBeSpaceDImporting()
  }
  
  // Test Signal
  val pin1 = 25          // Stack Empty Light Sensor
  val pin2 = 22          // Loader Picked Up Light Sensor
  val pin3 = 23          // Loader Dropped Off Light Sensor
  val hi = "HIGH"
  val lo = "LOW"

  // Test Event
  val d1 = new GregorianCalendar(); d1.add(Calendar.SECOND, 1)
  val t1 = TimePoint(d1.getTime.getTime)      
  val se1 = FestoSensorEvent(StackEjectorRetracted, t1, ObstructedHigh  )

  // ------------------------------------------- Test Importing Festo Events into BeSpaceD

  def testBeSpaceDImporting()
  {
    println("==================== TEST BeSpaceD Importing =================")

    importSignal(pin1, lo)
    importSignal(pin2, hi)
    importSignal(pin2, lo)
    importSignal(pin3, hi)
    importSignal(pin3, lo)
    importSignal(pin1, hi)
  }

  // ------------------------------------------- Test Messaging BeSpaceD to eStorED via AMQP

  def testEStorEdAmqpCommunication()
  {
    println("==================== TEST eStorED AMQP Communication =================")

    // Topology
    publishTopology() 
    
    eStorEdPublisherDynamic.publishEvent(se1)
  }

  // ------------------------------------------- Sessions

   var sessionName: String = "None"
  
    def beginSession(sessionName: String)
    {
      this.sessionName = sessionName
      
      println()
      println()
      println()
      
      println(s"==================== BEGIN AMQP SESSION: $sessionName")
    }
   
   
    def endSession
    {
      println(s"==================== END AMQP SESSION: $sessionName")
      
      println()
      println()
      println()
    }
    


    // ------------------------------------------- Import Signal

    def importSignal(gpioPin: Int, value: String)
    {
      // 1. Convert GPIO Values to BeSpaceD Festo Model Values
      val device = gpioPinToFestoDevice(gpioPin)
      
      debugOn(s"device = $device")
      
      val potentialDescription = findByDevice(device)
      
      if (potentialDescription.isEmpty)
      {
        println(s"FAILURE: Device $device does not have a description")
        return
      }
        
      val deviceDescription = findByDevice(device).get
      
      debugOn(s"deviceDescription = $deviceDescription")
      
      val signal: FestoSignal = gpioStateToFestoSignal(device)(value)
      
      debugOn(s"signal = $signal")
      
      if (deviceDescription.isSensor)
      {
        val sensor: FestoSensor = device.asInstanceOf[FestoSensor]
        
        // 2. Create an ComponentState invariant
        val festoSensorState: FestoSensorState = FestoSensorState(sensor, signal)
        
        debugOn(s"festoSensorState = $festoSensorState")

        
        // 3. Create the event as an INSTATE - This is a BeSpaceD Event.
        val bsdNow = TimePoint()
        val bsdEvent = FestoSensorEvent(sensor, bsdNow, festoSensorState)
                
        debugOn {
          val eStorEdSensorName = bsdDeviceToEStorEdSensorName(sensor)
          println(s"New Signal to import: $bsdEvent")
        }
        
        // 4. Send this signal to the eStorEd AMQP server running in the cloud
        eStorEdPublisherDynamic.publishEvent(bsdEvent)
      }
    }
        
}




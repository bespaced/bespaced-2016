/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.demonstrator

package object multiBottleCapping {
  
  def main(args: Array[String]) = 
  {
    
    // Initial Tests
    
    
    
    // Setup GPIO Listeners
    // TODO...
    
    
    while(true)
    {
      // Probably don't need an event loop as all is handles in lister callbacks.
    }
  }
  
  // TODO: Separate the RaspberryPi GPIO/Listener code from the domain specific Festo code.
  //       into two separate modules. Let the RPi/GPIO module be reused by many student projects.
  
  def onFestoSignal() =
  {
    // TODO...
  }
  
  
  // ================================================================== TESTS
  
  /**
   * This is a reproduction of the original Festo demo using station one only.
   * This is done for testing purposes. Testing the new architecture with the state machine at its core.
   * 
   */
  
  // SHELVED: We will use other tolls for control applications.
}
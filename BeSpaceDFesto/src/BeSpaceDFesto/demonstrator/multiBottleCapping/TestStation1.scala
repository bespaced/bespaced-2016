package BeSpaceDFesto.demonstrator.multiBottleCapping

import BeSpaceDCore._
import BeSpaceDCore.FiniteStateAutomata._
import BeSpaceDCore.FiniteStateAutomata.MealyMachine._

import BeSpaceDFesto.model._
import BeSpaceDFesto.model.Sensors._
import BeSpaceDFesto.model.Events._


/* SHELVED
 * 
 * NOTE: This control automation of Festo using the state machine has been shelved as we are moving to implement control using other tools.
 * 
 * HOWEVER: This may server as a partial example for future use of the machine in control applications.
 */

object TestStation1 {
  
  // Comment out one of the mains below.
  
  // On development PC: Save only
  //def main(args: Array[String]) = transducer.save("TestStation1", eventNameFunction)
  
  // On deployment PC:  Load only
  def main(args: Array[String])
  {
    transducer.load[FestoState, FestoSensorEvent, FestoAction](transducer.inputAlphabet, transducer.actionAlphabet, transducer.states, transducer.initialState, "TestStation1", createStateFunction, createActionFunction, eventNameFunction).get
        
    // --------------------------------------------- Setup listeners to All GPIO pins used by Station 1
    
    val devices = List(
    Sensors.CapDispenser.StackEjectorExtended,
		Sensors.CapDispenser.StackEjectorRetracted,
		Sensors.CapDispenser.WorkpieceGripped,
		Sensors.CapDispenser.LoaderPickedUp,
		Sensors.CapDispenser.LoaderDroppedOff,
		Sensors.CapDispenser.StackEmpty,
		
		Actuators.CapDispenser.StackEjectorExtend,
		Actuators.CapDispenser.VacuumGrip,
		Actuators.CapDispenser.EjectAirPulse,
		Actuators.CapDispenser.LoaderPickup,
		Actuators.CapDispenser.LoaderDropoff
		)
		
		// TODO...

  }
  
  def createStateFunction(stateName: String):   FestoState  = FestoState(stateName)
  def createActionFunction(actionName: String): FestoAction = FestoAction(actionName)
  
  def eventNameFunction(event: FestoSensorEvent): String = event.name
  
  def processEvent(event: FestoSensorEvent)
  {
    transducer.trigger(event)
  }
  
  
  
  // ---------------------------------------------------------------------------- FSM Types ...
  // Sensor States
  private val StackEmptyFestoSensorState: FestoSensorState = FestoSensorState(CapDispenser.StackEmpty, on = true) 
  private val StackFullFestoSensorState:  FestoSensorState = FestoSensorState(CapDispenser.StackEmpty, on = false)
  
  // Machine States
  private val StackEmptyState = FestoState("StackEmpty") 
  private val StackFullState = FestoState("StackFull")
  
  // Inputs {Public}
  val bsdNow = TimePoint()
  
  val StackEmpty = FestoSensorEvent(CapDispenser.StackEmpty, bsdNow, StackEmptyFestoSensorState)
  val StackFull  = FestoSensorEvent(CapDispenser.StackEmpty, bsdNow, StackFullFestoSensorState)
    
  val INPUT_ALPHABET: InputAlphabet[FestoSensorEvent] = Set(StackEmpty, StackFull)
  
  // Outputs (Actions) {Public}
  class FestoAction(override val name: String, override val execute: () => Unit = { () => }) extends Action
  object FestoAction { def apply(name: String, execute: () => Unit = { () => }): FestoAction = new FestoAction(name, execute) }
  object DoNothing extends FestoAction("DoNothing")
  
  val ACTION_ALPHABET: OutputAlphabet[FestoAction] = Set(DoNothing)
  
  // States
  case class FestoState(override val name: String) extends State
  private object TestState extends FestoState("TestState")
  
  private val STATE_SET: Set[FestoState] = Set(TestState)
  
  
  
  // ---------------------------------------------------------------------------- FSM Logic ...
  private val INITIAL_STATE = TestState
  
  // Transition Function
  private
  def transit(currentState: FestoState, event: FestoSensorEvent): FestoState =
  {
    (currentState, event) match
    {
      case (TestState, StackEmpty) => TestState
      case (TestState, StackFull)  => TestState
    }
  }
  
  // Output (Action) Function
  private
  def act(currentState: FestoState, event: FestoSensorEvent): FestoAction =
  {
    (currentState, event) match
    {
      case (TestState, StackEmpty) => DoNothing
      case (TestState, StackFull)  => DoNothing
    }
  }
  
  // Transducer
  private
  var transducer = ActionTransducer[FestoState, FestoSensorEvent, FestoAction](
      INPUT_ALPHABET,
      ACTION_ALPHABET,
      STATE_SET,
      INITIAL_STATE,
      transit,
      act
      )  

}
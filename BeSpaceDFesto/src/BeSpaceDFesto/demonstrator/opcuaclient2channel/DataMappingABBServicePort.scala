/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.demonstrator.opcuaclient2channel

class DataMappingABBServicePort {  
  
 
  // -------------------------------------------------------------------- Festo Names
  
  	val station1 = Map(
		    // Inputs
		    "GPIO 0" -> "stackEjectorExtendedLS",
		    "GPIO 3" -> "stackEjectorRetractedLS",
		    "GPIO 21" -> "workpieceGrippedSensor",
		    "GPIO 22" -> "loaderPickupLS",
		    "GPIO 23" -> "loaderDropoffLS",
		    "GPIO 25" -> "stackEmptySensor",
		    // Outputs
		    // Inversed: High is off, low is on				
		    "GPIO 4" -> "stackEjectorExtendSol",
		    "GPIO 5" -> "vacuumGripperSol",
		    "GPIO 6" -> "ejectionAirPulseSol",
		    "GPIO 26" -> "loaderPickupSol",
		    "GPIO 27" -> "loaderDropoffSol"
  		  )

  val allGpioPins = station1.keySet
  

  // -------------------------------------------------------------------- Festo Mappings
  
  def gpioPinToFestoDevice(gpioPin: String): String =
  {
  	  if (allGpioPins contains gpioPin)
        station1(gpioPin)
      else
      {
        println(s"=========== Unknown GPIO Pin: $gpioPin")
        "GPIO UNKNOWN"
      }
  }
  
  val allDevices = station1.values.toSeq

  
  def gpioStateToFestoValue(festoDevice: String)(state: String): Float =
  {
    if (festoDevice.endsWith("Sol"))
          gpioInputStateToFestoValue(festoDevice)(state)
        else
          gpioOutputStateToFestoValue(festoDevice)(state)
  }
  
  
  def gpioInputStateToFestoValue(festoDevice: String)(state: String): Float =
  {
    (festoDevice, state) match {
      case (_, "HIGH") => 5.5F
      case (_, "LOW") => 5.0F
      case (_, _) =>
        {
          println(s"================== Unknown input device state: $festoDevice=$state")
          0.0F
        }
    }
  }
  
  def gpioOutputStateToFestoValue(festoDevice: String)(state: String): Float =
  {
    (festoDevice, state) match {
      case (_, "HIGH") => 80F
      case (_, "LOW") => 100F
      case (_, _) =>
        {
          println(s"================== Unknown output device state: $festoDevice=$state")
          0.0F
        }
    }
  }
  
  
  
  // -------------------------------------------------------------------- BeSpaceD Mappings
  
  // BeSpaceD Devices
//  val EJECTION_AIR_PULSE_SOL: String = "Ejection Air Pulse SOL"
//  val LOADER_PICKUP_SOL: String = "Loader Pickup SOL"

  // Conversion
 def festoDeviceNameToBsdName(festoDevice: String): String = festoDevice
//  {
//    festoDevice match
//    {
//      case "ejectionAirPulseSol" => EJECTION_AIR_PULSE_SOL  // GPIO_06"
//      case "loaderPickupSol" => LOADER_PICKUP_SOL  // <GPIO 26>
//      case _ => {
//        println(s"=============== Unknown festo device: $festoDevice")
//        festoDevice
//      }
//    }
//  }
    

  
  
  // -------------------------------------------------------------------  OPC Mappings
  
  // OPC Sensors
  //val allOPCSensors = Set("LightSensor", "ArmActuator")
  val allOPCSensors = allDevices
  
  // Conversion
  def bsdDeviceToOpcSensorName(bsdDevice: String): String =
  {
    if (allOPCSensors contains bsdDevice)
    {
      bsdDevice
    }
    else
    {
      println(s"============= Unknown BeSpaceD device: $bsdDevice")
      bsdDevice
    }
  }
    
  
  
  
  // -------------------------------------------------------------------  eStoeEd Mappings
    
  // Conversion
  def bsdDeviceToEStorEdSensorName(bsdDevice: String): String =
  {
    if (allDevices contains bsdDevice)
    {
      bsdDevice
    }
    else
    {
      println(s"============= Unknown BeSpaceD device: $bsdDevice")
      bsdDevice
    }
  }
    
  
  
  
  // ------------------------------------------------------------------- Lagacy (for old festo demo)

  def pinToDevice(pin: String): String =
  {
    pin match {
      case p =>
      {
        println(s"============ Unknown pin: $p")
        p
      }
    }
  }  
  

}
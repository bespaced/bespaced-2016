/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore.Log._

import BeSpaceDIA.model.PhysicalModel._

import Actuators.CapDispenser._
import Sensors.CapDispenser._



object DataMapping {  
  
 
  // -------------------------------------------------------------------- Festo Names
  
  // TODO: Use component description to map GPIOs and get rid of this data duplication.
  
  @Deprecated
	val station1 = Map(
	    // Sensors
	    0  -> StackEjectorExtended,
	    3  -> StackEjectorRetracted,
	    21 -> WorkpieceGripped,
	    22 -> LoaderPickedUp,
	    23 -> LoaderDroppedOff,
	    25 -> StackEmpty,
	    
	    // Actuators
	    // Inversed: High is off, low is on				
	    4  -> StackEjectorExtend,
	    5  -> VacuumGrip,
	    6  -> EjectAirPulse,
	    26 -> LoaderPickup,
	    27 -> LoaderDropoff
	    
		  )

  
  // All
  @Deprecated val allGpioMappings = station1
  
  @Deprecated val allGpioPins = allGpioMappings.keySet
  @Deprecated val allDevices = allGpioMappings.values.toSet
  

  // -------------------------------------------------------------------- Festo Mappings
  
  @Deprecated
  def gpioPinToFestoDevice(gpioPin: Int): FestoDevice =
  {
  	  if (allGpioPins contains gpioPin)
        allGpioMappings(gpioPin)
      else
      {
        println(s"================== Unknown GPIO Pin: $gpioPin")
        throw new RuntimeException(s"Unknown GPIO Pin: $gpioPin")
      }
  }
  
  @Deprecated
  def gpioStateToFestoSignal(festoDevice: FestoDevice)(state: String): FestoSignal =
  {
    // TODO: Use type system to determine type not the name string.
    if (festoDevice.id.endsWith("Sol"))
          gpioInputStateToFestoSignal(festoDevice)(state)
        else
          gpioOutputStateToFestoSignal(festoDevice)(state)
  }
  
  
  @Deprecated
  def gpioInputStateToFestoSignal(festoDevice: FestoDevice)(state: String): FestoSignal =
  {
    // TODO: Use the meta-data in device description to determine which way the device is wired.
    (festoDevice, state) match {
      case (_, "HIGH") => High
      case (_, "LOW") => Low
      case (_, _) =>
        {
          println(s"================== Unknown input device state: $festoDevice=$state")
          throw new RuntimeException(s"Unknown input device state: $festoDevice=$state")
        }
    }
  }
  
  @Deprecated
  def gpioOutputStateToFestoSignal(festoDevice: FestoDevice)(state: String): FestoSignal =
  {
    // TODO: Use the meta-data in device description to determine which way the device is wired.
    (festoDevice, state) match {
      case (_, "HIGH") => High
      case (_, "LOW") => Low
      case (_, _) =>
        {
          println(s"================== Unknown output device state: $festoDevice=$state")
          throw new RuntimeException(s"Unknown output device state: $festoDevice=$state")
        }
    }
  }
  
  
    
  // -------------------------------------------------------------------  eStorEd Mappings
  import BeSpaceDCore._
  // Conversion
  def bsdDeviceToEStorEdSensorName(bsdDevice: FestoDevice ): String =
  //def bsdDeviceToEStorEdSensorName(bsdDevice: FestoSensor): String =
  {
    //if (allDevices.find(_ == bsdDevice).isDefined)
    if (allDevices contains bsdDevice)
    {
      bsdDevice.id
    }
    else
    {
      println(s"============= Unknown BeSpaceD device: $bsdDevice")
      debugOn("allDevices:")
      debugOn(s"$allDevices")
      
      val existential = allDevices map (_ == bsdDevice)
      debugOn("existential:")
      debugOn(s"$existential")
      
      "Unknown Device"
    }
  }
  
}



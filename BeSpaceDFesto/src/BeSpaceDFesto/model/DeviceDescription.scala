/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._
import BeSpaceDCore.Log._

import BeSpaceDIA.model.PhysicalModel._



object DeviceDescription {

  val core = standardDefinitions; import core._

  // Note: This is META DATA about all the devices in the Festo mini factory.
  //       Data is represented as Attribute names and values
  //       We use Component[String] for attribute names and Component[String|Int] for attribute values.

  // ----------------------------- Types
  
  // Device Category
  val DeviceCategory = Component("Festo Device Category")
  val Part           = ComponentValue("Part")
  val Actuator       = ComponentValue("Actuator")
  val Sensor         = ComponentValue("Sensor")
  
  val isPart     = DeviceCategory ==> Part
  val isActuator = DeviceCategory ==> Actuator
  val isSensor   = DeviceCategory ==> Sensor

  // Device Type
  val DeviceType = Component("Festo Device Type")
  
  // Device Type - Parts
  val ConveyerPart         = ComponentValue("Conveyer")
  val RobotArmPart         = ComponentValue("Robot Arm")
  val SuctionLoaderPart    = ComponentValue("Suction Loader")
  val VerticalTubePart     = ComponentValue("Vertical Tube")
  val HorizontalPusherPart = ComponentValue("Horizontal Pusher")
  
  val isConveyer           = DeviceType ==> ConveyerPart
  val isRobotArm           = DeviceType ==> RobotArmPart
  val isSuctionLoader      = DeviceType ==> SuctionLoaderPart
  val isVerticalTube       = DeviceType ==> VerticalTubePart
  val isHorizontalPusher   = DeviceType ==> HorizontalPusherPart
  
  // Device Type - Actuators
  val SolenoidActuator = ComponentValue("Solenoid")
  val PumpActuator     = ComponentValue("Pump")
  
  val isSolenoid = DeviceType ==> SolenoidActuator
  val isPump     = DeviceType ==> PumpActuator

  // Device Type - Sensors
  val LightSensor = ComponentValue("Light Sensor")
  val GripSensor  = ComponentValue("Grip Sensor")
  
  val isLightSensor = DeviceType ==> LightSensor
  val isGripSensor  = DeviceType ==> GripSensor
  
  // Signal Mapping
  // (Actuators and Sensors only)
  val SignalMapping = Component("Signal Mapping")
  type SignalTo[R]           = Map[FestoSignal, R]
  type SignalSensorMapping   = SignalTo[FestoSensorState]
  type SignalActuatorMapping = SignalTo[FestoActuatorState]

  
  
  // ----------------------------- Signal Mapping - Sensors
  
  val HighLightSensorMapping: SignalSensorMapping = Map(High -> ObstructedHigh,   Low -> UnobstructedLow)
  val LowLightSensorMapping:  SignalSensorMapping = Map(High -> UnobstructedHigh, Low -> ObstructedLow)
  val HighGripSensorMapping:  SignalSensorMapping = Map(High -> GrippedHigh,      Low -> LooseLow)
  val LowGripSensorMapping:   SignalSensorMapping = Map(High -> LooseHigh,        Low -> GrippedLow)
  
  val isHighLightSensor = SignalMapping ==> ComponentValue(HighLightSensorMapping)
  val isLowLightSensor  = SignalMapping ==> ComponentValue(LowLightSensorMapping)
  val isHighGripSensor  = SignalMapping ==> ComponentValue(HighGripSensorMapping)
  val isLowGripSensor   = SignalMapping ==> ComponentValue(LowGripSensorMapping)

  
  
  // ----------------------------- Signal Mapping - Actuators
  
  val HighSolenoidActuatorMapping: SignalActuatorMapping = Map(High -> ActiveHigh,  Low -> PassiveLow)
  val LowSolenoidActuatorMapping:  SignalActuatorMapping = Map(High -> PassiveHigh, Low -> ActiveLow)
  val HighGripActuatorMapping:     SignalActuatorMapping = Map(High -> GripHigh,    Low -> ReleaseLow)
  val LowGripActuatorMapping:      SignalActuatorMapping = Map(High -> ReleaseHigh, Low -> GripLow)
  
  val isHighSolenoidActuator = SignalMapping ==> ComponentValue(HighSolenoidActuatorMapping)
  val isLowSolenoidActuator  = SignalMapping ==> ComponentValue(LowSolenoidActuatorMapping)
  val isHighGripActuator     = SignalMapping ==> ComponentValue(HighGripActuatorMapping)
  val isLowGripActuator      = SignalMapping ==> ComponentValue(LowGripActuatorMapping)

  
  
  // ----------------------------- GPIO Pins
  
  val GPIO    = Component("PLC RPi GPIO Pin Allocation")
    
  // GPIO Pins - Actuators
  val GPIO_1  = ComponentValue(1)
  val GPIO_4  = ComponentValue(4)
  val GPIO_5  = ComponentValue(5)
  val GPIO_6  = ComponentValue(6)
  val GPIO_15 = ComponentValue(15)
  val GPIO_16 = ComponentValue(16)
  val GPIO_26 = ComponentValue(26)
  val GPIO_27 = ComponentValue(27)
  
  val isGpio1  = GPIO ==> GPIO_1
  val isGpio4  = GPIO ==> GPIO_4
  val isGpio5  = GPIO ==> GPIO_5
  val isGpio6  = GPIO ==> GPIO_6
  val isGpio15 = GPIO ==> GPIO_15
  val isGpio16 = GPIO ==> GPIO_16
  val isGpio26 = GPIO ==> GPIO_26
  val isGpio27 = GPIO ==> GPIO_27
  
  // GPIO Pins - Sensors
  val GPIO_0  = ComponentValue(0)
  val GPIO_2  = ComponentValue(2)
  val GPIO_3  = ComponentValue(3)
  val GPIO_7  = ComponentValue(7)
  val GPIO_8  = ComponentValue(8)
  val GPIO_9  = ComponentValue(9)
  val GPIO_12 = ComponentValue(12)
  val GPIO_13 = ComponentValue(13)
  val GPIO_21 = ComponentValue(21)
  val GPIO_22 = ComponentValue(22)
  val GPIO_23 = ComponentValue(23)
  val GPIO_25 = ComponentValue(25)
  
  val isGpio0  = GPIO ==> GPIO_0
  val isGpio2  = GPIO ==> GPIO_2
  val isGpio3  = GPIO ==> GPIO_3
  val isGpio7  = GPIO ==> GPIO_7
  val isGpio8  = GPIO ==> GPIO_8
  val isGpio9  = GPIO ==> GPIO_9
  val isGpio12 = GPIO ==> GPIO_12
  val isGpio13 = GPIO ==> GPIO_13
  val isGpio21 = GPIO ==> GPIO_21
  val isGpio22 = GPIO ==> GPIO_22
  val isGpio23 = GPIO ==> GPIO_23
  val isGpio25 = GPIO ==> GPIO_25
  
  
  
  // ----------------------------- Assocoiation to Parts
  
  val PartAssociation = Component("Part Association")
  
  
  
  // ----------------------------- Spatial Variations
  
  val SpatialVariations = Component("Spatial Variations")
  
  
//  object CapDispenser
//  {
//    abstract class LoaderSpacialVariation extends ATOM with SpacialVariation
//    object LoaderPicking extends LoaderSpacialVariation
//    object LoaderDropping extends LoaderSpacialVariation
//  }
  
  // ----------------------------- Geometric Locations
  
  // We will use OccupyBox3D to represent the locations of the devices.
  type SpatialLocationEntry = FestoComponentEntry[String, Occupy3DBox]
  
  val SpatialLocation = Component("Spatial Location")

  /**
   * This convenience function allows one to construct a spatial location entry using a single point and three sizes.
   */
  def occupy(x1: Int, y1: Int, z1: Int, width: Int, depth: Int, height: Int): SpatialLocationEntry = SpatialLocation ==> ComponentValue(Occupy3DBox(x1, y1, z1, x1+width, y1+depth, z1+height))
  
  
  // ----------------------------- Description Types
  
  type FestoComponentEntry[K, V] = IMPLIES[Component[K], ComponentValue[V]]
    
  class FestoComponentDescription(entries: BeMap[Component[Any],ComponentValue[Any]]) extends ComponentDescription[Component[Any],ComponentValue[Any]](entries.terms)
  {
    // TODO: Parts
    
    def isPart:        Boolean = findConclusion(entries, DeviceCategory) == Part
    
    def isActuator:    Boolean = findConclusion(entries, DeviceCategory) == Actuator
    def isSolenoid:    Boolean = findConclusion(entries, DeviceType)     == SolenoidActuator
    def isPump:        Boolean = findConclusion(entries, DeviceType)     == PumpActuator

    def isSensor:      Boolean = findConclusion(entries, DeviceCategory) == Sensor
    def isLightSensor: Boolean = findConclusion(entries, DeviceType)     == LightSensor
    def isGripSensor:  Boolean = findConclusion(entries, DeviceType)     == GripSensor
    
    def gpioPin:       Int     = findConclusion(entries, GPIO).asInstanceOf[ComponentValue[Int]].value
        
    // Mappings to/from Festo Signals/States
    var conclusion: Invariant = null
    
    val signalToActuatorState: SignalTo[FestoActuatorState] = 
      if (!isPart)
      {
        conclusion = findConclusion(entries, SignalMapping)
    
        conclusion match
        {
          case FALSE() =>
            {
              debugOn {
                println(s"conclusion = $conclusion")
                println(SignalMapping)
                println(toJson(entries))
              }
              throw new RuntimeException(s"No signal mapping found")
            }
    
          case c: ComponentValue[SignalTo[FestoActuatorState]] => c.value
          
          case _ =>
            {
              debugOn(s"conclusion = $conclusion")
              throw new RuntimeException(s"Invalid conclusion: ${conclusion.getClass.getName} :: $conclusion")
            }
        }
      }
      else
        null
    
      val signalToSensorState: SignalTo[FestoSensorState] = 
        if (!isPart)
        {
          conclusion match
          {
            case FALSE() =>
              {
                debugOn(s"conclusion = $conclusion")
                throw new RuntimeException(s"No signal mapping found")
              }
      
            case c: ComponentValue[SignalTo[FestoSensorState]] => c.value
            
            case _ =>
              {
                debugOn(s"conclusion = $conclusion")
                throw new RuntimeException(s"Invalid conclusion: ${conclusion.getClass.getName} :: $conclusion")
              }
          }
        }
        else
          null
          
    // Inverse the maps
    val sensorStateToSignal = if (signalToSensorState != null) signalToSensorState map {_.swap} else null
    val actuatorStateToSignal = if (signalToActuatorState != null) signalToActuatorState map {_.swap} else null
    }
    
    
  object FestoComponentDescription 
  {
    //def apply(entries: BeMap[Component[Any],ComponentValue[Any]]): FestoComponentDescription = new FestoComponentDescription(entries)
    //def apply[C <: Component[Any], V <: ComponentValue[Any], E <: IMPLIES[C, V]](entries: E*): FestoComponentDescription = new FestoComponentDescription(BeMap(entries.toList))
    def apply[E <: IMPLIES[Component[Any], ComponentValue[Any]]](entries: E*): FestoComponentDescription = new FestoComponentDescription(BeMap(entries.toList))
  }
  
  
  // ------------------------- Data
  //   -- Device Category
  //   -- Device Type
  //   -- GPIO Pin Allocation
  
  // Unknown
  object UnknownDeviceDescription extends ComponentDescription(Nil)
  
  
  type FestoPartDescriptions     = BeMap[Component[FestoPart],     FestoComponentDescription]
  type FestoActuatorDescriptions = BeMap[Component[FestoActuator], FestoComponentDescription]
  type FestoSensorDescriptions   = BeMap[Component[FestoSensor],   FestoComponentDescription]

  
  
  
  // ===================================================================================== MODEL: All Device Descriptions
  
  // ------------------------------------------------------------------------------------- Station 1: Cap Dispenser
  
  object CapDispenser
  {
    // Measurements
    // (in 1/8 inches)
    object X
    {
      val Station1EdgeLeft         = 0
      
      val TubeLeft                 = Station1EdgeLeft + 68
      
      val StackEjectorLeft         = Station1EdgeLeft + 40
      val StackEjectorRight        = Station1EdgeLeft + 85
      
      val ExtendRetractSensorRight = StackEjectorRight
      val ExtendRetractSensorLeft  = ExtendRetractSensorRight - Width.ExtendRetractSensor
      
      val LoaderLeft               = Station1EdgeLeft + 10
    }
    
    object Y
    {
      val Station1EdgeFront        = 0
        
      val StackEjectorFront        = Station1EdgeFront + 76
      
      val ExtendSensorFront        = StackEjectorFront + 122
      val ExtendSensorBack         = ExtendSensorFront + Depth.ExtendRetractSensor
      
      val RetractSensorFront       = StackEjectorFront + 236
      val RetractSensorBack        = RetractSensorFront + Depth.ExtendRetractSensor
      
      val TubeFront                = StackEjectorFront + 55

      val LoaderFront              = Station1EdgeFront + 115
      val LoaderBack               = Station1EdgeFront + 180
    }
    
    object Z
    {
      val Base                        = 0
      val StackEjectorPlatform        = Base + 34
      
      val TubeBottom                  = StackEjectorPlatform
      val TubeTop                     = Base + 230
        
      val StackEmptyLightSensor       = Base + 236
      val StackFullLightSensor        = Base + 112
      
      val StackEjectorBottom          = Base
      
      val ExtendRetractSensorBottom   = Base + 4
      val ExtendRetractSensorTop      = Base + 20
      
      val LoaderBottom                = StackEjectorPlatform
      val LoaderTop                   = Base + 288
    }
    
    object Diameter
    {
      val Tube            = 60
      val TubeLightSensor = 6
    }
        
    object Width
    {
      val Tube                = Diameter.Tube
      val TubeLightSensor     = 15
      
      val StackEjector        = X.StackEjectorRight - X.StackEjectorLeft
      val ExtendRetractSensor = 32
      
      val Loader              = 230 + 160
    }
    
    object Depth
    {
      val Tube                = Diameter.Tube
      val TubeLightSensor     = Diameter.TubeLightSensor
      
      val StackEjector        = 284
      val ExtendRetractSensor = 10
      
      val Loader              = Y.LoaderBack - Y.LoaderFront
    }
    
    object Height
    {
      val Tube                = Z.TubeTop - Z.TubeBottom
      val TubeLightSensor     = Diameter.TubeLightSensor
      
      val StackEjector        = 58
      val ExtendRetractSensor = Z.ExtendRetractSensorTop - Z.ExtendRetractSensorBottom
      
      val Loader              = Z.LoaderTop - Z.LoaderBottom
    }
    
    
    // Parts
    import Parts.CapDispenser._
    
    // Part Associations
    val forStackEjector        = PartAssociation   ==> ComponentValue(StackEjector)    
    val forLoader              = PartAssociation   ==> ComponentValue(Loader)
    val forTube                = PartAssociation   ==> ComponentValue(Tube)
    
    // Spatial Variations
    val stackEjectorVariations = SpatialVariations ==> ComponentValue(XOR(StackEjectorPositions.terms map { pos => ComponentValue.apply(pos) }))
    val loaderVariations       = SpatialVariations ==> ComponentValue(XOR(LoaderPositions.terms map ComponentValue.apply))
    
    // Spatial Locations
    val tubeOccupy         = occupy(X.TubeLeft,         Y.TubeFront,         Z.TubeBottom,         Width.Tube,         Depth.Tube,         Height.Tube)
    val stackEjectorOccupy = occupy(X.StackEjectorLeft, Y.StackEjectorFront, Z.StackEjectorBottom, Width.StackEjector, Depth.StackEjector, Height.StackEjector)
    val loaderOccupy       = occupy(X.LoaderLeft,       Y.LoaderFront,       Z.LoaderBottom,       Width.Loader,       Depth.Loader,       Height.Loader)
    
    val parts: FestoPartDescriptions = BeMap(
         Component(Tube)                 ==> FestoComponentDescription(isPart, isVerticalTube,                             tubeOccupy) ^
         Component(StackEjector)         ==> FestoComponentDescription(isPart, isHorizontalPusher, stackEjectorVariations, stackEjectorOccupy) ^
         Component(Loader)               ==> FestoComponentDescription(isPart, isSuctionLoader,    loaderVariations,       loaderOccupy)
       )

    
    // Actuators
    import Actuators.CapDispenser._
    
    val actuators: FestoActuatorDescriptions = BeMap(
        Component(StackEjectorExtend)    ==> FestoComponentDescription(isActuator, isSolenoid, isGpio4,  isHighSolenoidActuator, forStackEjector)  ^
        Component(VacuumGrip)            ==> FestoComponentDescription(isActuator, isSolenoid, isGpio5,  isHighSolenoidActuator, forLoader)        ^
        Component(EjectAirPulse)         ==> FestoComponentDescription(isActuator, isSolenoid, isGpio6,  isHighSolenoidActuator, forLoader)        ^
        Component(LoaderPickup)          ==> FestoComponentDescription(isActuator, isSolenoid, isGpio26, isHighSolenoidActuator, forLoader)        ^
        Component(LoaderDropoff)         ==> FestoComponentDescription(isActuator, isSolenoid, isGpio27, isHighSolenoidActuator, forLoader)        )
    
    
    // Sensors
    import Sensors.CapDispenser._
    
    // Spatial Locations
    val stackEjectorExtendedOccupy  = occupy(X.ExtendRetractSensorLeft, Y.ExtendSensorFront,  Z.ExtendRetractSensorBottom, Width.ExtendRetractSensor, Depth.ExtendRetractSensor, Height.ExtendRetractSensor)
    val stackEjectorRetractedOccupy = occupy(X.ExtendRetractSensorLeft, Y.RetractSensorFront, Z.ExtendRetractSensorBottom, Width.ExtendRetractSensor, Depth.ExtendRetractSensor, Height.ExtendRetractSensor)

    // TODO: Finish measuring the Festo sensors of station 1 and enter the values below.
    
    val sensors: FestoSensorDescriptions = BeMap(
        Component(StackEjectorExtended)  ==> FestoComponentDescription(isSensor, isLightSensor, isGpio0,  isHighLightSensor, forStackEjector, stackEjectorExtendedOccupy) ^
        Component(StackEjectorRetracted) ==> FestoComponentDescription(isSensor, isLightSensor, isGpio3,  isHighLightSensor, forStackEjector, stackEjectorRetractedOccupy) ^
        Component(WorkpieceGripped)      ==> FestoComponentDescription(isSensor, isGripSensor,  isGpio21, isHighGripSensor,  forLoader,       occupy(0, 0, 0, 2, 2, 2)) ^
        Component(LoaderPickedUp)        ==> FestoComponentDescription(isSensor, isLightSensor, isGpio22, isHighLightSensor, forLoader,       occupy(0, 0, 0, 2, 2, 2)) ^
        Component(LoaderDroppedOff)      ==> FestoComponentDescription(isSensor, isLightSensor, isGpio23, isHighLightSensor, forLoader,       occupy(0, 0, 0, 2, 2, 2)) ^
        Component(StackEmpty)            ==> FestoComponentDescription(isSensor, isLightSensor, isGpio25, isHighLightSensor, forTube,         occupy(0, 0, 0, 2, 2, 2)) )

    val devices = parts ^ actuators ^ sensors
  }
  
  // ------------------------------------------------------------------------------------- Station 2: Cap Conveyer
  
  object CapConveyer
  {
    import Actuators.CapConveyer._
    
    val actuators: FestoActuatorDescriptions = BeMap(
        Component(ConveyorBeltGate)  ==> FestoComponentDescription(isActuator, isSolenoid, isGpio15, isHighSolenoidActuator) ^
        Component(ConveyorBeltMove)  ==> FestoComponentDescription(isActuator, isSolenoid, isGpio16, isHighSolenoidActuator) )
    
    import Sensors.CapConveyer._
        
    val sensors: FestoSensorDescriptions = BeMap(
        Component(FirstConveyor)     ==> FestoComponentDescription(isSensor, isLightSensor, isGpio8, isHighLightSensor)  ^
        Component(SecondConveyor)    ==> FestoComponentDescription(isSensor, isLightSensor, isGpio9, isHighLightSensor)  ^
        Component(LastConveyor)      ==> FestoComponentDescription(isSensor, isLightSensor, isGpio7, isHighLightSensor)  )
  
    val devices = actuators ^ sensors
  }
  
  // ------------------------------------------------------------------------------------- Station 3: Capping

  object Capping
  {
    import Actuators.Capping._
    
    val actuators: FestoActuatorDescriptions = BeMap(
        Component(MoveToConveyorBelt)    ==> FestoComponentDescription(isActuator, isSolenoid, isGpio15, isHighSolenoidActuator) ^
        Component(MoveFromConveyorBelt)  ==> FestoComponentDescription(isActuator, isSolenoid, isGpio16, isHighSolenoidActuator) ^
        Component(LowerArm)              ==> FestoComponentDescription(isActuator, isSolenoid, isGpio1,  isHighSolenoidActuator)  ^
        Component(GripWorkpiece)         ==> FestoComponentDescription(isActuator, isSolenoid, isGpio4,  isHighSolenoidActuator)  )
    
    import Sensors.Capping._
        
    val sensors: FestoSensorDescriptions = BeMap(
        Component(EndOfConveyor)         ==> FestoComponentDescription(isSensor, isLightSensor, isGpio8,  isHighLightSensor) ^
        Component(ConveyorPosition)      ==> FestoComponentDescription(isSensor, isLightSensor, isGpio9,  isHighLightSensor) ^
        Component(DropoffPosition)       ==> FestoComponentDescription(isSensor, isLightSensor, isGpio7,  isHighLightSensor) ^
        Component(MiddlePosition)        ==> FestoComponentDescription(isSensor, isLightSensor, isGpio0,  isHighLightSensor) ^
        Component(ArmLowered)            ==> FestoComponentDescription(isSensor, isLightSensor, isGpio2,  isHighLightSensor) ^
        Component(ArmRaised)             ==> FestoComponentDescription(isSensor, isLightSensor, isGpio12, isHighLightSensor) )
  
    val devices = actuators ^ sensors
  }

  // ------------------------------------------------------------------------------------- Station 4: Rotary Table Part 1

  object RotaryTablePart1
  {
    import Actuators.RotaryTablePart1._
    
    val actuators: FestoActuatorDescriptions = BeMap(
        Component(MoveSmallConveyor)    ==> FestoComponentDescription(isActuator, isSolenoid, isGpio15, isHighSolenoidActuator) ^
        Component(RotateTable)          ==> FestoComponentDescription(isActuator, isSolenoid, isGpio16, isHighSolenoidActuator)  ^
        Component(MoveLargeConveyor)    ==> FestoComponentDescription(isActuator, isSolenoid, isGpio1,  isHighSolenoidActuator)  )
    
    import Sensors.RotaryTablePart1._
        
    val sensors: FestoSensorDescriptions = BeMap(
        Component(EnteringRotaryTable)  ==> FestoComponentDescription(isSensor, isLightSensor, isGpio7,  isHighLightSensor) ^
        Component(LiquidFiller)         ==> FestoComponentDescription(isSensor, isLightSensor, isGpio2,  isHighLightSensor) ^
        Component(GranularFiller)       ==> FestoComponentDescription(isSensor, isLightSensor, isGpio12, isHighLightSensor) )
  
    val devices = actuators ^ sensors
  }

  // ------------------------------------------------------------------------------------- Station 5: Rotary Table Part 2

  object RotaryTablePart2
  {
    import Actuators.RotaryTablePart2._
    
    val actuators: FestoActuatorDescriptions = BeMap(
        Component(CloseGripper)  ==> FestoComponentDescription(isActuator, isSolenoid, isGpio16, isHighGripActuator) ^
        Component(LowerGripper)  ==> FestoComponentDescription(isActuator, isSolenoid, isGpio1,  isHighGripActuator)  ^
        Component(UnscrewCap)    ==> FestoComponentDescription(isActuator, isSolenoid, isGpio4,  isHighSolenoidActuator)  ^
        Component(ScrewCap)      ==> FestoComponentDescription(isActuator, isSolenoid, isGpio5,  isHighSolenoidActuator)  )
    
    import Sensors.RotaryTablePart2._
        
    val sensors: FestoSensorDescriptions = BeMap(
        Component(DropLid)       ==> FestoComponentDescription(isSensor, isLightSensor, isGpio8, isHighLightSensor) ^
        Component(ScrewLid)      ==> FestoComponentDescription(isSensor, isLightSensor, isGpio9, isHighLightSensor) ^
        Component(BottleFinish)  ==> FestoComponentDescription(isSensor, isLightSensor, isGpio7, isHighLightSensor) ^
        Component(EnterConveyor) ==> FestoComponentDescription(isSensor, isLightSensor, isGpio0, isHighLightSensor) ^
        Component(ThreeBottles)  ==> FestoComponentDescription(isSensor, isLightSensor, isGpio2, isHighLightSensor) )
  
    val devices = actuators ^ sensors
  }
  
  // ------------------------------------------------------------------------------------- Station 6: Capped Bottles Conveyer Belt

  object CappedBottlesConveyerBelt
  {
    import Actuators.CappedBottlesConveyerBelt._
    
    val actuators: FestoActuatorDescriptions = BeMap(
        Component(MoveConveyor)            ==> FestoComponentDescription(isActuator, isSolenoid, isGpio15, isHighSolenoidActuator) ^
        Component(OpenGripper)             ==> FestoComponentDescription(isActuator, isSolenoid, isGpio16, isHighSolenoidActuator) ^
        Component(CloseGripper)            ==> FestoComponentDescription(isActuator, isSolenoid, isGpio1,  isHighSolenoidActuator)  ^
        Component(MoveGripperToConveyor)   ==> FestoComponentDescription(isActuator, isSolenoid, isGpio5,  isHighSolenoidActuator)  ^
        Component(MoveGripperFromConveyor) ==> FestoComponentDescription(isActuator, isSolenoid, isGpio6,  isHighSolenoidActuator)  )
    
    import Sensors.CappedBottlesConveyerBelt._
        
    val sensors: FestoSensorDescriptions = BeMap(
        Component(GripperOpen)             ==> FestoComponentDescription(isSensor, isLightSensor, isGpio8,  isHighLightSensor) ^
        Component(GripperClosed)           ==> FestoComponentDescription(isSensor, isLightSensor, isGpio9,  isHighLightSensor) ^
        Component(GripperLeftPostion)      ==> FestoComponentDescription(isSensor, isLightSensor, isGpio2,  isHighLightSensor) ^
        Component(GripperRightPosition)    ==> FestoComponentDescription(isSensor, isLightSensor, isGpio12, isHighLightSensor) ^
        Component(GripperMiddlePosition)   ==> FestoComponentDescription(isSensor, isLightSensor, isGpio13, isHighLightSensor) )
  
    val devices = actuators ^ sensors
  }
  
  // TODO...

  // ------------------------------------------------------------------------------------- Station 7: Packaging Station

  object PackagingStation
  {
    val devices = TRUE()
  }

  // ------------------------------------------------------------------------------------- Station 8: Bottle Conveyer

  object BottleConveyer
  {
    val devices = TRUE()
  }
  
  // ------------------------------------------------------------------------------------- Station 9: Bottle Conveyer Sorting

  object BottleConveyerSorting
  {
    val devices = TRUE()
  }


  
  // COMPOSE ALL DEVICES
        
  val devices = simplify(
                        CapDispenser.devices ^
                        CapConveyer.devices ^
                        Capping.devices ^
                        RotaryTablePart1.devices ^
                        RotaryTablePart2.devices ^
                        CappedBottlesConveyerBelt.devices ^
                        PackagingStation.devices ^
                        BottleConveyer.devices ^
                        BottleConveyerSorting.devices
                        ).asInstanceOf[BIGAND[FestoComponentDescription]]
   
   
  // Searching
  
  def findByDevice(device: Device[ID]): Option[FestoComponentDescription] =
  {
        val description = findConclusion(devices, Component(device))
        
        if (description.isInstanceOf[FestoComponentDescription]) 
          Some(description.asInstanceOf[FestoComponentDescription])
        else 
          None
  }
  
  def findByGpio(gpioPin: Int): Option[FestoComponentDescription] =
  {
        val description = findConjunctionWithTerm(devices, GPIO ==> ComponentValue(gpioPin))
        
        if (description.isInstanceOf[FestoComponentDescription]) 
          Some(description.asInstanceOf[FestoComponentDescription])
        else 
          None
  }
    
}



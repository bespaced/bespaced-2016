/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import java.util.Date
import java.util.Calendar
import java.util.GregorianCalendar

import BeSpaceDCore._
import BeSpaceDGestalt.core._

import BeSpaceDIA.model.PhysicalModel._



class FestoDeviceEvent(device: FestoDevice, timepoint: TimePoint[Long], state: FestoDeviceState)
extends DeviceEvent[ID, FestoSignal](device, timepoint, state)
//with FiniteStateAutomata.Input[FestoDeviceEvent]
{
   override def eventType = Events.FestoEventType
   
   def apply(index: Int): Option[BeSpaceDFesto.model.FestoSignal] = ???
   def apply(key: String): Option[BeSpaceDFesto.model.FestoSignal] = ???
}

//object FestoDeviceEvent {
//  
//  /**
//   * This constructs a Festo specific event which is a refinement of a BeSpaceD INSTATE invariant.
//   * 
//   * @param s The sensor.
//   * @param t The time the event actually happened in the real world.
//   * @param v The state of the sensor.
//   * 
//   * @return The event.
//   */
//  def apply(s: FestoDevice, t: TimePoint[Long], v: FestoDeviceState) = new FestoDeviceEvent(s, t, v) 
//  def apply(eventName: String): FestoDeviceEvent =
//   {
//     val part = eventName.split(":")
//     val deviceName  = part(0)
//     val deviceState = part(1)
//     
//     val device = deviceName
//     val signal = FestoSignal(sensorState)
//     val state  = FestoSensorState(device, signal)
//     
//     FestoDeviceEvent(device, TimePoint(), state)
//   }
//  }

class FestoSensorEvent(sensor: FestoSensor, timepoint: TimePoint[Long], state: FestoSensorState)
extends SensorEvent[ID, FestoSignal](sensor, timepoint, state)
//with FiniteStateAutomata.Input[FestoSensorEvent]
{
   override def eventType = Events.FestoEventType
   
   def apply(index: Int): Option[BeSpaceDFesto.model.FestoSignal] = ???
   def apply(key: String): Option[BeSpaceDFesto.model.FestoSignal] = ???
}

object FestoSensorEvent {
  
  /**
   * This constructs a Festo specific event which is a refinement of a BeSpaceD INSTATE invariant.
   * 
   * @param s The sensor.
   * @param t The time the event actually happened in the real world.
   * @param v The state of the sensor.
   * 
   * @return The event.
   */
  def apply(s: FestoSensor, t: TimePoint[Long], v: FestoSensorState) = new FestoSensorEvent(s, t, v) 
  def apply(eventName: String): FestoSensorEvent =
   {
     val part = eventName.split(":")
     val sensorName  = part(0)
     val sensorState = part(1)
     
     val sensor = FestoSensor(sensorName)
     val signal = FestoSignal(sensorState)
     val state  = FestoSensorState(sensor, signal)
     
     FestoSensorEvent(sensor, TimePoint(), state)
   }
  }

object Events {
  
  val FESTO_EVENT_TYPE_ID = "event-type.festo-mini-factory"
  
  object FestoEventType extends BeSpaceDGestalt.core.EventType(FESTO_EVENT_TYPE_ID)
    
}



/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._

import BeSpaceDIA.model.PhysicalModel._

import DeviceDescription._



class FestoSensor(override val id: ID) extends Component(id) with Sensor[ID] with FestoDevice
{
  lazy val description: FestoComponentDescription = DeviceDescription.findByDevice(this).get
}
object FestoSensor { def apply(name:ID): FestoSensor = { new FestoSensor(name) } }

object Sensors {

  // Station 1
  object CapDispenser
  {
    val StackEjectorExtended  = FestoSensor("Stack Ejector Extended" )
    val StackEjectorRetracted = FestoSensor("Stack Ejector Retracted")
    val WorkpieceGripped      = FestoSensor("Workpiece Gripped"      )
    val LoaderPickedUp        = FestoSensor("Loader Picked Up"       )
    val LoaderDroppedOff      = FestoSensor("Loader Dropped Off"     )
    val StackEmpty            = FestoSensor("Stack Empty"            )
  }
  
  // Station 2
  object CapConveyer
  {
    val FirstConveyor         = FestoSensor("First Conveyor"         )
    val SecondConveyor        = FestoSensor("Second Conveyor"        )
    val LastConveyor          = FestoSensor("Last Conveyor"          )
  }
  
  // Station 3
  object Capping
  {
    val EndOfConveyor         = FestoSensor("End Of Conveyor"        )
    val ConveyorPosition      = FestoSensor("Conveyor Position"      )
    val DropoffPosition       = FestoSensor("Dropoff Position"       )
    val MiddlePosition        = FestoSensor("Middle Position"        )
    val ArmLowered            = FestoSensor("Arm Lowered"            )
    val ArmRaised             = FestoSensor("Arm Raised"             )
  }

  // Station 4
  object RotaryTablePart1
  {
    val EnteringRotaryTable   = FestoSensor("Entering Rotary Table"  )
    val LiquidFiller          = FestoSensor("Liquid Filler"          )
    val GranularFiller        = FestoSensor("Granular Filler"        )    
  }

  // Station 5
  object RotaryTablePart2
  {
    val DropLid               = FestoSensor("Drop Lid"               )
    val ScrewLid              = FestoSensor("Screw Lid"              )
    val BottleFinish          = FestoSensor("Bottle Finish"          )
    val EnterConveyor         = FestoSensor("Enter Conveyor"         )
    val ThreeBottles          = FestoSensor("Three Bottles"          )
  }

  // Station 6
  object CappedBottlesConveyerBelt
  {
    val GripperOpen           = FestoSensor("Gripper Open"           )
    val GripperClosed         = FestoSensor("Gripper Closed"         )
    val GripperLeftPostion    = FestoSensor("Gripper Left Postion"   )
    val GripperRightPosition  = FestoSensor("Gripper Right Position" )
    val GripperMiddlePosition = FestoSensor("Gripper Middle Position")
  }
  
  // TODO...

  // Station 7
  object PackagingStation
  {
    
  }

  // Station 8
  object BottleConveyer
  {
      
  }
  
  // Station 9

                        
  type FestoSensorComponent  = Component[FestoSensor]
  type FestoSensorOccupyNode = OccupyNode[FestoSensor]
  
  // ---------------------------------------------------------------- Creation
  
  def createByName(name: String): FestoSensor = FestoSensor(name)
  
}

object SensorsInState
{
  object CapDispenser
  {
    import Sensors.CapDispenser._
  
    val  StackEjectorRetractedOn = StackEjectorRetracted ==> ObstructedHigh
    val StackEjectorRetractedOff = StackEjectorRetracted ==> UnobstructedLow
    val   StackEjectorExtendedOn = StackEjectorExtended  ==> ObstructedHigh
    val  StackEjectorExtendedOff = StackEjectorExtended  ==> UnobstructedLow
    val             StackEmptyOn = StackEmpty            ==> ObstructedHigh
    val            StackEmptyOff = StackEmpty            ==> UnobstructedLow
    val       WorkpieceGrippedOn = WorkpieceGripped      ==> ObstructedHigh
    val      WorkpieceGrippedOff = WorkpieceGripped      ==> UnobstructedLow
    val         LoaderPickedUpOn = LoaderPickedUp        ==> ObstructedHigh
    val        LoaderPickedUpOff = LoaderPickedUp        ==> UnobstructedLow
    val       LoaderDroppedOffOn = LoaderDroppedOff      ==> ObstructedHigh
    val      LoaderDroppedOffOff = LoaderDroppedOff      ==> UnobstructedLow
  }
  
  type FestoSensorInState = IMPLIES[FestoSensor, FestoSensorState]

}



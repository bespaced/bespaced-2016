/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._

import BeSpaceDIA.model.PhysicalModel._

import DeviceDescription._



class FestoPart(override val id: ID) extends Component(id) with Part[ID] with FestoDevice
{
  lazy val description: FestoComponentDescription = DeviceDescription.findByDevice(this).get
}
object FestoPart { def apply(name:ID): FestoPart = { new FestoPart(name) } }

object Parts {
  
  // Station 1
  object CapDispenser
  {
    val Tube          = FestoPart("Cap Tube")
    val StackEjector  = FestoPart("Stack Ejector")
    val Loader        = FestoPart("Cap Loader")
    val VacuumGripper = FestoPart("Vacuum Gripper")
    
    // Stack Ejector
    abstract class StackEjectorPosition extends ATOM with SpatialVariation
    object StackEjectorRetractedPosition extends StackEjectorPosition
    object StackEjectorExtendedPosition extends StackEjectorPosition
    
    val StackEjectorPositions: XOR[StackEjectorPosition] = XOR(StackEjectorRetractedPosition, StackEjectorExtendedPosition)
    
    
    // Loader
    abstract class LoaderPosition extends ATOM with SpatialVariation
    {
      
    }
    object LoaderPickupPosition extends LoaderPosition
    object LoaderDropoffPosition extends LoaderPosition
    
    val LoaderPositions: XOR[LoaderPosition] = XOR(LoaderPickupPosition, LoaderDropoffPosition)


  }
  
    // TODO...
    // Station 2
    // Station 3
    // Station 4
    // Station 5
    // Station 6
    // Station 7
    // Station 8
    // Station 9
    // Station 10

      
}
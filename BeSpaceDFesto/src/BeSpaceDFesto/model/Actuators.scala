/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._

import BeSpaceDIA.model.PhysicalModel._

import DeviceDescription._



class FestoActuator(override val id: String) extends Component(id) with Actuator[ID] with FestoDevice
{
  lazy val description: FestoComponentDescription = DeviceDescription.findByDevice(this).get
}
object FestoActuator { def apply(name:String): FestoActuator = { new FestoActuator(name) } }

object Actuators {

  // Station 1
  object CapDispenser
  {
    val StackEjectorExtend   = FestoActuator("Stack Ejector Extend"   )
    val VacuumGrip           = FestoActuator("Vacuum Grip"            )
    val EjectAirPulse        = FestoActuator("Eject Air Pulse"        )
    val LoaderPickup         = FestoActuator("Loader Pickup"          )
    val LoaderDropoff        = FestoActuator("Loader Dropoff"         )
  }
  
  // Station 2
  object CapConveyer
  {
    val ConveyorBeltGate     = FestoActuator("Conveyor Belt Gate"     )
    val ConveyorBeltMove     = FestoActuator("Conveyor Belt Move"     )
  }
  
  // Station 3
  object Capping
  {
    val MoveToConveyorBelt   = FestoActuator("Move To Conveyor Belt"  )
    val MoveFromConveyorBelt = FestoActuator("Move From Conveyor Belt")
    val LowerArm             = FestoActuator("Lower Arm"              )
    val GripWorkpiece        = FestoActuator("Grip Workpiece"         )
  }

  // Station 4
  object RotaryTablePart1
  {
    val MoveSmallConveyor    = FestoActuator("Move Small Conveyor"    )
    val RotateTable          = FestoActuator("Rotate Table"           )
    val MoveLargeConveyor    = FestoActuator("Move Large Conveyor"    )
  }

  // Station 5
  object RotaryTablePart2
  {
    val CloseGripper         = FestoActuator("Close Gripper"          )
    val LowerGripper         = FestoActuator("Lower Gripper"          )
    val UnscrewCap           = FestoActuator("Unscrew Cap"            )
    val ScrewCap             = FestoActuator("Screw Cap"              )
  }

  // Station 6
  object CappedBottlesConveyerBelt
  {
    val MoveConveyor            = FestoActuator("Move Conveyor"              )
    val OpenGripper             = FestoActuator("Open Gripper"               )
    val CloseGripper            = FestoActuator("Close Gripper"              )
    val MoveGripperToConveyor   = FestoActuator("Move Gripper To Conveyor"   )
    val MoveGripperFromConveyor = FestoActuator("Move Gripper From Conveyor" )
  }
  
  // TODO...

  // Station 7
  object PackagingStation
  {
    
  }
  
  // Station 8
  object BottleConveyer
  {
      
  }
  
  // Station 9
  object BottleConveyerSorting
  {
    
  }
  
  type FestoActuatorComponent  = Component[FestoActuator]
  type FestoActuatorOccupyNode = OccupyNode[FestoActuator]
  
}
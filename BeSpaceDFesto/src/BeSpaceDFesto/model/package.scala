/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * The "model" module is for defining the domain specific modeling types and values for the Festo Mini Factory
 */

package BeSpaceDFesto

import java.util.Date
import java.util.Calendar
import java.util.GregorianCalendar

import scala.compat.Platform.EOL

import BeSpaceDCore._
import BeSpaceDCore.Log._

import BeSpaceDIA.model.PhysicalModel._

import model.DeviceDescription.FestoComponentDescription
import BeSpaceDFesto.model.topology.Configuration
import BeSpaceDFesto.model.topology.Process



package object model {  
  
  val core = standardDefinitions; import core._
  
  /////////////////////////////
  object ExamplesOfJansTemporalConstraintsForFestoPhase2
  {
    implicit val stringOrdering = { _: String => Ordering[String] } 
    
    case class EventConstructor[Node, EventName] (n:Node, en: EventName)
    
    //constraints 
    val constraint1 = TimePoint(new TERTP( EventConstructor("first sensor", "triggered"), "300ms") ) ==> Event("second sensor triggered")
    
    //meta constraints action
    val handleconstraint1 = Event(constraint1) ==>  Event("stop machine")
    
    //TODO: Build event relative time points into the new festo models
  }
  //////////////////////////////
  
  // ==================================================================================== Festo Object Identity
  
  // Identity
  
  type ID = String
  
  
  // ==================================================================================== Festo Devices
  
  trait FestoDevice extends Device[ID] // with FiniteStateAutomata.Input[ID]
  {
    val id: String
    //val event = id
  }
  
  // Devices are parts, actuators and sensors.
  // Devices are defined by an enumeration using case classes and values.
  
  
  
  // ==================================================================================== Festo Materials
  
  trait FestoMeterial extends Material[ID]
  
  type FestoMeterialComponent  = Component[FestoMeterial]
  
  
  
  // ------------------------------------------------------------------------------------ Discrete

    class FestoDiscreteMaterial(id: ID) extends Component(id) with DiscreteMaterial[ID]
    object FestoDiscreteMaterial { def apply(id:String) = new FestoDiscreteMaterial(id) }
    
    val Bottle  = FestoDiscreteMaterial("Bottle")
    val Cap     = FestoDiscreteMaterial("Cap")
    
    
    type FestoDiscreteMaterialComponent  = Component[FestoDiscreteMaterial]
    type FestoDiscreteMaterialOccupyNode = OccupyNode[FestoDiscreteMaterial]
  
  
 
  // ------------------------------------------------------------------------------------ Analog  

    abstract class FestoAnalogMaterial(id: ID, override val unit: String, override val quantity: Double) extends Component(id) with AnalogMaterial[ID]
    
    class Corn(override val quantity: Double) extends FestoAnalogMaterial("Corn Kernels","g",  quantity)
    object Corn { def apply(quantity: Double) = new Corn(quantity) }

    val NoCorn = Corn(0)
    
    class Water(override val quantity: Double) extends FestoAnalogMaterial("Water", "ml", quantity)
    object Water { def apply(quantity: Double) = new Water(quantity) }

    val NoWater = Water(0)
    
    
    type FestoAnalogMaterialComponent  = Component[FestoAnalogMaterial]
    type FestoAnalogMaterialOccupyNode = OccupyNode[FestoAnalogMaterial]
  
  
  
   // ==================================================================================== Festo Signals

    class FestoSignal(val state: String) extends Jsonable
    {
      override def toString = state
      override def equals(that: Any): Boolean = (if (that.isInstanceOf[FestoSignal]) this.state == that.asInstanceOf[FestoSignal].state else false)
      
      // Jsonable
      override def toJson =
      {
        this match
        {
          case High => """{"type": "FestoSignal::High"}"""
          case Low  => """{"type": "FestoSignal::Low"}"""
          case DC   => """{"type": "FestoSignal::DC"}"""
        }
      }
    }
    
    object FestoSignal { def apply(state: String): FestoSignal = new FestoSignal(state) }
    
    case object High extends FestoSignal("High")
    case object Low  extends FestoSignal("Low" )
    case object DC   extends FestoSignal("Don't Care" )
    
        
   // ==================================================================================== Festo States

    abstract class FestoDeviceState(signal: FestoSignal) extends ComponentState(signal) with DeviceState[FestoSignal] with EventLike[FestoDeviceState] with Jsonable with PrettyPrintable
    {
      // EventLike
      override val event = this
      
      // Jsonable
      override def toJson =
      {
//        val fullClassName = this.getClass.getName
//        val cStart = fullClassName.lastIndexOf('$')
//        val className = fullClassName.substring(cStart+1)
        
        val fullDescription = this.toString
        val dStart = fullDescription.lastIndexOf('(')
        val description = fullDescription.substring(0, dStart)

        val theType   = s"""{"type": "$description""""
        val theSignal = if (signal == DC) "" else s""", "signal": ${signal.toString}}"""
        val theEnd = "}"
        
        theType + theSignal + theEnd
      }
    }
    
    
    
    // ---------------------------------------------------------------------------------- Actuator States
    
    abstract class FestoActuatorState(signal: FestoSignal) extends FestoDeviceState(signal) with ActuatorState[FestoSignal]
    object FestoActuatorState
    {
      def apply(actuator: FestoActuator, signal: FestoSignal): FestoActuatorState = 
      {
        val potentialDescription = model.DeviceDescription.findByDevice(actuator)
        
        if (potentialDescription.isEmpty) throw new RuntimeException(s"Unknown actuator: $actuator")
        else
        {
          val actuatorDescription: FestoComponentDescription = potentialDescription.get.asInstanceOf[FestoComponentDescription]
          
          debugOff(s"actuatorDescription = $actuatorDescription")
          debugOn(s"signalToState = ${actuatorDescription.signalToActuatorState}")
          debugOn(s"signal = ${signal}")
          assert( signal == High )
          
          actuatorDescription.signalToActuatorState(signal)
        }
      }
      
      def apply(actuator: FestoActuator, on: Boolean): FestoActuatorState = 
      {
        val potentialDescription = model.DeviceDescription.findByDevice(actuator)
        
        if (potentialDescription.isEmpty) throw new RuntimeException(s"Unknown actuator: $actuator")
        else
        {
          val actuatorDescription: FestoComponentDescription = potentialDescription.get.asInstanceOf[FestoComponentDescription]
          
          val highState = actuatorDescription.signalToActuatorState(High)
          
          if (highState == ObstructedHigh || highState == GrippedHigh)
            highState
          else 
            actuatorDescription.signalToActuatorState(Low)
        }
      }
    }

    abstract class FestoSolenoidActuatorState(signal: FestoSignal) extends FestoActuatorState(signal)
    class ActiveState(sig: FestoSignal)    extends FestoSolenoidActuatorState(sig)
    { 
      override def toString = s"Active${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Active${if (sig != DC) s"($sig)" else ""}"
    }
    object ActiveState { def apply(sig: FestoSignal): ActiveState = new ActiveState(sig) }
    class PassiveState(sig: FestoSignal)  extends FestoSolenoidActuatorState(sig)
    { 
      override def toString = s"Passive${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Passive${if (sig != DC) s"($sig)" else ""}"
    }
    object PassiveState { def apply(sig: FestoSignal): PassiveState = new PassiveState(sig) }
    
    // Constants
    val ActiveHigh  = ActiveState(High)
    val ActiveLow   = ActiveState(Low)
    val Active      = ActiveState(DC)
    val PassiveHigh = PassiveState(High)
    val PassiveLow  = PassiveState(Low)
    val Passive     = PassiveState(DC)
    
    abstract class FestoGripActuatorState(sig: FestoSignal) extends FestoActuatorState(sig)
    class GripState(sig: FestoSignal) extends FestoGripActuatorState(sig)
    {
      override def toString = s"Grip${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Grip${if (sig != DC) s"($sig)" else ""}"
    }
    object GripState { def apply(sig: FestoSignal): GripState = new GripState(sig) }
    class ReleaseState(sig: FestoSignal)   extends FestoGripActuatorState(sig)
    {
      override def toString = s"Release${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Release${if (sig != DC) s"($sig)" else ""}"
    }
    object ReleaseState { def apply(sig: FestoSignal): ReleaseState = new ReleaseState(sig) }
    
    // Constants
    val GripHigh    = GripState(High)
    val GripLow     = GripState(Low)
    val Grip        = GripState(DC)
    val ReleaseHigh = ReleaseState(High)
    val ReleaseLow  = ReleaseState(Low)
    val Release     = ReleaseState(DC)
    
    
    
    // ---------------------------------------------------------------------------------- Sensor States
    
    abstract class FestoSensorState(signal: FestoSignal) extends FestoDeviceState(signal) with SensorState[FestoSignal]
    object FestoSensorState
    {
      def apply(sensor: FestoSensor, signal: FestoSignal): FestoSensorState = 
      {
        val potentialDescription = model.DeviceDescription.findByDevice(sensor)
        
        if (potentialDescription.isEmpty) throw new RuntimeException(s"Unknown sensor: $sensor")
        else
        {
          val sensorDescription: FestoComponentDescription = potentialDescription.get.asInstanceOf[FestoComponentDescription]
          
          debugOff(s"sensorDescription = $sensorDescription")
          debugOn(s"signalToState = ${sensorDescription.signalToSensorState}")
          debugOn(s"signal = ${signal}")
          assert( signal == High )
          
          sensorDescription.signalToSensorState(signal)
        }
      }
      
      def apply(sensor: FestoSensor, on: Boolean): FestoSensorState = 
      {
        val potentialDescription = model.DeviceDescription.findByDevice(sensor)
        
        if (potentialDescription.isEmpty) throw new RuntimeException(s"Unknown sensor: $sensor")
        else
        {
          val sensorDescription: FestoComponentDescription = potentialDescription.get.asInstanceOf[FestoComponentDescription]
          
          val highState = sensorDescription.signalToSensorState(High)
          
          if (highState == ObstructedHigh || highState == GrippedHigh)
            highState
          else 
            sensorDescription.signalToSensorState(Low)
        }
      }
    }

    abstract class FestoLightSensorState(sig: FestoSignal) extends FestoSensorState(sig)
    class ObstructedState(sig: FestoSignal)    extends FestoLightSensorState(sig)
    {
      override def toString = s"Obstructed${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Obstructed${if (sig != DC) s"($sig)" else ""}"
    }
    object ObstructedState { def apply(sig: FestoSignal): ObstructedState = new ObstructedState(sig) }
    class UnobstructedState(sig: FestoSignal)  extends FestoLightSensorState(sig)
    {
      override def toString = s"Unobstructed${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Unobstructed${if (sig != DC) s"($sig)" else ""}"
    }
    object UnobstructedState { def apply(sig: FestoSignal): UnobstructedState = new UnobstructedState(sig) }
    
    // Constants
    val ObstructedHigh   = ObstructedState(High)
    val ObstructedLow    = ObstructedState(Low)
    val Obstructed       = ObstructedState(DC)
    val UnobstructedHigh = UnobstructedState(High)
    val UnobstructedLow  = UnobstructedState(Low)
    val Unobstructed     = UnobstructedState(DC)
    
    abstract class FestoGripSensorState(signal: FestoSignal) extends FestoSensorState(signal)
    class GrippedState(sig: FestoSignal) extends FestoGripSensorState(sig)
    {
      override def toString = s"Gripped${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Gripped${if (sig != DC) s"($sig)" else ""}"
    }
    object GrippedState { def apply(sig: FestoSignal): GrippedState = new GrippedState(sig) }
    class LooseState(sig: FestoSignal)   extends FestoGripSensorState(sig)
    {
      override def toString = s"Loose${if (sig != DC) s"($sig)" else "()"}"
      override def pp       = s"Loose${if (sig != DC) s"($sig)" else ""}"
    }
    object LooseState { def apply(sig: FestoSignal): LooseState = new LooseState(sig) }
    
    // Constants
    val GrippedHigh = GrippedState(High)
    val GrippedLow  = GrippedState(Low)
    val Gripped     = GrippedState(DC)
    val LooseHigh   = LooseState(High)
    val LooseLow    = LooseState(Low)
    val Loose       = LooseState(DC)
    

  
  // ==================================================================================== Festo Spatial Relationships
    
    // TODO:
    //       Think about conveyer belts. e.g. break up the belt into three parts: beginning, middle and end.
    //       When the loader is in the drop-off state it has Proximity to the beginning of the conveyer belt.
    //       So topology may involve the STATE of the device not just the device.

    
    // ---------------------------------------------------------------------------------- Festo Domain Specific Relationships
    
    trait FestoConnection               extends Annotation with Jsonable { def annotationJson = s""""edge":"${label}""""; def toJson = s"""{$annotationJson}\"""" }
    trait FunctionalFestoConnection     extends FestoConnection
    trait SpatialFestoConnection        extends FestoConnection with SpatialRelationship
    trait TemporalFestoConnection       extends FestoConnection with TemporalRelationship
    trait SpatioTemporalFestoConnection extends FestoConnection with SpatioTemporalRelationship
    
    // Actuator/Sensor Connections
    case object Attached                        extends FunctionalFestoConnection with SpatialFestoConnection { val label:String = "Attached" }
    case class  Proximity(distance: Double = 0) extends SpatialFestoConnection                                { val label:String = s"""Proximity($distance)"""; override def toJson = s"""{$annotationJson, "type"="Proximity","distance"=$distance}""" }

    
    // ==================================================================================== Festo Temporal Relationships
  
    case object ProcessSequence         extends SpatialFestoConnection with TemporalFestoConnection { val label:String = "ProcessSequence" }
    case object MaterialFlow            extends SpatialFestoConnection with TemporalFestoConnection { val label:String = "MaterialFlow" }
    
    class  FestoStateCorrelation(cause: FestoDeviceState, duration: TimeDuration[FestoDeviceState], effect: FestoDeviceState)
    extends TemporalCorrelation[FestoDeviceState, Integer](cause, duration, effect)
    with TemporalFestoConnection
    with Annotation3 with Jsonable
    {
      val labelSource = s"""${cause.pp}"""
      val labelTarget = s"""${effect.pp}"""
      val labelEdge   = s"""${prettyPrintInvariant(duration)}"""
      
      override val annotationJson = s""""source":"$labelSource","edge":"$labelEdge","target":"$labelTarget""""
      override def toJson = s"""{$annotationJson, "type": "FestoStateCorrelation", "cause": ${BeSpaceDCore.toJson(cause)}, "duration": ${BeSpaceDCore.toJson(duration)}, "effect": ${BeSpaceDCore.toJson(effect)}}"""
    }
    object FestoStateCorrelation { def apply(cause: FestoDeviceState, duration: TimeDuration[FestoDeviceState], effect: FestoDeviceState): FestoStateCorrelation = new FestoStateCorrelation(cause, duration, effect) }
    
    
    class  FestoStateConstraint(cause: FestoDeviceState, durationRange: TimeDurationRange[FestoDeviceState], effect: FestoDeviceState, inverse: Boolean = false)
    extends TemporalConstraint[FestoDeviceState, Integer](cause, durationRange, effect, inverse)
    with TemporalFestoConnection
    with Annotation3 with Jsonable
    {
      val labelSource = s"""${cause.pp}"""
      val labelTarget = s"""${effect.pp}"""
      val labelEdge   = s"""${prettyPrintInvariant(durationRange)}"""
      
      override val annotationJson = s""""source":"$labelSource","edge":"$labelEdge","target":"$labelTarget""""
      override def toJson = s"""{$annotationJson, "type": "FestoStateConstraint", "cause": ${BeSpaceDCore.toJson(cause)}, "durationRange": ${BeSpaceDCore.toJson(durationRange)}, "effect": ${BeSpaceDCore.toJson(effect)}}"""
    }
    object FestoStateConstraint { def apply(cause: FestoDeviceState, durationRange: TimeDurationRange[FestoDeviceState], effect: FestoDeviceState, inverse: Boolean = false): FestoStateConstraint = new FestoStateConstraint(cause, durationRange, effect, inverse) }
    
    
  // ==================================================================================== Generate Data Sets
    
    
    def main(args: Array[String])
    {
      TEST_Events
    }
    
    def TEST_Events
    {
      // Archive some example sensor events as JSON
      
      // This hacked version of FestoSensorEvent has been replaced with the real one (below)
      // class FestoSensorEvent(sensor: FestoSensor, timepoint: Date, state: FestoSensorState) extends SensorEvent[ID, Date, FestoSignal](sensor, timepoint, state) 
      // object FestoSensorEvent { def apply(s: FestoSensor, t: Date, v: FestoSensorState) = new FestoSensorEvent(s,t,v) }
      
      val d1 = new GregorianCalendar(); d1.add(Calendar.SECOND, 1)
      val d2 = new GregorianCalendar(); d2.add(Calendar.SECOND, 2)
      val d3 = new GregorianCalendar(); d3.add(Calendar.SECOND, 3)
      val d4 = new GregorianCalendar(); d4.add(Calendar.SECOND, 4)
      
      val t1 = TimePoint(d1.getTime.getTime)
      val t2 = TimePoint(d2.getTime.getTime)
      val t3 = TimePoint(d3.getTime.getTime)
      val t4 = TimePoint(d4.getTime.getTime)
      
      import Sensors._
      
      val se1 = FestoSensorEvent(CapDispenser.StackEjectorRetracted, t1, ObstructedHigh  )
      val se2 = FestoSensorEvent(CapDispenser.WorkpieceGripped,      t2, UnobstructedLow)
      val se3 = FestoSensorEvent(CapDispenser.LoaderPickedUp,        t3, ObstructedHigh)
      val se4 = FestoSensorEvent(CapDispenser.StackEmpty,            t4, UnobstructedLow)
      
      val exampleSensorEvents_Station1 = se1 ^ se2 ^ se3 ^ se4
        
      val exampleSensorEventsJson_Station1: String  = toJson(exampleSensorEvents_Station1)
      
      println("Example Sensor Events of Festo Station 1")
      println("---------------------------------------------------")
      println(exampleSensorEventsJson_Station1)
      println("---------------------------------------------------")
      
    }    
    
    
}









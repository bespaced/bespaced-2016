/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model.topology

import BeSpaceDCore._

import BeSpaceDIA.model.PhysicalModel._

import BeSpaceDFesto.model._
import BeSpaceDFesto.model.DeviceDescription._



object Configuration {
    
  // Convenience functions for annotated edges
  @inline def attached(s: FestoDevice, t: FestoDevice)          = EdgeAnnotated(s, t, Attached)
  @inline def proximity(s: FestoDevice, t: FestoDevice, d: Int) = EdgeAnnotated(s, t, Proximity(d))
        
  val partsAndSensorBasicTest: (BIGAND[FestoComponentDescription], FunctionalTopology[ID, SpatialFestoConnection]) =
  {        
    // TODO: Include all the parts, sensors and actuators.

    val spatialGraph = BeGraphAnnotated[FestoDevice, SpatialFestoConnection] (
        attached(Parts.CapDispenser.StackEjector , Sensors.CapDispenser.StackEjectorExtended)  ^
        attached(Parts.CapDispenser.StackEjector , Sensors.CapDispenser.StackEjectorRetracted) ^
        proximity(Sensors.CapDispenser.StackEjectorExtended , Sensors.CapDispenser.StackEmpty, 3)           
        )
    
    val allDescriptions = DeviceDescription.devices
    
    (allDescriptions, spatialGraph)
  }
}
package BeSpaceDFesto.model

import scala.compat.Platform.EOL

import BeSpaceDCore._



package object topology {
  
    val core = standardDefinitions; import core._
        
    // ==================================================================================== Generate Sample Topologies
    
    def main(args: Array[String])
    {
      TEST_Process
      TEST_Configuration
      TEST_SpatioTemporal_ProcessSequence  
      TEST_SpatioTemporal_ActuatorSensorCausality
      
      TEST_SpatioTemporal_ActuatorAvoidance
    }
    
    def TEST_Process
    {
      val sensorsProcessTopologyJson_Station1: String  = toJson(Process.Static.sensorTopology)
      
      println();println();println()
      println("Process Topology for Sensors Only of Festo Station 1")
      println("---------------------------------------------------")
      println(sensorsProcessTopologyJson_Station1)
      println("---------------------------------------------------")
    }
    
    def TEST_Configuration
      {
        // Archive the spatial topology as JSON
        val configurationTopology  = Configuration.partsAndSensorBasicTest
        val descriptions = configurationTopology._1
        val graph_Test   = configurationTopology._2
  
        println();println();println()
        println("Configuration (descriptions + topology)")
        println("--------------------------------------------------- Pretty")
        println(prettyPrintInvariant(descriptions))
        println(prettyPrintInvariant(graph_Test))
        println("--------------------------------------------------- JSON")
        val descriptions_json = toJson(descriptions)
        val graph_Test_json   = toJson(graph_Test)
        println(descriptions_json + EOL + graph_Test_json)
        println("---------------------------------------------------")
      }
      
    def TEST_SpatioTemporal_ProcessSequence
      {
        val spatioTemporalTopology_Station1  = SpatioTemporal.sensorProcessSequence_S1
        
        println();println();println()
        println("Spatio-Temporal Topology for Festo Station 1 - Process Sequence")
        println("--------------------------------------------------- Pretty")
        println(prettyPrintInvariant(spatioTemporalTopology_Station1))
        println("--------------------------------------------------- JSON")
        val graph_S1_json = toJson(spatioTemporalTopology_Station1)
        println(graph_S1_json)
        println("---------------------------------------------------")
      }
    
    def TEST_SpatioTemporal_ActuatorSensorCausality
      {
        val asCausalityTopology_Station1  = SpatioTemporal.actuatorSensorCausality_S1
        
        println();println();println()
        println("Spatio-Temporal Topology for Festo Station 1 - Actuator-Sensor Causality")
        println("--------------------------------------------------- Pretty")
        println(prettyPrintInvariant(asCausalityTopology_Station1))
        println("--------------------------------------------------- JSON")
        val graph_S1_json = toJson(asCausalityTopology_Station1)
        println(graph_S1_json)
        println("---------------------------------------------------")
      }
    
    def TEST_SpatioTemporal_ActuatorAvoidance
      {
        val avoidanceTopology_Station1  = SpatioTemporal.actuatorAvoidance_S1
        
        println();println();println()
        println("Spatio-Temporal Topology for Festo Station 1 - Actuator-Actuator Avoidance")
        println("--------------------------------------------------- Pretty")
        println(prettyPrintInvariant(avoidanceTopology_Station1))
        println("--------------------------------------------------- JSON")
        val graph_S1_json = toJson(avoidanceTopology_Station1)
        println(graph_S1_json)
        println("---------------------------------------------------")
      }
    
}
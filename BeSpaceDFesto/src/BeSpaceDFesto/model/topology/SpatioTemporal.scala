/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model.topology

import BeSpaceDCore._

import BeSpaceDIA.model.PhysicalModel._

import BeSpaceDFesto.model._

import scala.Left
import scala.Right



object SpatioTemporal
{
  
  val core = standardDefinitions; import core._
    
  //type SpatioTemporalFestoConnection = SpatialFestoConnection or TemporalFestoConnection
  
  // Convenience functions for edge annotations.
  @inline def attached(s: FestoDevice, t: FestoDevice)          = EdgeAnnotated(s, t, Some(Left(Attached)))
  @inline def proximity(s: FestoDevice, t: FestoDevice, d: Int) = EdgeAnnotated(s, t, Some(Left(Proximity(d))))
  
  
  val sensorProcessSequence_S1: TemporalTopology[FestoDevice, TemporalFestoConnection] = 
    {
      // NOTE: Timing is in seconds.
      
      @inline def causation[T <: Number, FDS <: FestoDeviceState](s: FestoDevice, t: FestoDevice, cause: FestoDeviceState, effect: FestoDeviceState, d: Int) =
        {
          val ertp: TERTP[FestoDeviceState,Integer] = IntERTP[FestoDeviceState](cause, d)
          val duration    = TimeDuration(ertp)
          
          val correlation = FestoStateCorrelation(cause, duration, effect)
          val connection: TemporalFestoConnection = correlation
        
          EdgeAnnotated(s, t, connection)
        }
      
      BeGraphAnnotated[FestoDevice, TemporalFestoConnection] (
          causation(Sensors.CapDispenser.StackEjectorRetracted , Sensors.CapDispenser.StackEjectorExtended,  Obstructed, Unobstructed, -2) ^
          
          causation(Sensors.CapDispenser.StackEjectorExtended  , Sensors.CapDispenser.StackEmpty,            Obstructed, Unobstructed, -1) ^
          causation(Sensors.CapDispenser.StackEjectorExtended  , Sensors.CapDispenser.StackEjectorRetracted, Obstructed, Obstructed,   -2) ^
          causation(Sensors.CapDispenser.StackEjectorExtended  , Sensors.CapDispenser.WorkpieceGripped,      Obstructed, Gripped,       5) ^
          
          causation(Sensors.CapDispenser.WorkpieceGripped      , Sensors.CapDispenser.LoaderPickedUp,        Gripped,    Obstructed,   -3) ^
          
          causation(Sensors.CapDispenser.LoaderPickedUp        , Sensors.CapDispenser.LoaderDroppedOff,      Obstructed, Obstructed,    3) ^
          
          causation(Sensors.CapDispenser.LoaderDroppedOff      , Sensors.CapDispenser.StackEjectorExtended,  Obstructed, Unobstructed,  1)
          )
          
    }
  
  val actuatorSensorCausality_S1: TemporalTopology[FestoDevice, TemporalFestoConnection] = 
    {
      // NOTE: Timing is in milliseconds.
      
      @inline def causation[T <: Number, FDS <: FestoDeviceState](s: FestoDevice, t: FestoDevice, cause: FDS, effect: FDS, d1: Int, d2: Int) =
        {
          val triggerEvent  = cause
          
          // Q: Should these be wrapped n a time duration or what ? Yes, use the TERTP version of TimeDuration
          val ertp1: TERTP[FestoDeviceState,Integer] = IntERTP[FestoDeviceState](triggerEvent, d1)
          val ertp2: TERTP[FestoDeviceState,Integer] = IntERTP(triggerEvent, d2)
          
          val minDuration   = TimeDuration(ertp1)
          val maxDuration   = TimeDuration(ertp2)
          val durationRange = TimeDurationRange(minDuration, maxDuration)
          
          val constraint    = FestoStateConstraint(cause, durationRange, effect)
          val connection: TemporalFestoConnection = constraint
          
          EdgeAnnotated(s, t, connection)
        }
    
    BeGraphAnnotated[FestoDevice, TemporalFestoConnection] (
        // Retract
          causation(Actuators.CapDispenser.StackEjectorExtend, Sensors.CapDispenser.StackEjectorExtended,   Passive, Unobstructed, 200,  300) 
        ^ causation(Actuators.CapDispenser.StackEjectorExtend, Sensors.CapDispenser.StackEjectorRetracted,  Passive, Obstructed,  1000, 1500)
        
        // Extend
        ^ causation(Actuators.CapDispenser.StackEjectorExtend, Sensors.CapDispenser.StackEjectorRetracted,  Active, Unobstructed,  200,  300)
        ^ causation(Actuators.CapDispenser.StackEjectorExtend, Sensors.CapDispenser.StackEjectorExtended,   Active, Obstructed,   1000, 1500)
        
        //Pickup
        ^ causation(Actuators.CapDispenser.LoaderPickup,       Sensors.CapDispenser.LoaderDroppedOff,       Active, Unobstructed,  200,  300)
        ^ causation(Actuators.CapDispenser.LoaderPickup,       Sensors.CapDispenser.LoaderPickedUp,         Active, Obstructed,   2000, 2500)
        
        // Dropoff
        ^ causation(Actuators.CapDispenser.LoaderDropoff,      Sensors.CapDispenser.LoaderPickedUp,         Active, Unobstructed,  200,  300)
        ^ causation(Actuators.CapDispenser.LoaderDropoff,      Sensors.CapDispenser.LoaderDroppedOff,       Active, Obstructed,   2000, 2500)
        
        // Vacuum Grip
        ^ causation(Actuators.CapDispenser.VacuumGrip,         Sensors.CapDispenser.WorkpieceGripped,       Active, Gripped,      1000, 1300)
        
        // Air Pulse Eject
        ^ causation(Actuators.CapDispenser.EjectAirPulse,      Sensors.CapDispenser.WorkpieceGripped,       Active, Loose,         500, 1000)
        )
    }
  
  val actuatorAvoidance_S1: TemporalTopology[FestoActuator, TemporalFestoConnection] = 
  {
    // NOTE: Timing is in milliseconds.
      
    // This convenience function constructs an edge between two actuators annotated with a spatio-temporal constraint.
    // The spatio-temporal constraint is a domain specific one (for the Festo Mini Factory) that represents saftey margins
    // that prevent the parts controlled by two actuators from colliding.
    
    @inline def safetyMargin[T <: Number, FAS <: FestoActuatorState](s: FestoActuator, t: FestoActuator, event: FAS, assertion: FAS, d1: Int, d2: Int) =
      {
        val triggerEvent  = event
        
        val ertp1: TERTP[FestoDeviceState,Integer] = IntERTP[FestoDeviceState](triggerEvent, d1)
        val ertp2: TERTP[FestoDeviceState,Integer] = IntERTP(triggerEvent, d2)
        
        val minDuration   = TimeDuration(ertp1)
        val maxDuration   = TimeDuration(ertp2)
        val durationRange = TimeDurationRange(minDuration, maxDuration)
        
        val constraint    = FestoStateConstraint(event, durationRange, assertion)
        val connection: TemporalFestoConnection = constraint
        
        EdgeAnnotated(s, t, connection)
      }
      
    @inline def safetyMarginNot[T <: Number, FAS <: FestoActuatorState](s: FestoActuator, t: FestoActuator, event: FAS, assertion: FAS, d1: Int, d2: Int) =
      {
        val triggerEvent  = event
        
        val ertp1: TERTP[FestoDeviceState,Integer] = IntERTP[FestoDeviceState](triggerEvent, d1)
        val ertp2: TERTP[FestoDeviceState,Integer] = IntERTP(triggerEvent, d2)
        
        val minDuration   = TimeDuration(ertp1)
        val maxDuration   = TimeDuration(ertp2)
        val durationRange = TimeDurationRange(minDuration, maxDuration)
        
        val constraint    = FestoStateConstraint(event, durationRange, assertion, inverse = true)
        val connection: TemporalFestoConnection = constraint
        
        EdgeAnnotated(s, t, connection)
      }
      
    BeGraphAnnotated[FestoActuator, TemporalFestoConnection] (
        // Extending the stack ejector: Can't collide with loader trying to pickup a cap
        // Don't actuate loader pickup from half second before through 1 second after.
          safetyMargin(Actuators.CapDispenser.StackEjectorExtend, Actuators.CapDispenser.LoaderPickup,   Active, Passive, -500,  1000)
        
        // Retracting the stack ejector: Can't retract while loader is picking up
        // Make sure the loader gets out of the pickup area at least one second before. 
        ^ safetyMargin(Actuators.CapDispenser.StackEjectorExtend, Actuators.CapDispenser.LoaderPickup,  Passive, Passive,  -1000, 0)
        
        // Picking up a cap: Can't have the stack ejector moving at the same time.
        // This rule should be covered by the two above. If we declared this one it would be redundant.
        
        // Loader moving to Dropping off position: No safety concerns
        
        // Vacuum gripping: No safety concerns
        
        // Air pulse ejecting a cap: Can't eject it near the pickup area.
        // Ensure the air pulse is only actuated when the loader is definitely not in the pickup area.
        // i.e. the loader drop-off was actuated recently (in the last 3 seconds) and the pickup was not.
        ^ safetyMargin(Actuators.CapDispenser.EjectAirPulse,    Actuators.CapDispenser.LoaderDropoff, Active, Active, -3000, 0)
        ^ safetyMarginNot(Actuators.CapDispenser.EjectAirPulse, Actuators.CapDispenser.LoaderPickup,  Active, Active, -3000, 0)
        )
  }
}
  
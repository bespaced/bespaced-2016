/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model.topology

import BeSpaceDCore._

import BeSpaceDIA.model.PhysicalModel._

import BeSpaceDFesto.model._
import BeSpaceDFesto.model.SpatialFestoConnection
import BeSpaceDFesto.model.TemporalFestoConnection

import BeSpaceDFesto.model.FestoSensor
import BeSpaceDFesto.model.Sensors
import BeSpaceDFesto.model.SensorsInState


object Process {
    
    val core = standardDefinitions; import core._

    // Topology for Sensors ONLY
    
    // KF & GP Notes: We decided that for the Visualisation of sensors in the Festo Mini Factory
    //                that the semantic relationship between them that makes the most sense is PROCESS.
    //                i.e. the chronological sequence that would normally take place in the manufacturing process.
    
    // KF Notes: 1. Actually the sequence cycles around as the ejector will be retracted before the
    //              work piece is gripped and after drop off it will go back to the start of the sequence.


    object Static
    {    
      val sensorTopology: TemporalTopology[FestoSensor, TemporalFestoConnection] =
      {
        import Sensors._
        
        @inline def edge(s: FestoSensor, t: FestoSensor) = EdgeAnnotated(s, t, Some(ProcessSequence))
        
        BeGraphAnnotated[FestoSensor, TemporalFestoConnection] (
            edge(CapDispenser.StackEjectorRetracted , CapDispenser.StackEjectorExtended)  ^
            edge(CapDispenser.StackEjectorExtended  , CapDispenser.StackEmpty)            ^
            edge(CapDispenser.StackEjectorExtended  , CapDispenser.StackEjectorRetracted) ^
            edge(CapDispenser.StackEjectorExtended  , CapDispenser.WorkpieceGripped)      ^
            edge(CapDispenser.WorkpieceGripped      , CapDispenser.LoaderPickedUp)        ^
            edge(CapDispenser.LoaderPickedUp        , CapDispenser.LoaderDroppedOff)      ^
            edge(CapDispenser.LoaderDroppedOff      , CapDispenser.StackEjectorExtended)
            )
      }
    }

    
    // KF Notes: 2. What happens when more caps are added to the tube? StackEmpty signal goes Low.
    //              This implies we need to include the sensor state as part of the node value.
    //              This would be a stateful process topology (see below)

    object Stateful
    {
      import SensorsInState._
    
      type FestoSensorsStatefulTopology = SensorsStatefulTopology[ID, FestoSignal, SpatialFestoConnection with TemporalFestoConnection]
    
      val sensorTopology: FestoSensorsStatefulTopology =
      {
        @inline def edge(s: FestoSensorInState, t: FestoSensorInState) = EdgeAnnotated(s, t, Some(ProcessSequence))
        
        BeGraphAnnotated[FestoSensorInState, SpatialFestoConnection with TemporalFestoConnection] (
            edge(CapDispenser.StackEjectorExtendedOn  , CapDispenser.StackEmptyOn)            ^
            edge(CapDispenser.StackEjectorExtendedOn  , CapDispenser.StackEjectorExtendedOff) ^
            edge(CapDispenser.StackEjectorExtendedOn  , CapDispenser.StackEjectorRetractedOn) ^
            edge(CapDispenser.StackEjectorExtendedOn  , CapDispenser.WorkpieceGrippedOn)      ^
            edge(CapDispenser.WorkpieceGrippedOn      , CapDispenser.LoaderPickedUpOn)        ^
            edge(CapDispenser.LoaderPickedUpOn        , CapDispenser.LoaderDroppedOffOn)      ^
            edge(CapDispenser.LoaderDroppedOffOn      , CapDispenser.WorkpieceGrippedOff)     ^
            edge(CapDispenser.WorkpieceGrippedOff     , CapDispenser.StackEjectorExtendedOn)
            )
      }
    }
  

}
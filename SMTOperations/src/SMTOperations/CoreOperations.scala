
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

package SMTOperations


import com.microsoft.z3._;
import BeSpaceDCore._;


class CoreOperations {
  
  // check if there is an overlap between two boxes
  def check2BoxSMT (c1x1 :Int, c1y1 : Int, c1x2 : Int, c1y2 : Int, c2x1 :Int, c2y1 : Int, c2x2 : Int, c2y2 : Int) : Boolean ={
    val retval : Boolean = true;
    
    val context :Context  = new Context();
    
    val x = context.mkSymbol("x");
    val y = context.mkSymbol("y");
    
    val xconst = context.mkIntConst(x);
    val yconst = context.mkIntConst(y);

    var s : Solver = null;

    try {
    
    val overlapCondition = (
    		context.mkAnd(context.mkLe(context.mkInt(c1x1),xconst), context.mkLe(xconst,context.mkInt(c1x2)),
    		context.mkLe(context.mkInt(c2x1),xconst), context.mkLe(xconst,context.mkInt(c2x2)),
    		context.mkLe(context.mkInt(c1y1),yconst), context.mkLe(yconst,context.mkInt(c1y2)),
    		context.mkLe(context.mkInt(c2y1),yconst), context.mkLe(yconst,context.mkInt(c2y2))
    		));
        s = context.mkSolver();

        s.add(overlapCondition);
        if (s.check() == Status.SATISFIABLE) println (s.getModel());

    } catch {
      case e : Z3Exception => println (e); return false;
    }
    return ((s.check() == Status.SATISFIABLE));
  }
  
  
  //seems ok, reviewed 28/08/2013: still seems ok
  //pairwise overlap check of boxlist, e.g., for comparing traces of robots
  def check2BoxListSMT (b1l: List[Invariant],b2l : List[Invariant]) : Boolean ={
      

    val retval : Boolean = true;
    
    val context :Context  = new Context();
    
    val x = context.mkSymbol("x"); // Will try to find one value of x that satisfies the context's conditions.
    val y = context.mkSymbol("y");
    
    val xconst = context.mkIntConst(x);
    val yconst = context.mkIntConst(y);

    var maincond = context.mkFalse();
    var s : Solver = null;
     //for ((b1,b2) <- List.map2(b1l,b2l) ((e1,e2) => (e1,e2))) {
    // changed 06/02/2015 not sure if good
    for ((b1,b2) <-  (b1l,b2l).zipped) {
            (b1,b2) match {
            	case (OccupyBox(c1x1, c1y1, c1x2, c1y2), OccupyBox(c2x1, c2y1, c2x2, c2y2)) => //println (b1 + " " + b2); 
            	    try {
                   val overlapCondition = context.mkAnd(context.mkLe(context.mkInt(c1x1),xconst), context.mkLe(xconst,context.mkInt(c1x2)),
                            context.mkLe(context.mkInt(c2x1),xconst), context.mkLe(xconst,context.mkInt(c2x2)),
                            context.mkLe(context.mkInt(c1y1),yconst), context.mkLe(yconst,context.mkInt(c1y2)),
                            context.mkLe(context.mkInt(c2y1),yconst), context.mkLe(yconst,context.mkInt(c2y2))
                            )
            	    	maincond = context.mkOr(maincond, overlapCondition);


            	    } catch {
            	    case e : Z3Exception => println (e); return false;
            	    }
            }
            
     }

     try {

    	 s = context.mkSolver();
    	 if (s.check() == Status.SATISFIABLE) println (s.getModel()) // Should NEVER be executed (hopefully)
       
    	 //println (maincond); // Can be HUGE
       
    	 s.add(maincond)  // Block until satisfied or determined not satisfiable.
     }
     catch
     {
       case e : Z3Exception => println (e)
       return false
     }

    return ((s.check() == Status.SATISFIABLE));
  }
  
}



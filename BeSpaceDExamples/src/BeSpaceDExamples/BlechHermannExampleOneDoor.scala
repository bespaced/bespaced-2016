/* 
 * Created 11/09/2013
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples
import BeSpaceDCore._;
object BlechHerrmannExampleOneDoor extends CoreDefinitions{

  val robotinvariantAbs1 = BIGAND ((OccupyBox(100,140,1100,160))::Nil);
  val robotinvariantAbs2 = BIGAND ((OccupyBox(100,140,1100,160))::(BIGOR((ComponentState("FAST"))::(ComponentState("SLOW"))::(ComponentState("STANDING"))::Nil))::Nil);
  

  


 
  def robotinvariantAbs1wt() : Invariant ={
	var rai : List[Invariant] = Nil;
	for (i <- 0 to 240) {
	  
	  rai ::= (IMPLIES(TimeStamp (i),(OccupyBox(100,140,1100,160))));
	}
	return (BIGAND (rai));    
  }
  
  def humanapproachingLeft() : Invariant ={
	var hali : List[Invariant] = Nil;
	for (i <- 0 to 240) {
	  hali ::= (IMPLIES(TimeStamp (i), (OccupyBox(0+(i/2),0,0+(i/2),300))));
	}
	return (BIGAND (hali));    
  }
 
  
  /*
   * normal behavior for a robot that moves from left to right
   */
  def robothumaninvariantPhysicsGenNormalBehavior(humanstarttimepoint : Int) : (Invariant,Invariant) ={
    val timeraster = 5.0; // in ms 
	var rai : List[Invariant] = Nil; //robotinvariant
    var hi : List[Invariant] = Nil; //humaninvariant

  	var speed : Double = 0; //robot speed
  	var d : Double = 0;
  	var speedh : Double = 0; //human speed
  	var dh : Double = 0; //human speed traveled
  	var hrdistance : Double =10000; //distance human robot
  	
  	var t : Int = 0;
  	var mode : Int = 3;
  	var nextmode : Int = 3;
  	var modetimer : Int = 0;
  	rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((99 + d).toInt,140,(121 + d).toInt,160))));

	while(t < 10000) {
	  if (mode == 3) { // green 
		  if (d <= 870) {
			  if (speed < 10) {
				  speed += (50.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
			  }
		  } else {
			  if (speed > 1) {
				  speed -= (50.0 / 1000.0) * timeraster;
			  }
		  }
	  }
	  
	  if (mode == 2) { // yellow 
		  if (d <= 870) {
			  if (speed > 2) {
				  speed -= (100.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
			  }
		  } else {
			  if (speed > 1) { // this case is a bit underspecified in the example document
				  speed -= (100.0 / 1000.0) / 10.0 * timeraster;
			  }
		  }
	  }
	  
	  if (mode == 1) { // red 
		if (speed > 0) {
			speed -= (150.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
		}  
	  }
		
	  t+=1;
	  d += (speed / 1000.0) * 10.0 * timeraster;
	  
	  if ((dh < 140) & (t >= humanstarttimepoint)) {
		  	speedh = 10.0;
		  } else {
		    speedh = 0.0;
		  }
		    
	  dh += (speedh / 1000.0) * 10.0 * timeraster;
	  rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((99 + d).toInt,140,(121 + d).toInt,160))));
	  hi ::= (IMPLIES(TimeStamp (t),(OccupyBox(500,(0 + dh).toInt,510,(1 + dh).toInt))));
	  hrdistance = Math.sqrt((140.0-(1.0+dh))* (140.0-(1.0+dh)) + (500.0-(1.0+d))* (500.0-(1.0+d))); //calculating the distance robot <-> human
	  
	  // handling the mode delay
	  if (modetimer == 0)
		 mode = nextmode;
	  if ((hrdistance < 250) && (nextmode == 3)) {
	    nextmode = 2;
	    modetimer = 100;
	  }
	  if ((hrdistance < 10) && ((nextmode == 3) || (nextmode == 2))) {
	    nextmode = 1;
	    if (modetimer < 0) // else, no modifications to modechange
	    	modetimer = 100;
	  }
	  
	  modetimer-=1;
	}
	return (BIGAND (rai),BIGAND(hi));    
  }
  
    def robotinvariantPhysicsGenNormalBehaviorCoarse() : Invariant ={
    val timeraster = 5.0; // in ms 
	var rai : List[Invariant] = Nil;
  	var speed : Double = 0;
  	var d : Double = 0;
  	var t : Int = 0;
  	rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((98 + d).toInt,139,(122 + d).toInt,161))));

	while(d < 980) {
	  if (d <= 870) {
		  if (speed < 10) {
			  speed += (50.0 / 1000.0) / 10.0 * timeraster; //acceleration / s -> ms / m -> m/10
		  }
	  } else {
		  if (speed > 1) {
			  speed -= (50.0 / 1000.0) / 10.0 * timeraster;
		  }
	  }
	  t+=1;
	  d += (speed / 1000.0) * 10.0 * timeraster ;
	  rai ::= (IMPLIES(TimeStamp (t),(OccupyBox((98 + d).toInt,139,(122 + d).toInt,161))));

	}
	return (BIGAND (rai));    
  }
  
  def main(args: Array[String]) {
    println("Topological Invariants:");
    println ("Geometric Invariants:");
    for ( i<- 0 to 240) {
    	println(prettyPrintInvariant(this.getSubInvariantForTime(i,simplifyInvariant(humanapproachingLeft)))+"   "+
    	    prettyPrintInvariant(this.getSubInvariantForTime(i,simplifyInvariant(robotinvariantAbs1wt))));
    	
  	}
    //println (this.robotinvariantPhysicsGenNormalBehavior);
    println("Normal Physics behavior");
    println(prettyPrintInvariant(this.getSubInvariantForTime(10,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>r})));
    
    //var t : Int = 0;
    for ( t <- 0 to 2000) {
          println(t*5 + "ms : " + this.getSubInvariantForTime(t,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>r}));

    }
    
     println (this.collisionTestsAlt1( 
            this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(1,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>r})))::Nil, 
            this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(1,(this.robothumaninvariantPhysicsGenNormalBehavior(1000)) match {case (r,h)=>h})))::Nil));
     
     var result : Boolean = false;
     var time = System.currentTimeMillis();
     
     
     
     for (tstart <- 600 to 620) {
    	 var rhi = this.robothumaninvariantPhysicsGenNormalBehavior(tstart);

    	 for (t <- 1000 to 1200) {
    		 result |=
    		 this.collisionTestsAlt1( 
    				 this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(t,(rhi) match {case (r,h)=>r})))::Nil, 
    				 this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(t,(rhi) match {case (r,h)=>h})))::Nil);
     
       
    	 }      
     }
     
      println("Time needed :"+ (System.currentTimeMillis() - time) + "milliseconds");
     println ("final result "+result);


    
       
    //println(this.getSubInvariantForTime(1,this.robotinvariantPhysicsGenNormalBehavior));


      //  println (this.collisionTestsAlt1( 
     //       this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invari()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bgeneration())))::Nil, 
    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2ageneration())))::Nil));
        
    //    println (this.collisionTestsAlt1( 
    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1aBIGANDgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant1bBIGANDgeneration())))::Nil, 
    //        this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2bBIGANDgeneration()))):: this.simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariant2aBIGANDgeneration())))::Nil));

    	//println (this.collisionTests( getSubInvariantForTime(i,invariant2bgeneration()) :: Nil, getSubInvariantForTime(i,invariant2bgeneration())::Nil));
    
  }
  
}

/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */


// 13/01/2013: needs fixing and second parameter

package Examples2D
import BeSpaceDCore._;
import java.awt.Rectangle
import BeSpaceD2D._;


//import java.util.Timer
object LiftingWorkpieceParameterized extends CoreDefinitions{

  


  
     //grapple hook C1

   def invariantR1(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(300,200+(i*speed).toInt,300,220+(i*speed).toInt,3)))	
	}
	for (i <- 0 to 100) {
	  if (i < stoppointup) {
		  inv ::= (IMPLIES(TimeStamp (i+101), 
				  OccupySegment(300,200+(100*speed).toInt-(i*speed).toInt,300,220+(100*speed).toInt-(i*speed).toInt,3)))	
	  } else {
		  inv ::= (IMPLIES(TimeStamp (i+101), 
				  OccupySegment(300,200+(100*speed).toInt-(stoppointup*speed).toInt,300,220+(100*speed).toInt-(stoppointup*speed).toInt,3)))	
	  }
	}
	return (BIGAND(inv));
   }
   
    def invariantR2(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(320,200+(i*speed).toInt,320,220+(i*speed).toInt,3)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment(320,200+(100*speed).toInt-(i*speed).toInt,320,220+(100*speed).toInt-(i*speed).toInt,3)))
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment(320,200+(100*speed).toInt-(stoppointup*speed).toInt,320,220+(100*speed).toInt-(stoppointup*speed).toInt,3)))	  	    
	  	  }
	
	}
	return (BIGAND(inv));
   }
    
   def invariantR3(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(300,200+(i*speed).toInt,320,200+(i*speed).toInt,3)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment(300,200+(100*speed).toInt-(i*speed).toInt,320,200+(100*speed).toInt-(i*speed).toInt,3)))  	    
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment(300,200+(100*speed).toInt-(stoppointup*speed).toInt,320,200+(100*speed).toInt-(stoppointup*speed).toInt,3)))  	  	    
	  	  }

	
	}
	return (BIGAND(inv));
   }
   
    def invariantR4(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(310,100,310,200+(i*speed).toInt,1)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) {  	    
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment(310,100,310,200+(100*speed).toInt-(i*speed).toInt,1)))	
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupySegment(310,100,310,200+(100*speed).toInt-(stoppointup*speed).toInt,1)))		  	    
	  	  }

	  	    
	}
	return (BIGAND(inv));
   }
    
   def invariantWP(speed : Float, stoppointup: Int) : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      //OccupyBox(100,100,20,20)))
	      OccupyBox(305,205 + (100*speed).toInt,315,215 + (100 * speed).toInt)))	
	}
	for (i <- 0 to 100) {
	  	  if (i < stoppointup) { 
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupyBox(305,200 + 5 + (100*speed).toInt-(i*speed).toInt,315,200 + 15 + (100 *speed).toInt-(i*speed).toInt)))	
	  	  } else {
	  		  inv ::= (IMPLIES(TimeStamp (i+101), 
	  				  OccupyBox(305,200 + 5 + (100*speed).toInt-(stoppointup*speed).toInt,315,200 + 15 + (100 *speed).toInt-(stoppointup*speed).toInt)))		  	    
	  	  }

	}
	return (BIGAND(inv));
   }
  
   // BehT for the Hook

   
  
  def behtcomp1def (speed:Float,stpu : Int) =
		  SimpleSpatioTemporalBehavioralType ("Hook", BIGAND(invariantR1(speed,stpu)::invariantR2(speed,stpu)::invariantR3(speed,stpu)::invariantR4(speed,stpu)::Nil), "H"::Nil , Map()("H" -> invariantR2(speed,stpu))) 
  
  
  def main(args: Array[String]) {
	val vis2d = new Visualization2D();
	vis2d.startup(null);
	println("x")

	for (i <- 0 to 200) {
	  Thread.sleep(100)
	  vis2d.setInvariant(BIGAND(
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR1(2.5f,15))))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR2(2.5f,15))))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR3(2.5f,15))))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR4(2.5f,15))))::

	      simplifyInvariant(getSubInvariantForTime(i,invariantWP(2.5f,15)))::

	      Nil
	  ))
	  vis2d.panel.repaint
	}

  }
}
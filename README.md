# BeSpaceD #

Introduction and Quick Start Guides.

### What is BeSpaceD ? ###

* BeSpaceD is a spatio-temporal logic language defined as an internal DSL in the Scala programming language.
* It's currently used in research context for experimenting with new spatio-tempral operators applied to the robotics and engineering industries.
* There is no formal versioning yet, but this repository is nominally for 2016 work.

### Subprojects

BeSpaceD has many subprojects. Each one has its own sub directory and most of them are valid eclipse projects. Here is a list of the important ones with a link to further subproject specific documentation:

|Subproject        |Description                                               |
|:---------------- |:-------------------------------------------------------- |
|BeSpaceDCore      | This defines the core language and operations of BeSpaceD.|
|BeSpaceDExamples  | This provides some examples of BeSpaceD usage and problem solving.|
|SMTOperations     | This provides an example mapping of BeSpaceD to SMT Solver [z3](https://github.com/Z3Prover/z3).|
|[BeSpaceDGestalt](BeSpaceDGestalt/README.md) | This provides infrastructure around BeSpaceD for networking, messaging and componentisation of BeSpaceD processing.|
|BeSpaceDData      | This provides facilities to capture and reload large BeSpaceD data sets.|
|data              | Contains a useful subset of actual BeSpaceD data sets for experimental use.|
|BeSpaceDKinect    | This provides tools for scanning 3D objects using a [Kinect](http://www.windowscentral.com/kinect) and converting the data into a BeSpaceD invariant.|
|BeSpaceDFesto     | This contains a detailed BeSpaceD model for a Festo Mini Factory and demonstrators that integrate this with ABB Service Port and run on Raspberry Pis.|
|BeSpaceD2D        | This provides a 2D visualisation layer for BeSpaceD in support of some of the examples. [Needs Updating to latest Java standards]|
|BeSpaceD3D        | This provides a 3D visualisation layer for BeSpaceD in support of some of the examples. [Needs Updating to latest Java standards]|
|BeSpaceDOPC       | This provides networking code for the [OPC-UA](https://opcfoundation.org/about/what-is-opc/) communications standard. [to be integrated into Gestalt]|

### How do I run a Quick Benchmark?

* Download the [BeSpaceDExample1 JAR file](https://bitbucket.org/bespaced/bespaced-2016/src/366106f39db621703358be90a176caca403af4eb/exports/BeSpaceDExample1.jar?at=master&fileviewer=file-view-default)
* Follow the instructions in the Raspberry Pi "Insallation and Benchmark Guide" documentation (below) starting at "Step 3" in the "BENCHMARKING (without BeSpaceD JAR)" section.
* Remember to add the VM argument "-Xss8m".

### How do I get set up for Development in Eclipse? ###

* Either
    * Download the latest version of Eclipse.
    * Install the Scala Plug-In.
* OR
    * Download the latest version of the Scala IDE (based on Eclipse).
* We are using Java 1.6 (For compatibility with Raspberry Pi) but 1.7 works and 1.8 should work but has not been tested.
* Clone this repository using git command line or inside eclipse.
* create a new separate project for your App or BeSpaceD extension.
* Add at least the BeSpaceDCore project as a project dependency to your own project.
* Other projects can be added on an as needed basis.

### How do I access the collection of BeSpaceD data sets?

* From your own BeSpaceD based App do the following:
    * Make sure you cloned the BeSpaceDData project in this repository.
    * Add the BeSpaceDData project as a dependency of your project.
    * Import BeSpaceDData._
    * In your code call one of the existing data set accessor methods:
        * Robotics.Lego.Trains.experiment1()
        * Robotics.Festo.MiniFactory.station1()
        * Weather.SmartSpace.Melbourne.Aug_27_2015()
        * Weather.SmartSpace.Melbourne.uvIndex_Dec_28_2015()   
        * Scan.Kinect.bottle()
        * Scan.Kinect.obstacles()
    * The above data sets are stored under the "data" directory in the repository.
    * The structure of the data is documented in a paper here:
        * [Example Data Sets and Collections for
BeSpaceD Explained](http://arxiv.org/pdf/1608.00433v1.pdf)
        
### How do I access very large BeSpaceD data sets?
* Large data sets are no longer stored in the git repository
* They are now available in the cloud here:
    * [Large BeSpaceD data sets](https://cloudstor.aarnet.edu.au/plus/index.php/s/BensqrGFRgSod9h)
* Download then copy the desired data set to the "data" directory in your git working set.
* In your code call one of the following with the file name as the ID:
    * loadOrThrow(some_data_set_id)
* For example:
    * loadOrThrow("aicause.kinect.scan.obstacles17")
    * loadOrThrow("aicause.kinect.scan.obstacles18")
* These methods return an Invariant object. The following functions may be used to get a readable form of the data set:
    * prettyPrintInvariant(inv)
    * toJson(inv)

### How do I deploy BeSpaceD Apps?

* Download the BeSpaceDXxxx JAR files that your app depends on. You will need:
    * [BeSpaceDCore](https://bitbucket.org/bespaced/bespaced-2016/downloads/BeSpaceDCore.jar)
* and optionally
    * [BeSpaceDOPC](https://bitbucket.org/bespaced/bespaced-2016/downloads/BeSpaceDOPC.jar)
    * [BeSpaceDFesto](https://bitbucket.org/bespaced/bespaced-2016/downloads/BeSpaceDFesto.jar)
    * [BeSpaceDData](https://bitbucket.org/bespaced/bespaced-2016/downloads/BeSpaceDData.jar)
    * [BeSpaceDKinect](https://bitbucket.org/bespaced/bespaced-2016/downloads/BeSpaceDKinect.jar)
* The JAR files will need to be on your class path when running Apps compiled using using BeSpaceD.

### How do I get set up for Raspberry Pi Deployment? ###

* See the Installation guide : [Installation and Benchmark Guide.txt](https://bitbucket.org/bespaced/bespaced-2016/src/0c06b93d3e09f875a70b711a4355078a45e8c294/RaspberryPi/Installation%20and%20Benchmark%20Guide.txt?at=master&fileviewer=file-view-default)

## Contribution guidelines ###

* Usually a fork is created for each project and you commit to there.
* Writing tests : We are using ScalaTest
* Code reviews can be done internally to your project team
* When you are ready to contribute your working features to the main repository make a [Pull Request](https://confluence.atlassian.com/bitbucket/work-with-pull-requests-223220593.html).
  We will review your changes then accept and/or give feedback. For more detailed information about this process see [Making a pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request).

### Who do I talk to? ###

* Currently [Keith Foster](mailto:keith.foster@rmit.edu.au) is the administrator and primary maintainer of the BeSpaceD code base.
* The BeSpaceD project is an initiative of AUICAUSE, RMIT University, Australia and the responsible researcher is [Jan Olaf Blech](http://jblech.net)

### Where is the Wiki? ###

* Click Wiki in the left hand side menu.
* Quick links:
    * [FAQ](../../wiki/FAQ)
    * [Case Studies](../../wiki/Case-Studies)

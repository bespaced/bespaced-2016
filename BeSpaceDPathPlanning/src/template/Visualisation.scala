package template

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.geometry._
import BeSpaceDCore._
import javax.imageio.ImageIO
import scalafx.embed.swing._
import scalafx.scene.image.WritableImage

//There might be more elegant and generic ways of dealing with multiple objects to be drawn at different points in the code. An idea is to use Platform.runLater
//https://homepages.thm.de/~hg51/Veranstaltungen/NVP_15_16/Aufgaben/nvpA-01.pdf

object Visualisation extends JFXApp {
//  def OccupyPointVis(points: List[OccupyPoint], colour: Color): Unit = {
//    val points : List[OccupyPoint] = MyParameters.points
    println(s"stage = $stage" + java.util.Calendar.getInstance.getTime)
    stage = new PrimaryStage {
      title = "Visualisation"
      val myScene = new Scene(1024, 1024) {
        val canvas = new Canvas(1024, 1024)
        val gc = canvas.graphicsContext2D
        var i : Int = 0
        while(i < VisParameters.list.size){
          gc.fill_=(VisParameters.list.apply(i)._2)
          VisParameters.list.apply(i)._1.foreach { x => gc.fillRect(x.x * VisParameters.magnifying, x.y * VisParameters.magnifying, 1 * VisParameters.magnifying, 1 * VisParameters.magnifying) }
          i = i + 1;
        }
        i = 0
        while(i < VisParameters.listBoxes.size){
          gc.fill_=(VisParameters.listBoxes.apply(i)._2)
          val box: OccupyBox = VisParameters.listBoxes.apply(i)._1
          box match{
            case OccupyBox(x1,y1,x2,y2) => gc.fillRect(x1 * VisParameters.magnifying, y1 * VisParameters.magnifying, (x2 - x1) * VisParameters.magnifying, (y2 - y1) * VisParameters.magnifying)
          }
          i = i + 1
        }
//        gc.fill_=(MyParameters.colour)
//        points.foreach { x => gc.fillRect(x.x, x.y, 1, 1) }
        content = canvas
        println("set the canvas at " + java.util.Calendar.getInstance.getTime)
      }
      scene = myScene
      var myImage: WritableImage = new WritableImage(1024, 1024)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File("output.png"))
    }
//  }

}
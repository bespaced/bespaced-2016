package template

import scala.collection.mutable.HashMap
import scala.annotation.tailrec

/**
 * @author https://rosettacode.org/wiki/Dijkstra%27s_algorithm#Scala
 * GNU Free Documentation License 1.2
 *
 */
object Dijkstra {

  type Path[Key] = (Double, List[Key])

  def Dijkstra[Key](lookup: Map[Key, List[(Double, Key)]], fringe: List[Path[Key]], dest: Key, visited: Set[Key]): Path[Key] = fringe match {
    case (dist, path) :: fringe_rest => path match {
      case key :: path_rest =>
        if (key == dest) (dist, path.reverse)
        else {
          val paths = lookup(key).flatMap { case (d, key) => if (!visited.contains(key)) List((dist + d, key :: path)) else Nil }
          val sorted_fringe = (paths ++ fringe_rest).sortWith { case ((d1, _), (d2, _)) => d1 < d2 }
          Dijkstra(lookup, sorted_fringe, dest, visited + key)
        }
    }
    case Nil => (0, List())
  }

  /*Added by Carl*/
  def DijkstraSearch[Key](graph: FirstGraph[Key], start: Key, target: Key): (List[Key], Double) = {
    val lookup: Map[Key, List[(Double, Key)]] = graph.getNodes().map { x =>
      val outEdges: List[Key] = graph.successors(x)
      (x, outEdges.map { y => (graph.weight(x, y), y) })
    }.toMap
    val output = Dijkstra[Key](lookup, List((0, List(start))), target, Set())
    return (output._2, output._1)
  }

  def DijkstraIterative[Key](graph: FirstGraph[Key], start: Key, target: Key): (List[Key], Double) = {
    //Initialisation
    val allNodes: List[Key] = graph.getNodes()
    val distanceToStart: HashMap[Key, Double] = new HashMap[Key, Double]
    val parentNode: HashMap[Key, Key] = new HashMap[Key, Key]
    val processed: HashMap[Key, Boolean] = new HashMap[Key, Boolean]
    //    allNodes.foreach { x => distanceToStart.+=((x, Int.MaxValue)) }
    def comparator(a: Key, b: Key): Int = if (distanceToStart.isDefinedAt(a) && distanceToStart.isDefinedAt(b)) return (distanceToStart.get(a).get - distanceToStart.get(b).get).signum else return 0
    val open = new BinaryMinHeap[Key](comparator)
    distanceToStart.+=((start, 0))
    open.+=(start)
    //Actual Algorithm
    var targetProcessed = false
    while (!open.isEmpty && !targetProcessed) {
      val s = open.dequeue
      processed.+=((s, true))
      val outEdges = graph.successors(s).filter { x => processed.get(x).isEmpty }
      outEdges.foreach { t =>
        {
          val pr = distanceToStart.get(s).get + graph.weight(s, t)
          if (!distanceToStart.get(t).isDefined) {
            distanceToStart.+=((t, pr))
            parentNode.+=((t, s))
            open.+=(t)
          } else if (open.contains(t) && pr < distanceToStart.get(t).get) {
            distanceToStart.+=((t, pr))
            parentNode.+=((t, s))
            open.reevaluateElement(t)
          }

        }
      }
      if (s == target) {
        targetProcessed = true
      }
    }
    
    @tailrec
    def path(end: Key, currentPath: List[Key]): List[Key] = {
      val predecessor: Option[Key] = parentNode.get(end)
      predecessor match {
        case None    => return end :: currentPath
        case Some(d) => return path(d, end :: currentPath)
      }
    }
    def pathLength(g: FirstGraph[Key], l: List[Key]): Double = {
      l match {
        case Nil                    => return 0.0
        case head :: Nil            => return 0.0
        case head1 :: head2 :: tail => return g.weight(head1, head2) + pathLength(g, head2 :: tail)
      }
    }
    
    if (parentNode.get(target).isEmpty) {
      return (Nil, 0.0)
    }

    val pathToTarget = path(target, Nil)
    val length = pathLength(graph, pathToTarget)
    return (pathToTarget, length)

    
  }
}
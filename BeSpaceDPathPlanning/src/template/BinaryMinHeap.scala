package template

import scala.collection.mutable.HashMap

/**
 * @author Carl
 *
 * I implemented this BinaryMinHeap largely following Christian Rössl's slides Algorithmen und Datenstrukturen 2014, lecture 5: Heaps, Otto-von-Guericke Universität Magdeburg.
 * The advantage over the standard scala.collections.mutable.PriorityQueue is that single elements can be reevaluated on demand.
 * Another advantage is that it takes a heuristic (comparator) as an option, so elements can be ordered in a different fashion in different BinaryMinHeaps.
 * Inplements backlinking (bidirectional linking), such that operations perform efficiently even if only the element and not its index is known.
 * Note: It only works if every element occurs only once in the heap. Otherwise, the backlinking that enables efficient reevaluation if only the element is known doesn't work and throws an error.
 * @param <Element>
 */
class BinaryMinHeap[Element/* <% Ordered[Element]*/](ordering: (Element, Element) => Int) extends Traversable[Element]{
  //The underlying structure is a HashMap, so it supports quick operations to write at or retrieve from a known index, and also quick resizing.
  val heap: HashMap[Int, Element] = new HashMap[Int, Element]
  //The heap explicitly stores a 
  val reverseMap: HashMap[Element, Int] = new HashMap[Element, Int]
  //Convenience functions for compatibility
  override def isEmpty: Boolean = heap.isEmpty
  override def head: Element = peak.get
  def += (x: Element) = insert(x)
  def dequeue: Element = {
    val output = remove
    return output match{
      case None => throw new NoSuchElementException
      case Some(d) => d
    }
  }
  def enqueue(x: Element) = +=(x)
  def contains(x: Element): Boolean = heap.find(p => p._2 == x).isDefined//This only works if the two Elements are from the same definition.
  //To implement Traversable, not sure if this actually works.
  def foreach[U](f: Element ⇒ U): Unit = {
    heap.foreach{
      kv => f(kv._2)
    }
  }
  //Some nice relationship functions
  def parent(k: Int) = heap.get(k / 2)
  def children(k: Int): List[Element] = {
    return (leftChild(k) :: rightChild(k) :: Nil).filter { x => x.isDefined }.map { x => x.get }
  }
  def leftChild(k: Int): Option[Element] = heap.get(k * 2)
  def rightChild(k: Int): Option[Element] = heap.get(k * 2 + 1)
  def leftSubTree(k: Int): HashMap[Int, Element] = {
    val list: List[Int] = leftSubTreeList(k)
    val output: HashMap[Int, Element] = new HashMap[Int, Element]
    list.foreach { x => output.+=((x, heap.get(x).get)) }
    return output
  }
  def leftSubTreeList(k: Int): List[Int] = {
    if (leftChild(k).isEmpty) return Nil
    return k * 2 :: leftSubTreeList(k * 2) ++ rightSubTreeList(k * 2)
  }
  def rightSubTreeList(k: Int): List[Int] = {
    if (rightChild(k).isEmpty) return Nil
    return k * 2 + 1 :: leftSubTreeList(k * 2 + 1) ++ rightSubTreeList(k * 2 + 1)
  }
  def rightSubtree(k: Int): HashMap[Int, Element] = {
    val list: List[Int] = rightSubTreeList(k)
    val output: HashMap[Int, Element] = new HashMap[Int, Element]
    list.foreach { x => output.+=((x, heap.get(x).get)) }
    return output
  }
  def insert(x: Element) = {
    val n: Int = heap.size + 1
    writeToHeap(n, x)
    //    heap.+=((n, x))
    upheap(n)
  }
  def upheap(k1: Int) = {
    val xOption: Option[Element] = heap.get(k1)
    if (xOption.isDefined) {
      val x: Element = xOption.get
      var k: Int = k1
      while (k > 1 && (ordering(x, heap(k / 2))) < 0) {
        val cache: Element = heap.get(k / 2).get
        removeFromHeap(k / 2)
        writeToHeap(k, cache)
        //        heap.+=((k, heap.get(k/2).get))//This should be defined at this point, because k is already defined.
        k = k / 2
      }
      writeToHeap(k, x)
      //      heap.+=((k, x))
    }
  }
  //Returns the smallest element and removes it.
  def remove: Option[Element] = {
    if (heap.isEmpty) return None
    if (heap.size == 1){
      val cache: Option[Element] = heap.get(1)
      clear
      return cache
    }
    val x: Element = heap.get(1).get
    val n: Int = heap.size
    val newTop: Element = heap.get(n).get
    removeFromHeap(n)
    writeToHeap(1, newTop)
    //    heap.+=((1, heap.get(n).get))
    
    downheap(1)
    return Some(x)
  }
  //Returns the smallest element without removing it.
  def peak: Option[Element] = {
    return heap.get(1)
  }
  def dequeueAll: List[Element] = {
    val head: Option[Element] = remove
    head match {
      case None    => return Nil
      case Some(d) => return d :: dequeueAll
    }
  }
  def addAll(elements: Traversable[Element]) = {
    elements.foreach { x => insert(x) }
  }
  def downheap(k1: Int) = {
    if (!heap.isEmpty) {
      var k = k1
      val n = heap.size
      val x: Element = heap.get(k).get
      var loopFlag: Boolean = true
      while (k <= n / 2 && loopFlag) {
        var j: Int = 2 * k
        if (j < n && ordering(heap.get(j).get, heap.get(j + 1).get) > 0) {
          j = j + 1
        }
        if (ordering(x, heap.get(j).get) <= 0) {
          loopFlag = false
        } else {
          val cache: Element = heap.get(j).get
          removeFromHeap(j)
          writeToHeap(k, cache)
          //        heap.+=((k, heap.get(j).get))
          k = j
        }
      }
      writeToHeap(k, x)
      //    heap.+=((k, x))
    }
  }
  def replace(x: Element): Element = {
    val r: Element = heap.get(1).get
    writeToHeap(1, x)
    //    heap.+=((1, x))
    if (ordering(x, r) > 0) {
      downheap(1)
    }
    return r
  }
  def writeToHeap(k: Int, x: Element) = {
    assert(heap.size == reverseMap.size)
    if(heap.get(k).isDefined){
      reverseMap.-=(heap.get(k).get)
    }
    heap.+=((k, x))
    reverseMap.+=((x, k))
//    println(heap.size)
//    println(reverseMap.size)
    assert(heap.size == reverseMap.size)
  }
  def removeFromHeap(k: Int) = {
    assert(heap.size == reverseMap.size)
    val element: Option[Element] = heap.get(k)
    element match {
      case None =>
      case Some(d) => {
        heap.-=(k)
        reverseMap.-=(d)
      }
    }
    assert(heap.size == reverseMap.size)
  }
  //Reevaluate should be called when the priority of a certain element has changed, so its position in the heap is updated.
  def reevaluate(k: Int) = {
    upheap(k)
    downheap(k)
  }
  def reevaluateElement(x: Element) = {
    find(x) match {
      case None    =>
      case Some(d) => reevaluate(d)
    }
  }
  //Refresh rebuilds the heap in a bottom-up fashion.
  def refresh = {
    var i: Int = heap.size / 2
    while (i > 0) {
      downheap(i)
      i = i - 1
    }
  }
  def find(x: Element): Option[Int] = {
    val index: Option[Int] = reverseMap.get(x)
    index match {
      case None    =>
      case Some(d) => assert(heap.get(d).isDefined && heap.get(d).get == x) //Just to be sure the concept of mapping in both directions (or its implementation) works.
    }
    return index
  }
  def clear = {
    heap.clear()
    reverseMap.clear()
  }
  /*def findIndex(x: Element): Option[Int] = {
    return findIndexHelper(x, 1, heap.size)
  }
  def findIndexHelper(x: Element, a: Int, b: Int): Option[Int] = {
    assert(heap.get(a).isDefined && heap.get(b).isDefined)
    if(x < heap.get(a).get) return None
    
    ???
  }*/
}
package template

import BeSpaceDCore._

class ConvexHullSpec extends UnitSpec{
  "Turn" should "say right turn for (170,120), (100,250), (20,190)" in
  {
    val hull = new ConvexHull()
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(100,250)
    var c = OccupyPoint(20,190)
    var result : String = hull.Turn(a,b,c)
    
    assertResult(expected = "right turn")(actual = result )
  }
  it should "say left turn for (170,120), (20,190), (100,250)" in
  {
    val hull = new ConvexHull()
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(100,250)
    var c = OccupyPoint(20,190)
    var result : String = hull.Turn(a,c,b)
    
    assertResult(expected = "left turn")(actual = result )
  }
  it should "say no turn for (170,120), (190,130), (230,150)" in
  {
    val hull = new ConvexHull()
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(190,130)
    var c = OccupyPoint(230,150)
    
    var result : String = hull.Turn(a,b,c)
    
    assertResult(expected = "no turn")(actual = result)
  }
  it should "say no turn for (170,120), (190,130), (190,130)" in
  {
    val hull = new ConvexHull()
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(190,130)
    var c = OccupyPoint(190,130)
    
    var result : String = hull.Turn(a,b,c)
    
    assertResult(expected = "no turn")(actual = result)
  }
  it should "say no turn for (170,120), (190,130), (170,120)" in
  {
    val hull = new ConvexHull()
    var a = OccupyPoint(170,120)
    var b = OccupyPoint(190,130)
    var c = OccupyPoint(170,120)
    
    var result : String = hull.Turn(a,b,c)
    
    assertResult(expected = "no turn")(actual = result)
  }
  "ConvexHull" should "find the hull counterclockwise" in{
    val hull = new ConvexHull()
    val points = List(OccupyPoint(2,4), OccupyPoint(3,7), OccupyPoint(4,2), OccupyPoint(5,9), OccupyPoint(7,5), OccupyPoint(4,7), OccupyPoint(5,1), OccupyPoint(4,1))
    val howItShouldBe = List(OccupyPoint(2,4), OccupyPoint(4,1), OccupyPoint(5,1), OccupyPoint(7,5), OccupyPoint(5,9), OccupyPoint(3,7)).reverse
    val result = hull.convexHull(points)
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
  it should "revert a clockwise hull into a counterclockwise one" in {
    val hull : ConvexHull = new ConvexHull()
    val input : List[OccupyPoint] = List(OccupyPoint(1,29), OccupyPoint(6,23), OccupyPoint(99,23), OccupyPoint(99,77), OccupyPoint(83,79), OccupyPoint(16,79), OccupyPoint(4,76), OccupyPoint(1,71))
    val howItShouldBe : List[OccupyPoint] = input.reverse
    val result : List[OccupyPoint] = hull.convexHull(input)
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
  "enforceCCW" should "reverse a clockwise list" in{
    val hull : ConvexHull = new ConvexHull()
    val input : List[OccupyPoint] = List(OccupyPoint(1,29), OccupyPoint(6,23), OccupyPoint(99,23), OccupyPoint(99,77), OccupyPoint(83,79), OccupyPoint(16,79), OccupyPoint(4,76), OccupyPoint(1,71))
    val howItShouldBe : List[OccupyPoint] = input.reverse
    val result : List[OccupyPoint] = hull.enforceCCW(input)
    
    assertResult(expected = howItShouldBe)(actual = result)
  }
}
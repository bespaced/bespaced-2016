package template

import scalafx.stage.FileChooser
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.geometry._
import BeSpaceDCore._
import javax.imageio.ImageIO
import scalafx.embed.swing._
import scalafx.scene.image.WritableImage
import java.io._
import BeSpaceDData._
import z3test.test1
import com.microsoft.z3._
import scala.collection.mutable.HashMap
import scala.io.Source

object Unifier extends JFXApp {
  /**
   * Asks the user to choose a directory, then returns (as an option)
   * an array of the files in that directory.
   *
   * @param title What to put in the file chooser dialog title bar.
   * @return The files in the chosen directory.
   */
  val files: List[File] = getFiles.toList
  val csvs: List[File] = files.filter { x => x.getName.endsWith("twoLines.csv") }
//  csvs.foreach { x => println(x) }
  val lastLines = csvs.map { x => Source.fromFile(x).getLines().toList.last }
//  lastLines.foreach { x => println(x) }
  val lastLinesRepaired = lastLines.map { x =>
    val startHeuristic: Int = x.indexOf("heuristic")
    val endHeuristic: Int = x.indexOf(")", startHeuristic)
    val brokenString = x.substring(startHeuristic, endHeuristic)
    val repairedString = brokenString.replaceAll(",", "|")
    val repairedLine = x.substring(0, startHeuristic) + repairedString + x.substring(endHeuristic, x.length())//x.replaceAll(brokenString, repairedString)
//    println(repairedLine)
    repairedLine
  }
//  lastLinesRepaired.foreach { x => println(x) }
  val titleLine = csvs match{
    case head::tail => Source.fromFile(head).getLines().toList.head
    case _ => ""
  }
  val newFile = saveFile
  val pw = new PrintWriter(newFile)
  pw.write(titleLine + '\n')
  lastLinesRepaired.foreach { x => pw.write(x + '\n') }
  pw.close()

  def getFiles: Seq[File] = {
    val fileChooser = new FileChooser
    val selectedFiles = fileChooser.showOpenMultipleDialog(stage)
    val actualFiles = selectedFiles.filterNot { x => x == null }
//    actualFiles.foreach { x => println(x) }
    return actualFiles
  }
  def saveFile: File = {
    val fileChooser = new FileChooser
    val selectedFile = fileChooser.showSaveDialog(stage)
    return selectedFile
  }
}
package template

import BeSpaceDCore._

case class InvariantObstacle(inv : Invariant) extends AbstractPolygon {
  val core = standardDefinitions; import core._
  
  def parameter = inv
  
  def collision(point : OccupyPoint) : Boolean = {
    return collisionTestsBig(unfoldInvariant(inv) :: Nil, unfoldInvariant(point) :: Nil)
  }
  
  def convexHull(): Polygon = {
    return new Polygon(new ConvexHull().convexHull(vis()))
  }
  
  def vis() : List[OccupyPoint] = {
    val something = simplifyInvariant(unfoldInvariant(inv))
    val list : List[OccupyPoint] = something match {
      case BIGAND(x : List[OccupyPoint]) => x
      case AND(OccupyPoint(a,b),OccupyPoint(c,d)) => List(OccupyPoint(a,b),OccupyPoint(c,d))
      case OccupyPoint(a,b) => List(OccupyPoint(a,b))
      case _ => Nil
    }
    return list
  }
  
//  override def toString() : String = {
//    inv.toString()
//  }
  
  def equivalencePoly(that: Polygon): Boolean = {
    return this.vis() == that.vis()
  }
  def equivalenceInv(that: InvariantObstacle): Boolean = {
    return this.inv == that.parameter
  }
  
  def overlapInv(that: InvariantObstacle): Boolean = {
    return collisionTestsBig(unfoldInvariant(inv) :: Nil, unfoldInvariant(that.parameter) :: Nil)
  }
  def overlapPoly(that: Polygon): Boolean = {
    return collisionTestsBig(unfoldInvariant(inv) :: Nil, unfoldInvariant(BIGAND(that.getCornerPoints())) :: Nil)
  }
  def getBorder: List[OccupyPoint] = {
    inv match{
      case OccupyPoint(x,y) => List(OccupyPoint(x,y))
      case OccupyBox(x1, y1, x2, y2) => vis().filter { x => x match{
          case OccupyPoint(x,y) => (x == x1 || x == x2) && (y == y1 || y == y2)
          case _ => false
        }
      }
      case Occupy3DPoint(x,y,z) => return List(OccupyPoint(x,y))
      case something => Nil
    }
  }
}
package template

import scalafx.scene.paint.Color
import BeSpaceDCore._

object VisParameters {
  var list : List[(List[OccupyPoint],Color)] = Nil
  var listBoxes: List[(OccupyBox, Color)] = Nil
  var magnifying : Double = 6.0
}
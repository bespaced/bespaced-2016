package template

import BeSpaceDCore._
import scala.collection.mutable.HashMap
import scala.collection.mutable.PriorityQueue
import scala.annotation.tailrec
import java.io._
import z3test.test1
import com.microsoft.z3._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.geometry._
import javax.imageio.ImageIO
import scalafx.embed.swing._
import scalafx.scene.image.WritableImage

/**
 * @author Carl
 * I assume the initial size is a power of 2.
 *
 */
case class FirstQuadTree(points: List[Occupy3DPointDouble], size: Int, empty: Double, full: Double) {

  val startField: Cell = Cell(points, 0, 0, size, Nil)
  def leaves: List[Cell] = listCells(recursiveRefinement(startField))
  def recursiveRefinement(current: Cell): Cell = {
    val size: Int = current.points.size
    if (current.sideLength > 1 && current.density > empty && current.density < full) {
      val refinedCell: Cell = refinement(current)
      return Cell(refinedCell.points, refinedCell.xMin, refinedCell.yMin, refinedCell.sideLength,
        refinedCell.branches.map { x => recursiveRefinement(x) })
    }
    return current
  }

  //I assume a Cell to be refined does not have any branches yet.
  def refinement(current: Cell): Cell = {
    val newResolution: Int = current.sideLength / 2

    val xMid: Int = current.xMin + current.sideLength / 2
    val yMid: Int = current.yMin + current.sideLength / 2
    //    var contents: HashMap[(Int, Int), List[Occupy3DPointDouble]] = new HashMap[(Int, Int), List[Occupy3DPointDouble]]
    //Not sure if I should include a "distinct" here.
    val lowLeft: List[Occupy3DPointDouble] = current.points.filter { x => x.x <= xMid && x.y <= yMid }
    val lowRight: List[Occupy3DPointDouble] = current.points.filter { x => x.x > xMid && x.y <= yMid }
    val upRight: List[Occupy3DPointDouble] = current.points.filter { x => x.x > xMid && x.y > yMid }
    val upLeft: List[Occupy3DPointDouble] = current.points.filter { x => x.x <= xMid && x.y > yMid }

    val output: List[Cell] = List(
      Cell(lowLeft, current.xMin, current.yMin, newResolution, Nil),
      Cell(lowRight, xMid, current.yMin, newResolution, Nil),
      Cell(upRight, xMid, yMid, newResolution, Nil),
      Cell(upLeft, current.xMin, yMid, newResolution, Nil))
    return Cell(current.points, current.xMin, current.yMin, current.sideLength, output)
  }
  def listCells(start: Cell): List[Cell] = {
    return if (start.branches.isEmpty) start :: Nil else start.branches.flatMap { x => listCells(x) }
  }
}

case class Cell(points: List[Occupy3DPointDouble], xMin: Int, yMin: Int, sideLength: Int, branches: List[Cell]) {
  val density: Double = points.distinct.length.toDouble / (sideLength * sideLength).toDouble
  val numPoints: Int = points.distinct.length
}

trait heuristicEnum
case class heuristic1(density: Double, size: Double) extends heuristicEnum //DensitySize
case class heuristic2(bonus1Bridgehead: Double, bonus2Bridgeheads: Double, bonusAdjacentComponent: Double, density: Double, size: Double) extends heuristicEnum //BridgeHead
case class heuristic3(angle: Double, density: Double, size: Double) extends heuristicEnum //AngleDensitySize
case class heuristic4(bonus: Double, density: Double, size: Double) extends heuristicEnum //Bridgepath
case class heuristic5() extends heuristicEnum //Density
case class heuristic6() extends heuristicEnum //Angle

trait direction
case class up() extends direction
case class low() extends direction
case class left() extends direction
case class right() extends direction

case class TreeController(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double, unionFind: DisjointSet[SecondQuadTree], minSize: Int, start: OccupyPoint, target: OccupyPoint, ordering: heuristicEnum, densityQuotientForPostProcessing: Double) {
  var outputImages: List[Image] = Nil
  def h1(): Boolean = ordering match {
    case heuristic1(_, _) => true
    case _                => false
  }
  def h2(): Boolean = ordering match {
    case heuristic2(_, _, _, _, _) => true
    case _                         => false
  }
  def h3(): Boolean = ordering match {
    case heuristic3(_, _, _) => true
    case _                   => false
  }
  def h4(): Boolean = ordering match{
    case heuristic4(_, _, _) => true
    case _ => false
  }
  def h5(): Boolean = ordering == heuristic5()
  def h6(): Boolean = ordering == heuristic6()
  val showIntermediateSteps: Boolean = false
  val parents: HashMap[SecondQuadTree, SecondQuadTree] = new HashMap[SecondQuadTree, SecondQuadTree]
  val branches: HashMap[SecondQuadTree, treeLayer] = new HashMap[SecondQuadTree, treeLayer]
  var startNodeSave: Option[SecondQuadTree] = None
  var targetNodeSave: Option[SecondQuadTree] = None
  val adjacentToStartOrTargetComponents: HashMap[SecondQuadTree, Int] = new HashMap[SecondQuadTree, Int]

  def leftFreeCells(node: SecondQuadTree): List[SecondQuadTree] = {
    if (node.density <= node.empty) return List(node)
    else {
      val children: Option[treeLayer] = branches.get(node)
      val leftChildren: List[SecondQuadTree] = children match {
        case None    => Nil
        case Some(d) => List(d.lowLeft, d.upLeft)
      }
      val output: List[SecondQuadTree] = leftChildren.flatMap { x => leftFreeCells(x) }
      return output
    }
  }
  def lowFreeCells(node: SecondQuadTree): List[SecondQuadTree] = {
    if (node.density <= node.empty) return List(node)
    else {
      val children: Option[treeLayer] = branches.get(node)
      val lowChildren: List[SecondQuadTree] = children match {
        case None    => Nil
        case Some(d) => List(d.lowLeft, d.lowRight)
      }
      val output: List[SecondQuadTree] = lowChildren.flatMap { x => lowFreeCells(x) }
      return output
    }
  }
  def rightFreeCells(node: SecondQuadTree): List[SecondQuadTree] = {
    if (node.density <= node.empty) return List(node)
    else {
      val children: Option[treeLayer] = branches.get(node)
      val rightChildren: List[SecondQuadTree] = children match {
        case None    => Nil
        case Some(d) => List(d.lowRight, d.upRight)
      }
      val output: List[SecondQuadTree] = rightChildren.flatMap { x => rightFreeCells(x) }
      return output
    }
  }
  def upFreeCells(node: SecondQuadTree): List[SecondQuadTree] = {
    if (node.density <= node.empty) return List(node)
    else {
      val children: Option[treeLayer] = branches.get(node)
      val upChildren: List[SecondQuadTree] = children match {
        case None    => Nil
        case Some(d) => List(d.upLeft, d.upRight)
      }
      val output: List[SecondQuadTree] = upChildren.flatMap { x => upFreeCells(x) }
      return output
    }
  }
  def freeNeighbours(node: SecondQuadTree): List[SecondQuadTree] = {
    val up: Option[SecondQuadTree] = upNeighbour(node)
    val low: Option[SecondQuadTree] = lowNeighbour(node)
    val left: Option[SecondQuadTree] = leftNeighbour(node)
    val right: Option[SecondQuadTree] = rightNeighbour(node)
    val upNeighbourList: List[SecondQuadTree] = up match {
      case None    => Nil
      case Some(d) => lowFreeCells(d)
    }
    val lowNeighbourList: List[SecondQuadTree] = low match {
      case None    => Nil
      case Some(d) => upFreeCells(d)
    }
    val leftNeighbourList: List[SecondQuadTree] = left match {
      case None    => Nil
      case Some(d) => rightFreeCells(d)
    }
    val rightNeighbourList: List[SecondQuadTree] = right match {
      case None    => Nil
      case Some(d) => leftFreeCells(d)
    }
    return upNeighbourList ++ lowNeighbourList ++ leftNeighbourList ++ rightNeighbourList
  }
  def sideLeaves(node: SecondQuadTree, o: direction): List[SecondQuadTree] = {
    val children: Option[treeLayer] = branches.get(node)
    val sideChildren: List[SecondQuadTree] = children match {
      case None => return List(node)
      case Some(d) => o match {
        case up()    => List(d.upRight, d.upLeft)
        case low()   => List(d.lowLeft, d.lowRight)
        case left()  => List(d.lowLeft, d.upLeft)
        case right() => List(d.lowRight, d.upRight)
      }
    }
    val output: List[SecondQuadTree] = sideChildren.flatMap { x => sideLeaves(x, o) }
    return output
  }
  def neighbours(node: SecondQuadTree): List[SecondQuadTree] = {
    val upN: Option[SecondQuadTree] = upNeighbour(node)
    val lowN: Option[SecondQuadTree] = lowNeighbour(node)
    val leftN: Option[SecondQuadTree] = leftNeighbour(node)
    val rightN: Option[SecondQuadTree] = rightNeighbour(node)
    val upNeighbourList: List[SecondQuadTree] = upN match {
      case None    => Nil
      case Some(d) => sideLeaves(d, low())
    }
    val lowNeighbourList: List[SecondQuadTree] = lowN match {
      case None    => Nil
      case Some(d) => sideLeaves(d, up())
    }
    val leftNeighbourList: List[SecondQuadTree] = leftN match {
      case None    => Nil
      case Some(d) => sideLeaves(d, right())
    }
    val rightNeighbourList: List[SecondQuadTree] = rightN match {
      case None    => Nil
      case Some(d) => sideLeaves(d, left())
    }
    return upNeighbourList ++ lowNeighbourList ++ leftNeighbourList ++ rightNeighbourList
  }
  def mixedNeighbours(node: SecondQuadTree): List[SecondQuadTree] = {
    val n = neighbours(node)
    return n.filter { x => x.density > x.empty && x.density < x.full }
  }

  val pathCells: HashMap[SecondQuadTree, Int] = new HashMap[SecondQuadTree, Int] //This just serves as storing which nodes are currently on the path. The Int is probably not useful otherwise.

  //The following two functions make sure the leafs containing start and target node are computed only once - not every time a heuristic calls them.
  def startNode: SecondQuadTree = startNodeSave match {
    case None => {
      val snInit: SecondQuadTree = directedLeaf(root, start)
      val sn: SecondQuadTree = if (snInit == root) startTree(root, start, minSize)._2 else snInit //Hoped to be useful if startNode is called before startTree.
      startNodeSave = Some(sn)
      //      val prio: Double = -heuristicDistance(sn, targetNode) //rough estimate designed to always underestimate the distance. Negative because of how the priorityQueue works.
      //      startComponent.+=(sn)
      return sn
    }
    case Some(d) => d
  }
  def targetNode: SecondQuadTree = targetNodeSave match {
    case None => {
      val tnInit: SecondQuadTree = directedLeaf(root, target)
      val tn: SecondQuadTree = if (tnInit == root) startTree(root, target, minSize)._2 else tnInit //Hoped to be useful if targetNode is called before startTree.
      targetNodeSave = Some(tn)
      //      val prio: Double = -heuristicDistance(tn, startNode) //rough estimate designed to always underestimate the distance. Negative because of how the priorityQueue works.
      //      targetComponent.+=(tn)
      return tn
    }
    case Some(d) => d
  }
  def closerToStart(a: SecondQuadTree, b: SecondQuadTree): Int = (heuristicDistance(a, startNode) - heuristicDistance(b, startNode)).signum
  def closerToTarget(a: SecondQuadTree, b: SecondQuadTree): Int = (heuristicDistance(a, targetNode) - heuristicDistance(b, targetNode)).signum
  val startComponent: BinaryMinHeap[SecondQuadTree] = new BinaryMinHeap[SecondQuadTree](closerToTarget)
  val targetComponent: BinaryMinHeap[SecondQuadTree] = new BinaryMinHeap[SecondQuadTree](closerToStart)

  val heuristic: ((SecondQuadTree, SecondQuadTree) => Double) = ordering match {
    case heuristic1(d, s)    => densitySizeHeuristic(d, s)
    case heuristic2(b1, b2, ba, d, s)        => bridgeheadDensitySizeHeuristic(b1, b2, ba, d, s)
    case heuristic3(a, d, s) => angleDensitySizeHeuristic(a, d, s)
    case heuristic4(b, d, s)        => BridgePathHeuristic(b, d, s)
    case heuristic5()        => densityHeuristic
    case heuristic6()        => angleHeuristic
  }
  //generateEmptyMixedCellGraph generates a graph with all free and mixed cells with their adjacency relations. Free to free costs 0, else 1. 
  def generateEmptyMixedCellGraph: FirstGraph[SecondQuadTree] = {
    val g: FirstGraph[SecondQuadTree] = new FirstGraph[SecondQuadTree]
    val cells: List[SecondQuadTree] = leaves(root).filter { x => x.density < x.full && (x.size > minSize || x.density < x.empty) }
    cells.foreach { x =>
      val up: Option[SecondQuadTree] = upNeighbour(x)
      val low: Option[SecondQuadTree] = lowNeighbour(x)
      val right: Option[SecondQuadTree] = rightNeighbour(x)
      val left: Option[SecondQuadTree] = leftNeighbour(x)
      up match {
        case None =>
        case Some(d) => {
          if (d.density < d.full && !branches.contains(d) && (d.size > minSize || d.density < d.empty)) {
            val weight: Double = if (d.density < d.empty && x.density < x.empty) 0 else d.density + x.density
            g.addEdge(x, d, false, weight, "" + x + " to " + d)
          }
        }
      }
      low match {
        case None =>
        case Some(d) => {
          if (d.density < d.full && !branches.contains(d) && (d.size > minSize || d.density < d.empty)) {
            val weight: Double = if (d.density < d.empty && x.density < x.empty) 0 else d.density + x.density
            g.addEdge(x, d, false, weight, "" + x + " to " + d)
          }
        }
      }
      right match {
        case None =>
        case Some(d) => {
          if (d.density < d.full && !branches.contains(d) && (d.size > minSize || d.density < d.empty)) {
            val weight: Double = if (d.density < d.empty && x.density < x.empty) 0 else d.density + x.density
            g.addEdge(x, d, false, weight, "" + x + " to " + d)
          }
        }
      }
      left match {
        case None =>
        case Some(d) => {
          if (d.density < d.full && !branches.contains(d) && (d.size > minSize || d.density < d.empty)) {
            val weight: Double = if (d.density < d.empty && x.density < x.empty) 0 else d.density + x.density
            g.addEdge(x, d, false, weight, "" + x + " to " + d)
          }
        }
      }
    }
    return g
  }
  def updateGraph(graph: FirstGraph[SecondQuadTree], oldFields: List[SecondQuadTree], newFields: List[SecondQuadTree]): FirstGraph[SecondQuadTree] = {
    //This assumes there are reasonable few old and new fields. Otherwise, the approach of the complete graph construction would be more suitable.
    //There might be potential for optimization because newFields is ordered lowLeft, lowRight, upRight, upLeft
    val oldNeighbours: List[SecondQuadTree] = oldFields.flatMap { x => graph.successors(x) }
    oldFields.foreach { x => graph.deleteNode(x) }
    //adding edges between newFields
    val nonFullAndNonSmallNewFields = newFields.filter { x => x.density < x.full && (x.size > minSize || x.density < x.empty) && !branches.contains(x) } //The last condition is probably necessary because there might be old fields in the successors. Whereas, actually that should be applied on the successors, right?
    val newPairs: List[(SecondQuadTree, SecondQuadTree)] = for (a <- nonFullAndNonSmallNewFields; b <- nonFullAndNonSmallNewFields) yield (a, b)
    val adjacentNewPairs = newPairs.filter(p => p._1.isAdjacentTo(p._2))
    adjacentNewPairs.foreach {
      p =>
        {
          val weight: Int = if (p._1.density < p._1.empty && p._2.density < p._2.empty) 0 else 1
          graph.addEdge(p._1, p._2, false, weight, "" + p._1 + " to " + p._2)
        }
    }
    //adding edges between new and old fields
    val newOldPairs: List[(SecondQuadTree, SecondQuadTree)] = for (a <- nonFullAndNonSmallNewFields; b <- oldNeighbours) yield (a, b)
    val adjacentNewOldPairs = newOldPairs.filter(p => p._1.isAdjacentTo(p._2))
    adjacentNewOldPairs.foreach {
      p =>
        {
          val weight: Int = if (p._1.density < p._1.empty && p._2.density < p._2.empty) 0 else 1
          graph.addEdge(p._1, p._2, false, weight, "" + p._1 + " to " + p._2)
        }
    }
    return graph
  }

  //minCellBridge finds the mixed cells that stand in the way between start and target and returns them in order from start to target.
  def minCellBridge(oldGraph: FirstGraph[SecondQuadTree], oldFields: List[SecondQuadTree], newFields: List[SecondQuadTree]): List[SecondQuadTree] = {
    //    val graph: FirstGraph[SecondQuadTree] = if (oldFields.isEmpty || newFields.isEmpty) generateEmptyMixedCellGraph else updateGraph(oldGraph, oldFields, newFields)
    //    ThirdAStar.graphVisTree(oldGraph)
    val s = startNode
    val t = targetNode
    val path: (List[SecondQuadTree], Double) = Dijkstra.DijkstraIterative[SecondQuadTree](oldGraph, t, s)
    return path._1.filter { x => x.density > x.empty }
  }

  def BridgePathHeuristic(proposedBonus: Double, density: Double, size: Double)(a: SecondQuadTree, b: SecondQuadTree): Double = {
    //      return PointArithmetic.angle(start, a.center, target) - PointArithmetic.angle(start, b.center, target)
    var bonus: Double = if (pathCells.contains(a)) proposedBonus else 0
    if (pathCells.contains(b)) {
      bonus -= proposedBonus
    }
    bonus += densitySizeHeuristic(density, size)(a, b)
    return bonus
  }
  def subdivision(node: SecondQuadTree) = {
    val newResolution: Int = node.size / 2
    val xMid = node.xMin + newResolution
    val yMid = node.yMin + newResolution
    val lowLeftMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 <= xMid && p._1._2 <= yMid)
    val lowRightMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 > xMid && p._1._2 <= yMid)
    val upRightMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 > xMid && p._1._2 > yMid)
    val upLeftMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 <= xMid && p._1._2 > yMid)
    val lowLeft: SecondQuadTree = SecondQuadTree(lowLeftMap, node.xMin, node.yMin, newResolution, node.empty, node.full, heuristic)
    val lowRight: SecondQuadTree = SecondQuadTree(lowRightMap, xMid, node.yMin, newResolution, node.empty, node.full, heuristic)
    val upRight: SecondQuadTree = SecondQuadTree(upRightMap, xMid, yMid, newResolution, node.empty, node.full, heuristic)
    val upLeft: SecondQuadTree = SecondQuadTree(upLeftMap, node.xMin, yMid, newResolution, node.empty, node.full, heuristic)
    val layer: treeLayer = treeLayer(lowLeft, lowRight, upRight, upLeft)
    parents.+=(lowLeft -> node)
    parents.+=(lowRight -> node)
    parents.+=(upRight -> node)
    parents.+=(upLeft -> node)
    branches.+=(node -> layer)
  }

  def directedLeaf(startNode: SecondQuadTree, targetPosition: OccupyPoint): SecondQuadTree = {
    if (!branches.contains(startNode)) return startNode
    val currentBranches: treeLayer = branches.apply(startNode)
    if (currentBranches.lowLeft.containsLocation(targetPosition)) return directedLeaf(currentBranches.lowLeft, targetPosition)
    if (currentBranches.lowRight.containsLocation(targetPosition)) return directedLeaf(currentBranches.lowRight, targetPosition)
    if (currentBranches.upRight.containsLocation(targetPosition)) return directedLeaf(currentBranches.upRight, targetPosition)
    if (currentBranches.upLeft.containsLocation(targetPosition)) return directedLeaf(currentBranches.upLeft, targetPosition)
    return startNode
  }

  def leaves(startNode: SecondQuadTree): List[SecondQuadTree] = {
    if (!branches.contains(startNode)) return startNode :: Nil
    val currentBranches: treeLayer = branches.apply(startNode)
    return leaves(currentBranches.lowLeft) ++ leaves(currentBranches.lowRight) ++ leaves(currentBranches.upRight) ++ leaves(currentBranches.upLeft)
  }

  def upNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.lowLeft) return Some(siblings.upLeft)
    else if (startNode == siblings.lowRight) return Some(siblings.upRight)
    val my: Option[SecondQuadTree] = upNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.upLeft) return Some(b.lowLeft) else return Some(b.lowRight)
      }
    }
    ???
  }
  def lowNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.upLeft) return Some(siblings.lowLeft)
    else if (startNode == siblings.upRight) return Some(siblings.lowRight)
    val my: Option[SecondQuadTree] = lowNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.lowLeft) return Some(b.upLeft) else return Some(b.upRight)
      }
    }
    ???
  }
  def leftNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.lowRight) return Some(siblings.lowLeft)
    else if (startNode == siblings.upRight) return Some(siblings.upLeft)
    val my: Option[SecondQuadTree] = leftNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.upLeft) return Some(b.upRight) else return Some(b.lowRight)
      }
    }
    ???
  }
  def rightNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.lowLeft) return Some(siblings.lowRight)
    else if (startNode == siblings.upLeft) return Some(siblings.upRight)
    val my: Option[SecondQuadTree] = rightNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.upRight) return Some(b.upLeft) else return Some(b.lowLeft)
      }
    }
    ???
  }

  //The new union method solves the problem that a larger neighbouring cell might not exist when a smaller neighbour is created, causing the link between the two not to be found.
  //It assumes that each free cell gets added to the unionFind structure right at its creation.
  def union(node: SecondQuadTree) = {
    if (node.density <= node.empty) {
      val freeNeighbourCells: List[SecondQuadTree] = freeNeighbours(node)
      freeNeighbourCells.foreach { x => assert(unionFind.contains(x)); unionTrack(node, x) }
    }
  }
  //unionOld is no longer in use.
  def unionOld(node: SecondQuadTree) = {
    if (node.density <= node.empty) {
      val left = leftNeighbour(node) match {
        case None => None
        case Some(d) => {
          assert(d != null);
          assert(d.isAdjacentTo(node));
          val condition: Boolean = node.xMin == d.xMin + d.size
          if (!condition) {
            println("this: " + node.xMin + " " + node.yMin + " " + node.size)
            println("that: " + d.xMin + " " + d.yMin + " " + d.size)
          }
          assert(condition);
          if (d.density <= d.empty) {
            unionTrack(node, d);
            Some(d)
          }
        }
      }
      val right = rightNeighbour(node) match {
        case None => None
        case Some(d) =>
          assert(d != null);
          assert(d.isAdjacentTo(node));
          assert(node.xMin + node.size == d.xMin);
          if (d.density <= d.empty) {
            /*val pw = new PrintWriter(new File("debug_right_neighbour.txt"))
            pw.write("d:" + '\n')
            pw.write(d + ("" + '\n'))
            pw.write("Parameters:" + '\n')
            pw.write("xMin: " + d.xMin + ", yMin: " + d.yMin + ", size: " + d.size + ", density: " + d.density + '\n')
            pw.close*/
            unionTrack(node, d);
            Some(d)
          }
      }
      val up = upNeighbour(node) match {
        case None => None
        case Some(d) => {
          assert(d != null);
          assert(d.isAdjacentTo(node));
          assert(node.yMin + node.size == d.yMin);
          if (d.density <= d.empty) {
            unionTrack(node, d);
            Some(d)
          }
        }
      }
      val low = lowNeighbour(node) match {
        case None => None
        case Some(d) => {
          assert(d != null);
          assert(d.isAdjacentTo(node));
          assert(node.yMin == d.yMin + d.size);
          if (d.density <= d.empty) {
            unionTrack(node, d);
            Some(d)
          }
        }
      }
    }
  }

  //The following method allows to maintain the component priority queues for start and target when a union is performed.
  def unionTrack(a: SecondQuadTree, b: SecondQuadTree): Unit = {
    if (!unionFind.isConnected(a, b)) {
      if (h2()) {
        if (unionFind.isConnected(a, startNode)) {
          //          assert(startComponent.toList.find { x => x == a }.isDefined) //I might want to comment this line if it doesn't throw an exception for many runs, since it might take a while to compute.
          val ccB = connectedComponent(b)
          ccB.foreach { x =>
            startComponent.+=(x)
            val mn = mixedNeighbours(x)
            mn.foreach { y => adjacentToStartOrTargetComponents.+=((y, 1)) }
          }
        }
        if (unionFind.isConnected(a, targetNode)) {
          //          assert(targetComponent.toList.find { x => x == a }.isDefined) //I might want to comment this line if it doesn't throw an exception for many runs, since it might take a while to compute.
          val ccB = connectedComponent(b)
          ccB.foreach { x =>
            targetComponent.+=(x)
            val mn = mixedNeighbours(x)
            mn.foreach { y => adjacentToStartOrTargetComponents.+=((y, 1)) }
          }
        }
        if (unionFind.isConnected(b, startNode)) {
          //          assert(startComponent.toList.find { x => x == b }.isDefined) //I might want to comment this line if it doesn't throw an exception for many runs, since it might take a while to compute.
          val ccA = connectedComponent(a)
          ccA.foreach { x =>
            startComponent.+=(x)
            val mn = mixedNeighbours(x)
            mn.foreach { y => adjacentToStartOrTargetComponents.+=((y, 1)) }
          }
        }
        if (unionFind.isConnected(b, targetNode)) {
          //          assert(targetComponent.toList.find { x => x == b }.isDefined) //I might want to comment this line if it doesn't throw an exception for many runs, since it might take a while to compute.
          val ccA = connectedComponent(a)
          ccA.foreach { x =>
            targetComponent.+=(x)
            val mn = mixedNeighbours(x)
            mn.foreach { y => adjacentToStartOrTargetComponents.+=((y, 1)) }
          }
        }
      }
      unionFind.union(a, b)
    }
  }

  //The following function defines the minimum distance between two SecondQuadTrees.
  def heuristicDistance(a: SecondQuadTree, b: SecondQuadTree): Double = {
    if (PointArithmetic.polygonOverlap(a.toPolygon.getCornerPoints(), b.toPolygon.getCornerPoints())) {
      return 0.0
    }
    if (!(a.xMin > b.xMin + b.size || b.xMin > a.xMin + a.size)) {
      if (a.yMin > b.yMin) {
        return a.yMin - (b.yMin + b.size)
      } else if (b.yMin > a.yMin) {
        return b.yMin - (a.yMin + a.size)
      } else {
        assert(a.yMin != b.yMin)
      }
    } else if (!(a.yMin > b.yMin + b.size || b.yMin > a.yMin + a.size)) {
      if (a.xMin > b.xMin) {
        return a.xMin - (b.xMin + b.size)
      } else if (b.xMin > a.xMin) {
        return b.xMin - (a.xMin + a.size)
      } else {
        assert(a.xMin != b.xMin)
      }
    }
    //If a and b don't overlap, the corner of a closest to the center of b and vice versa are the closest pair of corners s.t. one is in a and the other in b.
    val aClose = a.corners.minBy { x => PointArithmetic.distance(x, b.center) }
    val bClose = b.corners.minBy { x => PointArithmetic.distance(x, a.center) }
    val distance = PointArithmetic.distance(aClose, bClose)
    return distance //(PointArithmetic.distance(a.center, b.center) - a.size / 2 * Math.sqrt(2) - b.size / 2 * Math.sqrt(2))
  }
  def connectedComponent(node: SecondQuadTree): List[SecondQuadTree] = unionFind.connectedComponent(node)

  def densityHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return b.density - a.density
  }
  def bridgeheadDensitySizeHeuristic(bonus1Bridgehead: Double, bonus2Bridgeheads: Double, bonusAdjacentComponent: Double, density: Double, size: Double)(a: SecondQuadTree, b: SecondQuadTree): Double = {
//    val bonus1Bridgehead = 10000
//    val bonus2Bridgeheads = 1000000
//    val bonusAdjacentComponent = 1000
    //    val startCell: SecondQuadTree = startNode
    //    val ccStart: List[SecondQuadTree] = connectedComponent(startCell)
    val nearestToTarget: SecondQuadTree = startComponent.head //ccStart.minBy { x => PointArithmetic.distance(x.center, target) }
    //    val targetCell: SecondQuadTree = targetNode
    //    val ccTarget: List[SecondQuadTree] = connectedComponent(targetCell)
    val nearestToStart: SecondQuadTree = targetComponent.head //ccTarget.minBy { x => PointArithmetic.distance(x.center, start) }
    var output = 0.0
    //      output = output + (a.density - b.density)
    if (a.isAdjacentTo(nearestToTarget)) output = output - bonus1Bridgehead
    if (b.isAdjacentTo(nearestToTarget)) output = output + bonus1Bridgehead
    if (a.isAdjacentTo(nearestToStart)) output = output - bonus1Bridgehead
    if (b.isAdjacentTo(nearestToStart)) output = output + bonus1Bridgehead
    if (a.isAdjacentTo(nearestToTarget) && a.isAdjacentTo(nearestToStart)) output = output - bonus2Bridgeheads
    if (b.isAdjacentTo(nearestToTarget) && b.isAdjacentTo(nearestToStart)) output = output + bonus2Bridgeheads
    //Add bonus for all cells adjacent to connected components of start or target.
    if (adjacentToStartOrTargetComponents.contains(a)) output = output - (bonusAdjacentComponent / Math.max(1, Math.min(heuristicDistance(a, nearestToStart), heuristicDistance(a, nearestToTarget))))
    if (adjacentToStartOrTargetComponents.contains(b)) output = output + (bonusAdjacentComponent / Math.max(1, Math.min(heuristicDistance(b, nearestToStart), heuristicDistance(b, nearestToTarget))))
    output = -output
    output += densitySizeHeuristic(density, size)(a, b)
    return output
  }
  def densitySizeHeuristic(d: Double, s: Double)(a: SecondQuadTree, b: SecondQuadTree): Double = {
    val density: Double = (b.density - a.density) / (full - empty)
    val size: Double = (a.size - b.size) / (this.size - 2 * minSize)
    return /*if(a.size <= minSize) Double.MinValue else*/ d * density + s * size
  }
  def sizeHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return a.size - b.size
  }
  def angleHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    val angleA = PointArithmetic.angle(start, a.center, target)
    val angleB = PointArithmetic.angle(start, b.center, target)
    return angleA - angleB
  }
  def angleDensitySizeHeuristic(angle: Double, density: Double, size: Double)(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return angle * angleHeuristic(a, b) + density * 180 * densityHeuristic(a, b) / (full - empty) + size * 180 * sizeHeuristic(a, b) / (size - minSize)
  }
  val root: SecondQuadTree = SecondQuadTree(points, xMin, yMin, size, empty, full, densitySizeHeuristic(0, 0))

  //startTree returns (whole tree, current leaf). It can also be used for the target. This was previously defined within freeSpace.
  //  @tailrec
  def startTree(root: SecondQuadTree, start: OccupyPoint, minSize: Int): (SecondQuadTree, SecondQuadTree) = {
    val currentLeaf: SecondQuadTree = directedLeaf(root, start) //startNode should not be called here, because this function is called both for computing start and target nodes.
    if (currentLeaf.size < 2 * minSize || currentLeaf.density <= currentLeaf.empty) {
      return (root, currentLeaf)
    }
    subdivision(currentLeaf)
    return startTree(root, start, minSize)
  }
  def freeSpace: (List[SecondQuadTree], Option[SecondQuadTree], Option[SecondQuadTree], List[Image]) = {
    def heuristicInt(a: SecondQuadTree, b: SecondQuadTree): Int = -(heuristic(a, b).signum)
    val q: BinaryMinHeap[SecondQuadTree] = new BinaryMinHeap[SecondQuadTree](heuristicInt)

    //simpleRoot is needed to define heuristic2. The actual heuristic is not used in simpleRoot.
    //heuristic2 doesn't work because simpleRoot is not in the unionFind structure.
    val simpleRoot: SecondQuadTree = SecondQuadTree(points, xMin, yMin, size, empty, full, densityHeuristic)

    val parentMap: parentStorage = new parentStorage
    val branchesMap: branchesStorage = new branchesStorage

    println("Initial Tree computation started at: " + java.util.Calendar.getInstance.getTime)
    val initialTreeStart: (SecondQuadTree, SecondQuadTree) = startTree(root, start, minSize)
    println("startTree computed at: " + java.util.Calendar.getInstance.getTime)
    val startLeaf: SecondQuadTree = initialTreeStart._2
    startNodeSave = Some(startLeaf)
    val initialTreeTarget: (SecondQuadTree, SecondQuadTree) = startTree(initialTreeStart._1, target, minSize)
    println("targetTree computed at: " + java.util.Calendar.getInstance.getTime)
    val targetLeaf: SecondQuadTree = initialTreeTarget._2
    targetNodeSave = Some(targetLeaf)
    val initialTree: SecondQuadTree = initialTreeTarget._1

    val leafList: List[SecondQuadTree] = leaves(initialTree)

    def showStep(g: Option[FirstGraph[SecondQuadTree]]) {
      val edgePoints: List[(List[OccupyPoint], Color)] = g match {
        case None => Nil
        case Some(d) => {
          val edges = d.getEdges()
          edges.map { x =>
            val color = if (x.directed) Color.Purple else if (x.weight == 0) Color.DarkBlue else Color.Red //Color.rgb(Math.min(255, (255 * x.weight).toInt), Math.min(255, (255 * x.weight).toInt), Math.min(255, (255 * x.weight).toInt))
            val points = Bresenham.BresenhamAlgorithm(x.n1.center, x.n2.center)
            (points, color)
          }
        }
      }
      val p: List[OccupyPoint] = root.points.map {
        f => OccupyPoint(f._1._1, f._1._2)
      }.toList
      val cells = leaves(initialTree).map { x =>
        if (h2() && x == startComponent.head) (KinectScanDataTesterA.cellBox(x), Color.rgb(150, 50, 255, 0.1))
        else if (h2() && x == targetComponent.head) (KinectScanDataTesterA.cellBox(x), Color.rgb(50, 150, 255, 0.1))
        else if (x.density <= x.empty && unionFind.isConnected(x, startLeaf)) (KinectScanDataTesterA.cellBox(x), Color.rgb(50, 0, 255, 0.1))
        else if (x.density <= x.empty && unionFind.isConnected(x, targetLeaf)) (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 50, 255, 0.1))
        else if (x.density <= x.empty) (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 0, 255, 0.1))
        else if (x.density > x.full) (KinectScanDataTesterA.cellBox(x), Color.rgb(255, 0, 0, 0.1))
        else if (pathCells.contains(x)) (KinectScanDataTesterA.cellBox(x), Color.rgb(255, 255, 0, 0.1))
        else (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 255, 0, 0.1))
      }
      val timeStamp: Long = System.currentTimeMillis()
      outputImages = Image(root.size, root.size, cells, ((p, Color.Black) :: edgePoints), 1, "" + timeStamp + "", "")::outputImages
    }

    if (startLeaf.density > empty || targetLeaf.density > empty) {
      if (startLeaf.density > empty) println("start leaf not found")
      if (targetLeaf.density > empty) println("target leaf not found")
      showStep(None)
      return (leaves(initialTree), None, None, outputImages)
    }
    //  unionFind.add(startLeaf)
    //  unionFind.add(targetLeaf)

    println("checked for free start and target at: " + java.util.Calendar.getInstance.getTime)

    //At this point, the old nodes with heuristic3 or some other simple heuristic are replaced against new nodes with the same data but the (hopefully) more advanced heuristic2.
    /*def replaceHeuristic(current: SecondQuadTree) = {
      if(branches.contains(current)){
        val b: treeLayer = branches.apply(current)
        
      }
    }*/
    /*parents.foreach {
      f =>
        f match {
          case (SecondQuadTree(p1, x1, y1, s1, e1, f1, o1), SecondQuadTree(p2, x2, y2, s2, e2, f2, o2)) => {
            parents.remove(SecondQuadTree(p1, x1, y1, s1, e1, f1, o1))

            parents.+=((SecondQuadTree(p1, x1, y1, s1, e1, f1, heuristic2), SecondQuadTree(p2, x2, y2, s2, e2, f2, heuristic2)))

          }
          case _ => ???
        }
    }
    branches.foreach {
      f =>
        f match {
          case (SecondQuadTree(p0, x0, y0, s0, e0, f0, o0), treeLayer(SecondQuadTree(p1, x1, y1, s1, e1, f1, o1), SecondQuadTree(p2, x2, y2, s2, e2, f2, o2), SecondQuadTree(p3, x3, y3, s3, e3, f3, o3), SecondQuadTree(p4, x4, y4, s4, e4, f4, o4))) => {
            branches.remove(SecondQuadTree(p0, x0, y0, s0, e0, f0, o0))

            branches.+=((SecondQuadTree(p0, x0, y0, s0, e0, f0, heuristic2), treeLayer(SecondQuadTree(p1, x1, y1, s1, e1, f1, heuristic2), SecondQuadTree(p2, x2, y2, s2, e2, f2, heuristic2), SecondQuadTree(p3, x3, y3, s3, e3, f3, heuristic2), SecondQuadTree(p4, x4, y4, s4, e4, f4, heuristic2))))

          }
        }
    }*/

    //    println("Replaced old nodes with new ones with new heuristic at: " + java.util.Calendar.getInstance.getTime)

    println("initial leaves computed at: " + java.util.Calendar.getInstance.getTime)
    unionFind.setStart(startLeaf)
    unionFind.setTarget(targetLeaf)
    val freeLeaves: List[SecondQuadTree] = leafList.filter { x => x.density <= x.empty }
    val mixedLeaves: List[SecondQuadTree] = leafList.filter { x => x.size > minSize && x.empty < x.density && x.density < x.full }
    //    assert(freeLeaves.contains(startLeaf))
    //    assert(freeLeaves.contains(targetLeaf))
    startComponent.+=(startNode)
    val mn = mixedNeighbours(startNode)
    mn.foreach { y => adjacentToStartOrTargetComponents.+=((y, 1)) }
    targetComponent.+=(targetNode)
    val mn2 = mixedNeighbours(targetNode)
    mn2.foreach { y => adjacentToStartOrTargetComponents.+=((y, 1)) }
    freeLeaves.foreach { x => unionFind.add(x) } //This should include startLeaf and targetLeaf.
    println("added free leaves to unionFind at: " + java.util.Calendar.getInstance.getTime)
    freeLeaves.foreach { x => union(x) }
    println("union on free leaves computed at: " + java.util.Calendar.getInstance.getTime)
    mixedLeaves.foreach { x => q.+=(x) }

    println("Leaves computed at: " + java.util.Calendar.getInstance.getTime)

    //    println(startLeaf)
    //    println(targetLeaf)
    val smallFields: PriorityQueue[SecondQuadTree] = new PriorityQueue[SecondQuadTree]
    var oldFields: List[SecondQuadTree] = Nil
    var newFields: List[SecondQuadTree] = Nil
    var graph: FirstGraph[SecondQuadTree] = new FirstGraph[SecondQuadTree]
    if (h4()) {
      graph = generateEmptyMixedCellGraph
    }
    while (!unionFind.isConnected(startLeaf, targetLeaf)) {
      //                  println(q.size)
      /*if (pathCells.isEmpty) {
        println("pathCells empty")
      }*/
      //showStep is now further below to make sure the yellow path is displayed in time before the first subdivision.
      /*if (showIntermediateSteps) {
        showStep(if (h4()) Some(graph) else None)
        //        if (h4()) {
        //          ThirdAStar.graphVisTree(graph)
        //        }
      }*/

      if (q.isEmpty && smallFields.isEmpty) {
        if (showIntermediateSteps) showStep(None)
        return (leaves(initialTree), Some(startLeaf), Some(targetLeaf), outputImages)
      } else if (q.isEmpty) {
        val x: SecondQuadTree = smallFields.dequeue()

        if (x.densityOnMeasurementPoints / x.density < densityQuotientForPostProcessing) {
          unionFind.add(x)
          union(x)
          println("Small cell was declared empty.")
        }
      } else {

        //The getNext()-method might find out early that there is no path. In that case, it returns None.
        def getNext(): Option[SecondQuadTree] = {
          if (h4()) {
            if (pathCells.isEmpty) {
              val newBridge: List[SecondQuadTree] = minCellBridge(graph, oldFields, newFields)

              //Early termination in case there is no path.
              if (newBridge.isEmpty) {
                return None
              }
              oldFields = Nil
              newFields = Nil
              newBridge.foreach { x => pathCells.+=((x, 0)); q.reevaluateElement(x) } //I don't think I need the Int, just HashMap wouldn't compile otherwise.

            }
          } else if (h2()) {
            q.refresh
          }
          if (showIntermediateSteps) {
            showStep(if (h4()) Some(graph) else None)
          }

          val next = q.dequeue

          //Early termination in case there is no path.
          if (h2()) {
            if (!adjacentToStartOrTargetComponents.contains(next)) {
              return None
            }
          }

          if (h4() && pathCells.contains(next)) {
            pathCells.-=(next)
          }
          return Some(next)
        }
        def getNextOld(): SecondQuadTree = {
          //Refresh
          if (h4() || h2()) {
            q.refresh
            //            val cache = q.dequeueAll
            //            cache.foreach { x => q.+=(x) }
          }
          val next = q.dequeue

          if (h4()) {
            if (pathCells.isEmpty) {
              val newBridge: List[SecondQuadTree] = minCellBridge(graph, oldFields, newFields)
              oldFields = Nil
              newFields = Nil
              newBridge.foreach { x => pathCells.+=((x, 0)) } //I don't think I need the Int, just HashMap wouldn't compile otherwise.
            }
            //            val next: SecondQuadTree = q.dequeue()
            if (pathCells.contains(next)) {
              pathCells.-=(next)
            }

          }

          return next
        }
        val next = getNext()
        if (next.isEmpty) {
          return (leaves(initialTree), Some(startLeaf), Some(targetLeaf), outputImages)
        }
        val current: SecondQuadTree = next.get

        if (current.size <= minSize) {
          smallFields.+=(current)
        } else {
          subdivision(current)
          oldFields = current :: oldFields

          val children: List[SecondQuadTree] = branches(current) match {
            case treeLayer(a, b, c, d) => List(a, b, c, d)
            case _                     => ??? //This should not happen, since the node has just been subdivided.
          }
          if (h4()) {
            val successors: List[SecondQuadTree] = graph.successors(current)
            graph.deleteNode(current)
            val childrenForGraph = children.filter { x => x.density < x.full && (x.size > minSize || x.density <= x.empty) }
            val childrenPairs = for (a <- childrenForGraph; b <- childrenForGraph) yield (a, b)
            childrenPairs.filterNot(p => p._1 == p._2).foreach(f => {
              if (f._1.isAdjacentTo(f._2)) {
                val weight: Double = if (f._1.density < f._1.empty && f._2.density < f._2.empty) 0 else f._1.density + f._2.density
                graph.addEdge(f._1, f._2, false, weight, "" + f._1 + " to " + f._2)
              }
            })
            val childrenToSuccessorsPairs = for (a <- childrenForGraph; b <- successors) yield (a, b)
            childrenToSuccessorsPairs.foreach(f => {
              if (f._1.isAdjacentTo(f._2)) {
                val weight: Double = if (f._1.density < f._1.empty && f._2.density < f._2.empty) 0 else f._1.density + f._2.density
                graph.addEdge(f._1, f._2, false, weight, "" + f._1 + " to " + f._2)
              }
            })
            //            ThirdAStar.graphVisTree(graph)
          }
          newFields = children ++ newFields
          def addBonusForAdjacencyToStartOrTargetComponent(node: SecondQuadTree) = {
            if (h2()) {
              val n = freeNeighbours(node)
              val anyNeighbourInComponent = n.exists { x => unionFind.contains(x) && (unionFind.isConnected(x, startNode) || unionFind.isConnected(x, targetNode)) }
              if (anyNeighbourInComponent) {
                adjacentToStartOrTargetComponents.+=((node, 1))
              }
            }
          }
          children.foreach { x => if (x.density <= x.empty) { unionFind.add(x) } else if (x.size > minSize && x.empty < x.density && x.density < x.full) { q.+=(x); addBonusForAdjacencyToStartOrTargetComponent(x) } }
          children.foreach { x => if (x.density <= x.empty) { union(x) } }
        }
      }
    }
    assert(unionFind.isConnected(startLeaf, targetLeaf)) //This assertion strangely doesn't fail if there is no path.
    //    showStep(None)
    return (leaves(initialTree), Some(startLeaf), Some(targetLeaf), outputImages)
  }
  def channel(allLeaves: List[SecondQuadTree], startLeaf: SecondQuadTree, targetLeaf: SecondQuadTree): List[SecondQuadTree] = {
    allLeaves.filter { x => x.density <= x.empty && (unionFind.isConnected(startLeaf, x) && unionFind.isConnected(targetLeaf, x)) }
  }
}
case class PriorityNode[Element](priority: Double, node: Element) extends Ordered[PriorityNode[Element]] {
  def compare(that: PriorityNode[Element]): Int = (this.priority - that.priority).signum
}
//For Copy and Paste:
//SecondQuadTree(points, xMin, yMin, size, empty, full, parent, unionFind, minSize, start, target, ordering, Some(treeLayer(lowLeft, lowRight, upRight, upLeft)))

case class SecondQuadTree(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double, ordering: ((SecondQuadTree, SecondQuadTree) => Double)) extends Ordered[SecondQuadTree] {
  def compare(that: SecondQuadTree): Int = ordering(this, that).signum

  val density: Double = points.size.toDouble / (size * size).toDouble
  val densityOnMeasurementPoints: Double = points.map {
    f =>
      f match {
        case ((x, y), (d, i)) => i
      }
  }.sum / (size * size).toDouble
  //  println(density)
  val numPoints: Int = points.size

  def isAdjacentTo(that: SecondQuadTree): Boolean = {
    //    val xLeft: Boolean = xMin == that.xMin + that.size
    //    val yBelow: Boolean = yMin == that.yMin + that.size
    //    val xRight: Boolean = xMin + size == that.xMin
    //    val yUp: Boolean = yMin + size == that.yMin
    val yOverlap: Boolean = (yMin <= that.yMin && that.yMin <= yMin + size) || (that.yMin <= yMin && yMin <= that.yMin + that.size)
    val xOverlap: Boolean = (xMin <= that.xMin && that.xMin <= xMin + size) || (that.xMin <= xMin && xMin <= that.xMin + that.size)
    val corner: Boolean = (xMin == that.xMin + that.size || xMin + size == that.xMin) && (yMin == that.yMin + that.size || yMin + size == that.yMin)
    return yOverlap && xOverlap && !corner
  }
  def center: OccupyPoint = OccupyPoint(xMin + size / 2, yMin + size / 2)
  def leftCenter: OccupyPoint = OccupyPoint(xMin, yMin + size / 2)
  def lowCenter: OccupyPoint = OccupyPoint(xMin + size / 2, yMin)
  def rightCenter: OccupyPoint = OccupyPoint(xMin + size, yMin + size / 2)
  def upCenter: OccupyPoint = OccupyPoint(xMin + size / 2, yMin + size)

  def containsLocation(test: OccupyPoint): Boolean = {
    return xMin <= test.x && test.x <= xMin + size && yMin <= test.y && test.y <= yMin + size
  }
  def distanceToBorder(test: OccupyPoint): Option[Double] = if (!containsLocation(test)) return None else {
    val xMax = xMin + size
    val yMax = yMin + size
    val output: Double = test match {
      case OccupyPoint(x, y) => Math.min(Math.min(x - xMin, xMax - x), Math.min(y - yMin, yMax - y))
    }
    return Some(output)
  }
  //For a specific neighbouring cell
  def distanceToBorder(point: OccupyPoint, cell: SecondQuadTree): Option[Double] = {
    if (!isAdjacentTo(cell)) return None
    //    println(cell)
    val distanceToLowerX: Int = math.abs(cell.xMin - point.x)
    val distanceToUpperX: Int = math.abs(cell.xMin + cell.size - point.x)
    val distanceToLowerY: Int = math.abs(cell.yMin - point.y)
    val distanceToUpperY: Int = math.abs(cell.yMin + cell.size - point.y)
    val closestXEnd: Int = math.min(distanceToLowerX, distanceToUpperX) //if(math.abs(cell.xMin - point.x) < math.abs(cell.xMin + cell.size - point.x)) cell.xMin else cell.xMin + cell.size
    val closestYEnd: Int = math.min(distanceToLowerY, distanceToUpperY) //if(math.abs(cell.yMin - point.y) < math.abs(cell.yMin + cell.size - point.y)) cell.yMin else cell.yMin + cell.size
    val closestX: Int = if (cell.xMin <= point.x && point.x <= cell.xMin + cell.size) 0 else closestXEnd
    val closestY: Int = if (cell.yMin <= point.y && point.y <= cell.yMin + cell.size) 0 else closestYEnd
    return Some(math.sqrt(closestX * closestX + closestY * closestY))
  }

  def lineSegmentCollisionSMT(a: OccupyPoint, b: OccupyPoint): Boolean = {
    //    val context: Context = new Context();
    //
    //    val x = context.mkSymbol("x");
    val lowRight: Boolean = test1.linesegmentTriangleTest2D(a.x, a.y, b.x, b.y, xMin, yMin, xMin + size, yMin, xMin + size, yMin + size)
    val upLeft: Boolean = test1.linesegmentTriangleTest2D(a.x, a.y, b.x, b.y, xMin, yMin, xMin, yMin + size, xMin + size, yMin + size)
    println(lowRight || upLeft)
    return lowRight || upLeft
  }
  def lineSegmentCollision(a: OccupyPoint, b: OccupyPoint): Boolean = {
    val left: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin, yMin), OccupyPoint(xMin, yMin + size))
    val up: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin + size, yMin + size), OccupyPoint(xMin, yMin + size))
    val right: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin + size, yMin), OccupyPoint(xMin + size, yMin + size))
    val low: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin, yMin), OccupyPoint(xMin + size, yMin))
    val result = left || up || right || low
    //    println(result)
    return result
  }
  def lowLeftCorner: OccupyPoint = OccupyPoint(xMin, yMin)
  def lowRightCorner: OccupyPoint = OccupyPoint(xMin + size, yMin)
  def upRightCorner: OccupyPoint = OccupyPoint(xMin + size, yMin + size)
  def upLeftCorner: OccupyPoint = OccupyPoint(xMin, yMin + size)
  def corners: List[OccupyPoint] = List(lowLeftCorner, lowRightCorner, upRightCorner, upLeftCorner)
  def toPolygon: Polygon = Polygon(List(lowLeftCorner, lowRightCorner, upRightCorner, upLeftCorner))
  def lineSegmentCollision(a: OccupyPoint, b: OccupyPoint, clearance: Int): Boolean = {
    return SecondQuadTree(points, xMin - clearance, yMin - clearance, size + 2 * clearance, empty, full, ordering).lineSegmentCollision(a, b)
  }

  override def toString(): String = "SecondQuadTree(" + xMin + " , " + yMin + " , " + size + ")"
}

class parentStorage {
  var parent: Option[SecondQuadTree] = None
  def setParent(newParent: SecondQuadTree) = {
    parent = Some(newParent)
  }
  def getParent: Option[SecondQuadTree] = parent
}
class branchesStorage {
  var branches: Option[treeLayer] = None
  def setBranches(newBranches: treeLayer) = {
    branches = Some(newBranches)
  }
  def getBranches: Option[treeLayer] = branches
}

case class treeLayer(lowLeft: SecondQuadTree, lowRight: SecondQuadTree, upRight: SecondQuadTree, upLeft: SecondQuadTree)
case class thirdTreeLayer(lowLeft: ThirdQuadTree, lowRight: ThirdQuadTree, upRight: ThirdQuadTree, upLeft: ThirdQuadTree)

//case class QuadTree(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double)

case class ThirdQuadTree(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double) {
  val density: Double = points.size.toDouble / (size * size).toDouble
  val numPoints: Int = points.size
  val branches: Option[thirdTreeLayer] = {
    if (size > 8 && density > empty && density < full) {
      val newResolution: Int = size / 2
      val xMid = xMin + newResolution
      val yMid = yMin + newResolution
      val lowLeftMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 <= xMid && p._1._2 <= yMid)
      val lowRightMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 > xMid && p._1._2 <= yMid)
      val upRightMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 > xMid && p._1._2 > yMid)
      val upLeftMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 <= xMid && p._1._2 > yMid)
      val lowLeft: ThirdQuadTree = ThirdQuadTree(lowLeftMap, xMin, yMin, newResolution, empty, full)
      val lowRight: ThirdQuadTree = ThirdQuadTree(lowRightMap, xMid, yMin, newResolution, empty, full)
      val upRight: ThirdQuadTree = ThirdQuadTree(upRightMap, xMid, yMid, newResolution, empty, full)
      val upLeft: ThirdQuadTree = ThirdQuadTree(upLeftMap, xMin, yMid, newResolution, empty, full)
      Some(thirdTreeLayer(lowLeft, lowRight, upRight, upLeft))
    } else None
  }
  def leaves: List[ThirdQuadTree] = branches match {
    case None        => this :: Nil
    case Some(layer) => layer.lowLeft.leaves ++ layer.lowRight.leaves ++ layer.upRight.leaves ++ layer.upLeft.leaves
  }
}
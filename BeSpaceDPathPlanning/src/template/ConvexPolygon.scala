/*package template

import BeSpaceDCore._
import PointArithmetic._



*//**
 * @author Carl
 *
 * Generates the convex hull of its input.
 * 
 * TODO: How to make this inherit from Polygon?
 *//*
class ConvexPolygon (hull: List[OccupyPoint]) extends CoreDefinitions with AbstractPolygon{
  def getCornerPoints(): List[OccupyPoint] = PointArithmetic.enforceCCW(new ConvexHull().convexHull(hull))
  def collision(point : OccupyPoint) : Boolean = {
    return PointArithmetic.inclusion(getCornerPoints(), point)
  }
  
  def vis() : List[OccupyPoint] = {
    val maxX : Int = getCornerPoints().maxBy { x => x.x }.x
    val minX : Int = getCornerPoints().minBy { x => x.x }.x
    val maxY : Int = getCornerPoints().maxBy { x => x.y }.y
    val minY : Int = getCornerPoints().minBy { x => x.y }.y
    var list : List[OccupyPoint] = Nil
    (minX to maxX).foreach{
      x => (minY to maxY).foreach{
        y => list = if(collision(OccupyPoint(x,y))) OccupyPoint(x,y) :: list else list
      }
    }
    return list
  }
  
  override def toString() : String = {
    getCornerPoints().toString()
  }
  
  //How to make sure this function is applied when calling == ?
  def ==(that: ConvexPolygon): Boolean = {
    return (this.getCornerPoints() ++ this.getCornerPoints()).containsSlice(that.getCornerPoints()) && (that.getCornerPoints() ++ that.getCornerPoints()).containsSlice(this.getCornerPoints())
  }
  //How to make sure this function is applied when calling == ?
  def ==(that: Polygon): Boolean = {
    return (this.getCornerPoints() ++ this.getCornerPoints()).containsSlice(that.getCornerPoints()) && (that.getCornerPoints() ++ that.getCornerPoints()).containsSlice(this.getCornerPoints())
  }
}*/
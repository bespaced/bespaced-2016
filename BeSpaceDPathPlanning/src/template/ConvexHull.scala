package template
import scala.collection.mutable.ArrayBuffer
import BeSpaceDCore._

//https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain#Scala
//The plain method cannot guarantee to find the hull in counterclockwise direction, so it is enforced externally.
class ConvexHull {
  def convexHull(points: List[OccupyPoint]) : List[OccupyPoint] = {
    var buf = ArrayBuffer.empty[(Int, Int)]
    var list = points
    while(list != Nil){
      buf += ((list.head.x, list.head.y))
      list = list.tail
    }
    var result = convexHull(buf)
    var output : List[OccupyPoint] = Nil
    var i : Int = 0
    while(i < result.length){
      output = output ++ (OccupyPoint(result(i)._1, result(i)._2)::Nil)
      i += 1
    }
    return enforceCCW(output)
  }
  def convexHull(points: ArrayBuffer[(Int, Int)]): ArrayBuffer[(Int, Int)] = {
    
    
    // 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
    // Returns a positive value, if OAB makes a counter-clockwise turn,
    // negative for clockwise turn, and zero if the points are collinear.
    def cross(o: (Int, Int), a: (Int, Int), b: (Int, Int)): Int = {
      (a._1 - o._1) * (b._2 - o._2) - (a._2 - o._2) * (b._1 - o._1)
    }
    
    val distinctPoints = points.distinct
    
    // No sorting needed if there are less than 2 unique points.
    if(distinctPoints.length < 2) {
      return points
    } else {
      
      val sortedPoints = distinctPoints.sorted
      
      // Build the lower hull
      val lower = ArrayBuffer[(Int, Int)]()
      for(i <- sortedPoints){
        while(lower.length >= 2 && cross(lower(lower.length - 2), lower(lower.length - 1) , i) <= 0){
          lower -= lower.last
        }
        lower += i
      }
      
      // Build the upper hull
      val upper = ArrayBuffer[(Int, Int)]()
      for(i <- sortedPoints.reverse){
        while(upper.size >= 2 && cross(upper(upper.length - 2), upper(upper.length - 1) , i) <= 0){
          upper -= upper.last
        }
        upper += i
      }
      
      // Last point of each list is omitted because it is repeated at the beginning of the other list.
      lower -= lower.last
      upper -= upper.last
      
      // Concatenation of the lower and upper hulls gives the convex hull
      return lower ++= upper
    }
  }
  
  //At some occasions, the above implementation returns a clockwise list, for I don't know what reasons. So this method enforces counter-clockwiseness. Probably I keep getting confused because of the orientation of the coordinate system.
  def enforceCCW(points : List[OccupyPoint]) : List[OccupyPoint] = {
    if(CCWchecker(points)) return points else if(CCWchecker(points.reverse)) return points.reverse else return Nil
  }
  def CCWchecker(points: List[OccupyPoint]) : Boolean = {
    points match{
      case head1::head2::head3::tail => return List("no turn", "left turn").contains(Turn(head1, head2, head3)) && CCWchecker(head2::head3::tail)
      case _ => return true
    }
  }
  
  //Determines if point is in the hull or on its border. 
  def insideOrBorder(point : OccupyPoint, hull : List[OccupyPoint]) : Boolean = {
    return List("no turn", "left turn").contains(Turn(hull.last, hull.head, point)) && isInRec(point, hull)
  }
  def isInRec(point : OccupyPoint, hull : List[OccupyPoint]) : Boolean = {
    hull match{
      case head::head2::tail => return List("no turn", "left turn").contains(Turn(hull.head, hull.tail.head, point)) && isInRec(point, hull.tail)
      case _ => return true
    }
  }
  
  def Turn(a : OccupyPoint, b : OccupyPoint, c : OccupyPoint) : String = {
    val ab : Array[Int] = Array(b.x - a.x, b.y - a.y, 0)
    val bc : Array[Int] = Array(c.x - b.x, c.y - b.y, 0)
    val crossProduct = Array(ab(1)*bc(2) - ab(2)*bc(1),ab(2)*bc(0) - ab(0)*bc(2),ab(0)*bc(1) - ab(1)*bc(0))
    if(crossProduct(2) == 0) {return "no turn"}
    if(crossProduct(2) < 0) {return "left turn"}
    else {if(crossProduct(2) > 0) {return "right turn"} else "undef"}
  }
}
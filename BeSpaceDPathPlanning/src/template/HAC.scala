package template

import scala.collection.mutable.HashMap
import BeSpaceDCore._

/**
 * @author Carl
 * Hierarchical Agglomerative Clustering
 * Computes distances between clusters by distance between its components, and this is again computed by distance between its points.
 *
 */
class HAC(obstacles: List[AbstractPolygon]) extends CoreDefinitions {
  var distances: HashMap[(Set[AbstractPolygon], LinkageMode), Double] = new HashMap()
  distanceInit(obstacles)
  val startClusters: List[Cluster] = ClusterInit()
  val SingleCluster: Cluster = clustering(Single)
  def hacClustering(distanceThreshold: Double, linkageMode: Option[LinkageMode]): List[List[AbstractPolygon]] = {
    val linkage: LinkageMode = linkageMode match {
      case Some(d) => d
      case None    => Single
    }
//    distanceInit(obstacles)
    val cluster: Cluster = linkage match{
      case Single => SingleCluster
      case m => clustering(m)
    }
    val clusters: List[Cluster] = hacClusteringRecursive(cluster, distanceThreshold)
    val output: List[List[AbstractPolygon]] = clusters.map { x => x.data }
    return output
  }
  def hacClusteringRecursive(cluster: Cluster, distanceThreshold: Double): List[Cluster] = {
    if (cluster.distance >= distanceThreshold) {
      cluster match {
        case Cluster(Some(l), Some(r), _, _) => return hacClusteringRecursive(l, distanceThreshold) ++ hacClusteringRecursive(r, distanceThreshold)
        case Cluster(Some(l), None, _, _)    => return hacClusteringRecursive(l, distanceThreshold)
        case Cluster(None, Some(r), _, _)    => return hacClusteringRecursive(r, distanceThreshold)
        case Cluster(_, _, _, Some(p))       => return cluster :: Nil
        case Cluster(None, None, _, None)    => throw new IllegalArgumentException
      }
    }
    return cluster :: Nil
  }
  def clustering(linkage: LinkageMode): Cluster = hac(startClusters, linkage)

  //Idea: Precompute the distances between individual obstacles once, then just recall them accordingly for computing distances between clusters
  
  def distanceInit(obstacles: List[AbstractPolygon]): Unit = {
    for (i: AbstractPolygon <- obstacles; j: AbstractPolygon <- obstacles) {
      val pairs: List[(OccupyPoint, OccupyPoint)] = for (k: OccupyPoint <- i.getBorder; l: OccupyPoint <- j.getBorder) yield (k, l)
      val min = pairs.minBy(x => PointArithmetic.distance(x._1, x._2))
      val max = pairs.maxBy(x => PointArithmetic.distance(x._1, x._2))
      val avg = PointArithmetic.distance(i.centroid(), j.centroid())
      val minValue = PointArithmetic.distance(min._1, min._2)
      val maxValue = PointArithmetic.distance(max._1, max._2)
      val set: Set[AbstractPolygon] = Set(i, j)
      distances.put((set, Complete), maxValue)
      distances.put((set, Single), minValue)
      distances.put((set, Centroid), avg)
//      distances.+=((set, Complete) -> maxValue)
//      distances.+=((set, Single) -> minValue)
//      distances.+=((set, Centroid) -> avg)
    }
  }
  def distanceInit(): Unit = distanceInit(obstacles)
  def distanceInitAllPoints(obstacles: List[AbstractPolygon]): Unit = {
    for (i: AbstractPolygon <- obstacles; j: AbstractPolygon <- obstacles) {
      val pairs: List[(OccupyPoint, OccupyPoint)] = for (k: OccupyPoint <- i.vis(); l: OccupyPoint <- j.vis()) yield (k, l)
      val min = pairs.minBy(x => PointArithmetic.distance(x._1, x._2))
      val max = pairs.maxBy(x => PointArithmetic.distance(x._1, x._2))
      val avg = PointArithmetic.distance(i.centroid(), j.centroid())
      val minValue = PointArithmetic.distance(min._1, min._2)
      val maxValue = PointArithmetic.distance(max._1, max._2)
      distances.+=((Set(i, j), Complete) -> maxValue)
      distances.+=((Set(i, j), Single) -> minValue)
      distances.+=((Set(i, j), Centroid) -> avg)
//      (i,j) match{
//        case (m,n) => if(m == new InvariantObstacle(OccupyBox(29,69,30,70)) && 
//            n == new InvariantObstacle(OccupyBox(36,63,37,70))) 
//          debug.putKey = (i, j, Single) 
////        else debug.putKey = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
//        case _ => //do nothing
//      }
//      (i,j) match{
//        case (InvariantObstacle(OccupyBox(a,b,c,d)), InvariantObstacle(OccupyBox(a2,b2,c2,d2))) => {
//          println("this did match");
//          if((a,b,c,d,a2,b2,c2,d2) == (29,69,30,70,36,63,37,70)) debug.putKey = (i, j, Single)
////          else debug.putKey = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
//        }
////          if((a,b,c,d,a2,b2,c2,d2) == (29,69,30,70,36,63,37,70)) debug.putKey = (i, j, Single)
////          else debug.putKey = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
//        case _ => println("this did not match")
//      }
//      
//      println((i, j, Single) + "" + distances.apply((Set(i, j), Single)))
    }
  }
  def distance(first: Cluster, second: Cluster, linkage: LinkageMode): Double = {
    val pairs: List[(AbstractPolygon, AbstractPolygon)] = for (i: AbstractPolygon <- first.data; j: AbstractPolygon <- second.data) yield (i, j)
//    println(distances)
    val min = pairs.minBy(x => {
      val currentKey = (x._1, x._2, Single)
//      println(currentKey)
//      println(debug.putKey)
//      assert(currentKey == debug.putKey);
      distances.apply((Set(x._1, x._2), Single))})
    val max = pairs.maxBy(x => distances.apply((Set(x._1, x._2), Complete)))
    val avg = pairs.minBy(x => distances.apply((Set(x._1, x._2), Centroid))) //?
    val minValue = distances.apply((Set(min._1, min._2), Single))
    val maxValue = distances.apply((Set(max._1, max._2), Complete))
    val avgValue = distances.apply((Set(min._1, min._2), Centroid))
    linkage match {
      case Complete => return maxValue
      case Single   => return minValue
      case Centroid => return avgValue
    }
  }
  def ClusterInit(): List[Cluster] = {
    return obstacles.map { x => Cluster(None, None, 0.0, Some(List(x))) }
  }
  def hac(clusters: List[Cluster], linkage: LinkageMode): Cluster = {
    if (clusters.length <= 1) {
      return clusters.head
    }
    val allPairs: List[(Cluster, Cluster)] = for (i: Cluster <- clusters; j: Cluster <- clusters) yield (i, j)
    val pairs: List[(Cluster, Cluster)] = allPairs.filterNot(x => x._1 == x._2)
    val minPair = pairs.minBy(x => distance(x._1, x._2, linkage))
    val newCluster = Cluster(Some(minPair._1), Some(minPair._2), distance(minPair._1, minPair._2, linkage), None)
    return hac(clusters.filterNot { x => x == minPair._1 || x == minPair._2 } ++ List(newCluster), linkage)
  }
  //Methods for convenience
  def listClusters(start: Cluster): List[Cluster] = {
    start match{
      case Cluster(Some(l), Some(r), _, _) => return start :: listClusters(l) ++ listClusters(r)
      case Cluster(Some(l), None, _, _) => return start :: listClusters(l)
      case Cluster(None, Some(r), _, _) => return start :: listClusters(r)
      case Cluster(None, None, _, _) => return start :: Nil
    }
  }
  def initClusters(clusters: List[List[AbstractPolygon]]): List[Cluster] = {
    distanceInit(clusters.flatten)
    return clusters.map { x => Cluster(None, None, 0.0, Some(x)) }
  }
  //Methods for consistency testing
  def moreDifferentThanChildren(start: Cluster): Boolean = {
    return listClusters(start).forall { x => x match{
        case Cluster(Some(l), Some(r), d, _) => d >= l.distance && d >= r.distance
        case Cluster(Some(l), None, d, _) => d >= l.distance
        case Cluster(None, Some(r), d, _) => d >= r.distance
        case _ => true
      }
    }
  }
  def includesChildren(start: Cluster): Boolean = {
    return listClusters(start).forall { x => x match{
        case Cluster(Some(l), Some(r), _, _) => x.data == l.data ++ r.data//x.data.containsSlice(l.data) && x.data.containsSlice(r.data)
        case Cluster(Some(l), None, _, _) => x.data == l.data
        case Cluster(None, Some(r), _, _) => x.data == r.data
        case _ => true
      }
    }
  }
  def consistencyCheck(start: Cluster): Boolean = moreDifferentThanChildren(start) && includesChildren(start)
}
case class Cluster(left: Option[Cluster], right: Option[Cluster], distance: Double, payload: Option[List[AbstractPolygon]]) {
  def data: List[AbstractPolygon] = {
    payload match {
      case Some(d) => return d
      case None => {
        this match {
          case Cluster(Some(l), Some(r), _, _) => return l.data ++ r.data
          case Cluster(None, Some(r), _, _)    => return r.data
          case Cluster(Some(l), None, _, _)    => return l.data
        }
      }
    }
  }
}
trait LinkageMode
case object Complete extends LinkageMode
case object Single extends LinkageMode
case object Centroid extends LinkageMode

object debug{
  type Key = (AbstractPolygon, AbstractPolygon, LinkageMode)
  var getKey: Key = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
  var putKey: Key = (new InvariantObstacle(OccupyBox(0,0,0,0)), new InvariantObstacle(OccupyBox(0,0,0,0)), Single)
}
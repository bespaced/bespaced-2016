package template
import BeSpaceDCore._

//FirstAbstraction is a Convex Hull and later a Visibility Graph
class FirstAbstraction extends CoreDefinitions with Abstraction {
  def abstractClusters(clustering: Clustering): Clustering = {
    clustering.getData().foreach { x => abstractCluster(x) }//Does this modify the list? It returns a unit.
    var output : Clustering = new FirstClustering()
    output.setData(clustering.getData())
    return output
  }
  def abstractCluster(cluster: oldCluster): oldCluster = {
    var points: List[OccupyPoint] = Nil
    cluster.getData().foreach { x =>
      x match {
        case OccupyPoint(a, b)     => points = OccupyPoint(a, b) :: points
        case OccupyBox(a, b, c, d) => points = OccupyPoint(a, b) :: OccupyPoint(a, d) :: OccupyPoint(c, b) :: OccupyPoint(c, d) :: points
        case _                     => throw new Exception()
      }
    }
    val hull : ConvexHull = new ConvexHull()
    val list : List[OccupyPoint] = hull.convexHull(points)
    var output : oldCluster = new oldCluster()
    output.setData(list)
    return output
  }
  def GenerateGraph(clustering: Clustering): FirstGraph[OccupyPoint] = {
    var graph : FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]()
    ???
  }
}
package template

import BeSpaceDCore._

object CollisionTestVis extends CoreDefinitions with PointVis{
  def main(args: Array[String]): Unit = {
    val points : List[OccupyPoint] = List(OccupyPoint(3,79), OccupyPoint(95,79), OccupyPoint(98,73), OccupyPoint(99,69), OccupyPoint(99,30), OccupyPoint(96,25), OccupyPoint(65,24), OccupyPoint(41,24), OccupyPoint(21,37), OccupyPoint(3,74))
    val obstacle : AbstractPolygon = new Polygon(points)
    val point : OccupyPoint = OccupyPoint(51,39)
    val scene = new FirstSceneGenerator().Generator()
//    println(new InvariantObstacle(scene._3.head).collision(point))
    println(obstacle.collision(point))
    Vis(point::Nil, scalafx.scene.paint.Color.Red)
//    Vis(new InvariantObstacle(scene._3.head).vis(), scalafx.scene.paint.Color.Blue)
    Vis(obstacle.vis(), scalafx.scene.paint.Color.Blue)
    Draw()
  }
}
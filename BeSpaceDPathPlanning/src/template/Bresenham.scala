package template

import BeSpaceDCore._

/**
 * @author Carl
 *
 * Adapted from http://groups.csail.mit.edu/graphics/classes/6.837/F99/grading/asst2/turnin/rdror/Bresenham.java
 */
object Bresenham {
  def main(args: Array[String]): Unit = {

  }
  def BresenhamAlgorithm(a: OccupyPoint, b: OccupyPoint): List[OccupyPoint] = {
    var x1: Int = a.x
    var x2: Int = b.x
    var y1: Int = a.y
    var y2: Int = b.y
    var output: List[OccupyPoint] = Nil

    // If slope is outside the range [-1,1], swap x and y
    var xy_swap: Boolean = false
    if (Math.abs(y2 - y1) > Math.abs(x2 - x1)) {
      xy_swap = true
      var temp: Int = x1
      x1 = y1;
      y1 = temp;
      temp = x2;
      x2 = y2;
      y2 = temp;
    }

    // If line goes from right to left, swap the endpoints
    if (x2 - x1 < 0) {
      var temp: Int = x1;
      x1 = x2;
      x2 = temp;
      temp = y1;
      y1 = y2;
      y2 = temp;
    }

    var x: Int = x1
    var y: Int = y1
    var e: Int = 0
    var m_num: Int = y2 - y1
    var m_denom: Int = x2 - x1
    var threshold: Int = m_denom / 2
    while (x < x2) {
      if (xy_swap) {
        output = OccupyPoint(y, x) :: output
      } else {
        output = OccupyPoint(x, y) :: output
      }
      e = e + m_num
      // Deal separately with lines sloping upward and those
      // sloping downward
      if (m_num < 0) {
        if (e < -threshold) {
          e += m_denom;
          y -= 1;
        }
      } else if (e > threshold) {
        e -= m_denom;
        y += 1;
      }

      x = x + 1
    }
    if (xy_swap) {
      output = OccupyPoint(y, x) :: output
    } else {
      output = OccupyPoint(x, y) :: output
    }
    output = output.reverse
    return output
  }
}
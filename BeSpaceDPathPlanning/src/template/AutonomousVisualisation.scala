package template

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.geometry._
import BeSpaceDCore._
import javax.imageio.ImageIO
import scalafx.embed.swing._
import scalafx.scene.image.WritableImage
import java.io._
import BeSpaceDData._
//import z3test.test1
//import com.microsoft.z3._
import scala.collection.mutable.HashMap

case class Image(X: Int, Y: Int, cells: List[(OccupyBox, Color)], points: List[(List[OccupyPoint], Color)], magnificationFactor: Double, fileName: String, label: String)

//There might be more elegant and generic ways of dealing with multiple objects to be drawn at different points in the code. An idea is to use Platform.runLater
//https://homepages.thm.de/~hg51/Veranstaltungen/NVP_15_16/Aufgaben/nvpA-01.pdf

object AutonomousVisualisation extends JFXApp with PointVis {
  //  def OccupyPointVis(points: List[OccupyPoint], colour: Color): Unit = {
  //    val points : List[OccupyPoint] = MyParameters.points

  def drawScene(i: Image) = {
    i match{
      case Image(x, y, cells, points, magnificationFactor, fileName, label) => drawSceneLabel(x, y, cells, points, magnificationFactor, fileName, label)
    }
  }
  setArchivePath("E:/Dropbox/Uni/Praktikum/bespaced-2016z/data")
  val filePath: String = "E:/Dropbox/Uni/Praktikum/bespaced-2016z/CarlTemplate/stored_output/"
  def densityViewForDebug(points: HashMap[(Int, Int), (Double, Int)], densityFactor: Int): List[OccupyPoint] = {
    val positions: List[OccupyPoint] = points.filter {
      x => x._2._2 <= densityFactor
    }.map {
      x => OccupyPoint(x._1._1, x._1._2)
    }.toList
    return positions
  }
  def densityViewForDebugUpper(points: HashMap[(Int, Int), (Double, Int)], densityFactor: Int): List[OccupyPoint] = {
    val positions: List[OccupyPoint] = points.filter {
      x => x._2._2 >= densityFactor
    }.map {
      x => OccupyPoint(x._1._1, x._1._2)
    }.toList
    return positions
  }
  def minClearancePointForOutput(mp: Option[OccupyPoint]): OccupyPoint = mp match {
    case None    => OccupyPoint(-1, -1)
    case Some(d) => d
  }
  def twoLinesCSV(a: String, b: String): String = {
    val a1: Array[String] = a.split("\n").map { x => x.trim() }
    val b1: Array[String] = b.split("\n").map { x => x.trim() }
    val z: Array[(String, String)] = a1 zip b1
    var output: String = ""
    z.foreach {
      f => output += f._1 + f._2 + "\n"
    }
    return output
  }
  def parametersCSV(p: Configuration): String = {
    p match {
      case Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic) => {
        "dataSetTitle0:,dataSetTitle1:,dataSetTitle2:,dataSetTitle3:,dataSetTitle4:,dataSetTitle5:,dataSetTitle6:,dataSetTitle7:,dataSetTitle8:,dataSetTitle9:,dataSetTitle10:,dataSetTitle11:,dataSetTitle12:,dataSetTitle13:,dataSetTitle14:,dataSetTitle15:,z plane:,resolution:,magnification factor:,minimum cell size:,startX:,startY:,targetX:,targetY:,empty:,full:,cutoffMinX:,cutoffMaxX:,cutoffMinY:,cutoffMaxY:,densityFilterLevel:,filtered by density:,post-processing by density:,shortening enabled:,heuristic:," +
          '\n' + dataSetTitle.apply(0) + "," + dataSetTitle.apply(1) + "," + dataSetTitle.apply(2) + "," + dataSetTitle.apply(3) + "," + dataSetTitle.apply(4) + "," + dataSetTitle.apply(5) + "," + dataSetTitle.apply(6) + "," + dataSetTitle.apply(7) + "," + dataSetTitle.apply(8) + "," + dataSetTitle.apply(9) + "," + dataSetTitle.apply(10) + "," + dataSetTitle.apply(11) + "," + dataSetTitle.apply(12) + "," + dataSetTitle.apply(13) + "," + dataSetTitle.apply(14) + "," + dataSetTitle.apply(15) + "," + zPlane + "," + resolution + "," + magnificationFactor + "," + minSize + "," + start.x + "," + start.y + "," + target.x + "," + target.y + "," + empty + "," + full + "," + borders.minX + "," + borders.maxX + "," + borders.minY + "," + borders.maxY + "," + densityFilterLevel + "," + densityFilter + "," + densityPostProcessingThreshold + "," + shorteningEnabled + "," + heuristic + ","
      }
    }
  }
  def measurementsCSV(m: Results): String = {
    m match {
      case Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images) => {
        "QuadTree operations performed in:,Extracted free space in:,Graph generated in:,A* performed in:,Euclidian length of the path:,Number of corner points of the path:,Minimum Clearance:,Minimum Clearance at X:,Minimum Clearance at Y:,Number of free cells:,Number of all cells:,Time for shortening the path:," +
          '\n' + qtime + "," + ftime + "," + gtime + "," + atime + "," + length + "," + corners + "," + minClearance + "," + minClearancePointForOutput(minClearancePoint).x + "," + minClearancePointForOutput(minClearancePoint).y + "," + numFreeCells + "," + numAllCells + "," + stime + ","
      }
    }
  }
  val configsAndResults: List[(Configuration, Results)] = KinectScanDataTesterA.multiTesterWithReturnValue(10)//KinectScanDataTesterA.singleTester//obstacleMultiParameterTest(28) //obstacleMultiParameterTest(32)
  println(s"stage = $stage" + java.util.Calendar.getInstance.getTime)
  writeToFile(configsAndResults)
  
  def writeToFileSingle(configsAndResults: (Configuration, Results)) = {
    writeToFile(List(configsAndResults))
  }
  def writeToFile(configsAndResults: List[(Configuration, Results)]) = {
    val num = 0 to configsAndResults.length
    (num zip configsAndResults).foreach {
      f =>
        f match {
          case (n, (Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic), Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images))) => {
            /*val measurements: String = "QuadTree operations performed in " + qtime + '\n' + "Extracted free space in " +
            ftime + '\n' + "Graph generated in " + gtime + '\n' + "A* performed in " + atime + '\n' +
            "euclidian length of the path: " + length + '\n' +
            "number of corner points of the path: " + corners + '\n' +
            "minimum clearance: " + minClearance + " at " + minClearancePoint + '\n' +
            "number of free cells: " + numFreeCells + '\n' + "number of all cells: " + numAllCells

          val measurementsCSV: String = "QuadTree operations performed in: , Extracted free space in: , Graph generated in: , A* performed in: , Euclidian length of the path: , Number of corner points of the path: , Minimum Clearance: , Minimum Clearance at X: , Minimum Clearance at Y: , Number of free cells: , Number of all cells: " +
            '\n' + qtime + "," + ftime + "," + gtime + "," + atime + "," + length + "," + corners + "," + minClearance + "," + minClearancePointForOutput.x + "," + minClearancePointForOutput.y + "," + numFreeCells + "," + numAllCells
          val parameters: String = "dataSetTitle: " + dataSetTitle + '\n' +
            "z plane: " + zPlane + '\n' +
            "resolution: " + resolution + '\n' + "magnification factor: " + magnificationFactor + '\n' +
            "minimum cell size: " + minSize + '\n' + "start: " + start + '\n' + "target: " + target + '\n' +
            "empty: " + empty + '\n' + "full: " + full + '\n' + "cutOff: " + borders
          val parametersCSV: String = "dataSetTitle: , z plane: , resolution: , magnification factor: , minimum cell size: , startX: , startY: , targetX: , targetY: , empty: , full: , cutoffMinX: , cutoffMaxX: , cutoffMinY: , cutoffMaxY: " +
            '\n' + zPlane + "," + resolution + "," + magnificationFactor + "," + minSize + "," + start.x + "," + start.y + "," + target.x + "," + target.y + "," + empty + "," + full + "," + borders.minX + "," + borders.maxX + "," + borders.minY + "," + borders.maxY*/

            val timeStamp: Long = System.currentTimeMillis()
            val pw = new PrintWriter(new File(filePath + timeStamp + ".csv"))
            pw.write("Measurements:" + '\n')
            pw.write(measurementsCSV(Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images)) + '\n')
            pw.write("Parameters:" + '\n')
            pw.write(parametersCSV(Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic)))
            pw.close
            val pwp = new PrintWriter(new File(filePath + timeStamp + "parameters" + ".csv"))
            pwp.write("Parameters:" + '\n')
            pwp.write(parametersCSV(Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic)))
            pwp.close
            val pwr = new PrintWriter(new File(filePath + timeStamp + "results" + ".csv"))
            pwr.write("Measurements:" + '\n')
            pwr.write(measurementsCSV(Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images)) + '\n')
            pwr.close
            val sideBySide = new PrintWriter(new File(filePath + timeStamp + "twoLines" + ".csv"))
            val c: String = parametersCSV(Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold, shorteningEnabled, heuristic))
            val m: String = measurementsCSV(Results(pathCorners, path, leaves, qtime, ftime, gtime, atime, length, corners, minClearance, minClearancePoint, pointWorkingSet, numFreeCells, numAllCells, stime, images))
            val twoLines: String = twoLinesCSV(c, m)
            sideBySide.write(twoLines)
            sideBySide.close
            val cells = leaves.map { x =>
              if (x.density <= x.empty) (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 0, 255, 0.1))
              else if (x.density > x.full) (KinectScanDataTesterA.cellBox(x), Color.rgb(255, 0, 0, 0.1))
              else (KinectScanDataTesterA.cellBox(x), Color.rgb(0, 255, 0, 0.1))
            }
            val pointsOnPath = path.map { x =>
              if (!(minClearancePoint.isEmpty) && x == minClearancePoint.get) (x :: Nil, Color.Red)
              else (x :: Nil, Color.Green)
            }
            var pointCloud: List[OccupyPoint] = Nil
            pointWorkingSet.foreach {
              x => pointCloud = OccupyPoint(x._1._1, x._1._2) :: pointCloud
            }
            //The following code in block comment writes images with the points with different filter parameters.
            /*val factors: List[Int] = List.range(1, 10)
          factors.foreach { x =>
            val densePoints: List[OccupyPoint] = densityViewForDebug(pointWorkingSet, x)
            println(densePoints.size)
            println(pointCloud.size.toDouble / densePoints.size.toDouble)
            drawScene(magnificationFactor, magnificationFactor, Nil, ((densePoints, Color.Black) :: Nil), 1, n + "test" + x)
            val densePoints2: List[OccupyPoint] = densityViewForDebugUpper(pointWorkingSet, x)
            println(densePoints2.size)
            println(pointCloud.size.toDouble / densePoints2.size.toDouble)
            drawScene(magnificationFactor, magnificationFactor, Nil, ((densePoints2, Color.Black) :: Nil), 1, n + "testUpper" + x)
          }*/
            //          val densityFactor: Int = 10
            //          println(pointCloud.size)

            //I'd find it interesting to see if the following code works...
            //          val pointCloudMap = pointWorkingSet.map { x => (OccupyPoint(x._1._1, x._1._2), Color.Black) }
            //          val pointCloud = pointCloudMap.toList
            //          val minClearancePointList: List[(List[OccupyPoint], Color)] = ((minClearancePoint :: Nil, Color.Yellow) :: Nil)
            images.foreach { x => drawScene(x) }
            drawScene(magnificationFactor, magnificationFactor, cells, pointsOnPath ++ /*minClearancePointList ++ */ ((pointCloud, Color.Black) :: Nil), 1, timeStamp + "")
            drawSceneLabel(magnificationFactor, magnificationFactor, cells, pointsOnPath ++ /*minClearancePointList ++ */ ((pointCloud, Color.Black) :: Nil), 1, timeStamp + "" + "labeled", "" + heuristic + "\t" + (qtime + ftime + gtime + atime) + "\t" + "\t" + length)
          }

        }
    }
  }
  
  /*val ColorList = List(Color.BLUE, Color.Red, Color.Yellow, Color.Green, Color.Pink)
  val numbers = 1 until 6
  val mixed = ColorList zip numbers
  mixed.foreach { x => stageDefine(x._1, x._2) }
  def stageDefine(currentColor: Color, number: Int) = {
    stage = new PrimaryStage {
      title = "SecondVis"
      val myScene = new Scene(1024, 1024) {
        val canvas = new Canvas(1024, 1024)
        val gc = canvas.graphicsContext2D
        gc.fill_=(currentColor)
        gc.fillRect(43.6, 21.8, 65.3, 54.9)
        content = canvas
      }
      scene = myScene
      println("Third image to be written")
      var myImage: WritableImage = new WritableImage(1024, 1024)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File(number + ".png"))
      println("Third image written")
    }
  }*/

  def drawScene(X: Int, Y: Int, cells: List[(OccupyBox, Color)], points: List[(List[OccupyPoint], Color)], magnificationFactor: Double, fileName: String) = {
    stage = new PrimaryStage {
      title = fileName
      val myScene = new Scene(X, Y) {
        val canvas = new Canvas(X, Y)
        val gc = canvas.graphicsContext2D
        points.foreach {
          f => gc.fill_=(f._2); f._1.foreach { x => gc.fillRect(x.x * magnificationFactor, x.y * magnificationFactor, magnificationFactor, magnificationFactor) }
        }
        cells.foreach {
          f =>
            gc.fill_=(f._2);
            f._1 match {
              case OccupyBox(x1, y1, x2, y2) => gc.fillRect(x1 * magnificationFactor, y1 * magnificationFactor, (x2 - x1 + 1) * magnificationFactor, (y2 - y1 + 1) * magnificationFactor)
            }
        }
        content = canvas
      }
      scene = myScene
      var myImage: WritableImage = new WritableImage(X, Y)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File(filePath + fileName + ".png"))
    }
  }
  def drawSceneLabel(X: Int, Y: Int, cells: List[(OccupyBox, Color)], points: List[(List[OccupyPoint], Color)], magnificationFactor: Double, fileName: String, label: String) = {
    stage = new PrimaryStage {
      title = fileName
      val myScene = new Scene(X, Y) {
        val canvas = new Canvas(X, Y)
        val gc = canvas.graphicsContext2D
        points.foreach {
          f => gc.fill_=(f._2); f._1.foreach { x => gc.fillRect(x.x * magnificationFactor, x.y * magnificationFactor, magnificationFactor, magnificationFactor) }
        }
        cells.foreach {
          f =>
            gc.fill_=(f._2);
            f._1 match {
              case OccupyBox(x1, y1, x2, y2) => gc.fillRect(x1 * magnificationFactor, y1 * magnificationFactor, (x2 - x1 + 1) * magnificationFactor, (y2 - y1 + 1) * magnificationFactor)
            }
        }
        gc.fill_=(Color.Black)
        gc.fillText(label, 0, Y)
        content = canvas
      }
      scene = myScene
      var myImage: WritableImage = new WritableImage(X, Y)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File(filePath + fileName + ".png"))
    }
  }
  /*stage = new PrimaryStage {
      title = "FirstVis"
      val myScene = new Scene(1024, 1024) {
        val canvas = new Canvas(1024, 1024)
        val gc = canvas.graphicsContext2D
        gc.fill_=(Color.Blue)
        gc.fillRect(43.6, 21.8, 65.3, 54.9)
        content = canvas
      }
      scene = myScene
      println("First image to be written")
      var myImage: WritableImage = new WritableImage(1024, 1024)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File("output2.png"))
      println("First image written")
    }
    stage = new PrimaryStage {
      title = "Visualisation"
      val myScene = new Scene(1024, 1024) {
        val canvas = new Canvas(1024, 1024)
        val gc = canvas.graphicsContext2D
        var i : Int = 0
        while(i < VisParameters.list.size){
          gc.fill_=(VisParameters.list.apply(i)._2)
          VisParameters.list.apply(i)._1.foreach { x => gc.fillRect(x.x * VisParameters.magnifying, x.y * VisParameters.magnifying, 1 * VisParameters.magnifying, 1 * VisParameters.magnifying) }
          i = i + 1;
        }
        i = 0
        while(i < VisParameters.listBoxes.size){
          gc.fill_=(VisParameters.listBoxes.apply(i)._2)
          val box: OccupyBox = VisParameters.listBoxes.apply(i)._1
          box match{
            case OccupyBox(x1,y1,x2,y2) => gc.fillRect(x1 * VisParameters.magnifying, y1 * VisParameters.magnifying, (x2 - x1) * VisParameters.magnifying, (y2 - y1) * VisParameters.magnifying)
          }
          i = i + 1
        }
//        gc.fill_=(MyParameters.colour)
//        points.foreach { x => gc.fillRect(x.x, x.y, 1, 1) }
        content = canvas
        println("set the canvas at " + java.util.Calendar.getInstance.getTime)
      }
      scene = myScene
      println("Second image to be written")
      var myImage: WritableImage = new WritableImage(1024, 1024)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File("output1.png"))
      println("Second image written")
    }*/
  /*stage = new PrimaryStage {
      title = "SecondVis"
      val myScene = new Scene(1024, 1024) {
        val canvas = new Canvas(1024, 1024)
        val gc = canvas.graphicsContext2D
        gc.fill_=(Color.Red)
        gc.fillRect(43.6, 21.8, 65.3, 54.9)
        content = canvas
      }
      scene = myScene
      println("Third image to be written")
      var myImage: WritableImage = new WritableImage(1024, 1024)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File("output3.png"))
      println("Third image written")
    }*/
  //  }

}
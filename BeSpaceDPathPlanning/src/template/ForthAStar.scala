package template

import scala.collection.mutable.PriorityQueue
import scala.annotation.tailrec
import scala.collection.mutable.HashMap
import BeSpaceDCore._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }

object ForthAStar {
  def Search(graph: FirstGraph[OccupyPoint], start: OccupyPoint, target: OccupyPoint): (List[OccupyPoint], Double) = AStar(graph, start, target)
  def AStar(graph: FirstGraph[OccupyPoint], start: OccupyPoint, target: OccupyPoint): (List[OccupyPoint], Double) = {
    val allNodes: List[OccupyPoint] = graph.getNodes()
    if(!(allNodes.contains(start) && allNodes.contains(target))){
      return (Nil, 0)
    }
    val distanceToStart: HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]
    allNodes.foreach { x => distanceToStart.+=((x, Double.MaxValue)) }
    val distanceToTarget: HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]
    allNodes.foreach { x => distanceToTarget.+=((x, PointArithmetic.distance(x, target))) }
    val priority: HashMap[OccupyPoint, Double] = new HashMap[OccupyPoint, Double]
    allNodes.foreach { x => priority.+=((x, Double.MaxValue)) }
    val parentNode: HashMap[OccupyPoint, OccupyPoint] = new HashMap[OccupyPoint, OccupyPoint]
    /*case class helper(payload: OccupyPoint) extends Ordered[helper] {
      def compare(that: helper): Int = {
        -(priority.get(this.payload).get - priority.get(that.payload).get).signum
      }
    }*/
    def compare(a: OccupyPoint, b: OccupyPoint): Int = {
      (priority.get(a).get - priority.get(b).get).signum
    }
    var open: BinaryMinHeap[OccupyPoint] = new BinaryMinHeap[OccupyPoint](compare)
    open.+=(start)
    distanceToStart.+=((start, 0))
    priority.+=((start, distanceToTarget.get(start).get))
    while (!open.isEmpty) {
      val currentNode: OccupyPoint = open.dequeue
      if (currentNode == target) {
        val pathOutput: List[OccupyPoint] = path(target, Nil)
        val pathLengthOutput: Double = pathLength(graph, pathOutput)
        return (pathOutput, pathLengthOutput)
      }
      val successors: List[OccupyPoint] = graph.successors(currentNode)
      successors.foreach { x =>
        val dt: Double = distanceToStart.get(currentNode).get + graph.weight(currentNode, x)
        val pr: Double = dt + distanceToTarget.get(x).get
        if (!parentNode.contains(x) && !(x == start)) {
          priority.+=((x, pr))
          distanceToStart.+=((x, dt))
          parentNode.+=((x, currentNode))
          open.enqueue(x)
        } else if (open.contains(x) && dt < distanceToStart.get(x).get) {
          priority.+=((x, pr))
          distanceToStart.+=((x, dt))
          parentNode.+=((x, currentNode))
          //          val cache = open.dequeueAll
          //          cache.foreach { z => open.enqueue(z) }
          //Here I update the priority of the current node x in the priority queue.
          open.reevaluateElement(x)
          /*open = open.filterNot { z => z == x }
          open.enqueue((x))*/
        }
      }
    }
    @tailrec
    def path(end: OccupyPoint, currentPath: List[OccupyPoint]): List[OccupyPoint] = {
      val predecessor: Option[OccupyPoint] = parentNode.get(end)
      predecessor match {
        case None    => return end :: currentPath
        case Some(d) => return path(d, end :: currentPath)
      }
    }
    def pathLength(g: FirstGraph[OccupyPoint], l: List[OccupyPoint]): Double = {
      l match {
        case Nil                    => return 0.0
        case head :: Nil            => return 0.0
        case head1 :: head2 :: tail => return g.weight(head1, head2) + pathLength(g, head2 :: tail)
      }
    }
    return (Nil, 0)
  }
}
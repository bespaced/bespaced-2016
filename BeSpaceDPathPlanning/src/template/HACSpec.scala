package template

import BeSpaceDCore._

class HACSpec extends UnitSpec{
  val testObstacles: List[OccupyBox] = List(OccupyBox(29,69,30,70), OccupyBox(36,63,37,70), OccupyBox(79,43,80,44), OccupyBox(2,42,3,47), OccupyBox(20,54,21,55), OccupyBox(98,29,99,30), OccupyBox(75,36,76,37), OccupyBox(13,39,14,40), OccupyBox(44,51,59,52), OccupyBox(33,26,34,30), OccupyBox(12,34,13,35), OccupyBox(70,53,71,54), OccupyBox(19,68,34,77), OccupyBox(57,50,63,51), OccupyBox(78,23,83,24), OccupyBox(81,33,85,40), OccupyBox(20,50,21,51), OccupyBox(50,65,51,66), OccupyBox(46,70,52,71), OccupyBox(11,44,14,65), OccupyBox(20,36,21,36), OccupyBox(23,58,24,65), OccupyBox(50,45,51,54), OccupyBox(10,31,11,32), OccupyBox(83,47,90,54), OccupyBox(19,20,20,27), OccupyBox(57,50,63,51), OccupyBox(66,70,67,71), OccupyBox(46,70,47,71), OccupyBox(35,57,36,58), OccupyBox(30,32,31,38), OccupyBox(61,48,66,49), OccupyBox(67,78,68,79), OccupyBox(25,39,26,40), OccupyBox(22,56,29,57), OccupyBox(54,55,55,56), OccupyBox(39,40,40,41), OccupyBox(38,24,39,29), OccupyBox(11,45,12,58), OccupyBox(32,63,33,64), OccupyBox(46,69,47,70), OccupyBox(57,46,58,47), OccupyBox(56,21,60,22), OccupyBox(91,31,93,32), OccupyBox(44,60,45,71), OccupyBox(5,52,18,53), OccupyBox(15,64,16,64), OccupyBox(12,51,15,52), OccupyBox(29,75,30,75), OccupyBox(28,54,29,55), OccupyBox(99,21,99,41), OccupyBox(22,23,23,24), OccupyBox(77,63,78,64), OccupyBox(50,46,51,47), OccupyBox(2,33,4,34), OccupyBox(53,60,53,61), OccupyBox(69,29,70,40), OccupyBox(49,23,50,30), OccupyBox(18,48,19,51), OccupyBox(57,69,58,71), OccupyBox(50,69,51,70), OccupyBox(55,30,56,35), OccupyBox(50,30,53,31), OccupyBox(6,26,7,26), OccupyBox(27,52,28,61), OccupyBox(50,68,57,69), OccupyBox(31,42,37,43), OccupyBox(71,79,76,79), OccupyBox(31,25,32,33), OccupyBox(82,50,83,52), OccupyBox(86,49,87,50), OccupyBox(15,28,16,51), OccupyBox(6,60,7,61), OccupyBox(58,27,75,40), OccupyBox(39,54,40,55)).take(10)
  val abstractPolygons: List[AbstractPolygon] = testObstacles.map { x => new InvariantObstacle(x) }
  val hac: HAC = new HAC(abstractPolygons)
  "consistencyCheck" should "return true for HAC of the 75 obstacles test set" in {
    hac.distanceInit(abstractPolygons)
    println("initialized")
    val top: Cluster = hac.clustering(Single)
    println("clustering computed")
    val result: Boolean = hac.consistencyCheck(top)
    println("more different than children?: " + hac.moreDifferentThanChildren(top))
    println("same data as children?: " + hac.includesChildren(top))
    println(hac.listClusters(top))
    assertResult(expected = true)(actual = result)
  }
  "initClusters" should "return the same as hacClusteringRecursive(verySmall, Single)" in {
    hac.distanceInit(abstractPolygons)
    val connectivity: List[Cluster] = hac.initClusters(new ConnectivityClustering().findConnectedObstacles(abstractPolygons))
    val connectivityObstacles: List[List[AbstractPolygon]] = connectivity.map { x => x.data }
    val connectivityObstaclesSet: Set[List[AbstractPolygon]] = connectivityObstacles.toSet
    println("Connectivity: " + connectivityObstaclesSet)
    val zeroDifference: List[Cluster] = hac.hacClusteringRecursive(hac.clustering(Single), Double.MinPositiveValue)
    val zeroDifferenceObstacles: List[List[AbstractPolygon]] = zeroDifference.map { x => x.data }
    val zeroDifferenceObstaclesSet: Set[List[AbstractPolygon]] = zeroDifferenceObstacles.toSet
    println("zeroDifference: " + connectivityObstaclesSet)
    assertResult(expected = true)(actual = zeroDifferenceObstaclesSet == connectivityObstaclesSet)
  }
}
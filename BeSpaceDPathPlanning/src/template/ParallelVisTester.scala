package template

import scalafx.stage.FileChooser
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint.{ Stops, LinearGradient, Color }
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.geometry._
import BeSpaceDCore._
import javax.imageio.ImageIO
import scalafx.embed.swing._
import scalafx.scene.image.WritableImage
import java.io._
import BeSpaceDData._
import z3test.test1
import com.microsoft.z3._
import scala.collection.mutable.HashMap
import scala.io.Source

object ParallelVisTester {
  SimpleVis
  def main(args: Array[String]): Unit = {
//    SimpleVis.main(Array())
    
    println("Has called main")
    (1 to 10).foreach { x => SimpleVis.drawSceneLabel(100, 100, Nil, Nil, 1, "" + (x + 10), "" + x) }
  }

}

object SimpleVis extends JFXApp {
//  ParallelVisTester
  def filePath = "E:/Dropbox/Uni/Praktikum/bespaced-2016z/CarlTemplate/stored_output/parallelizationTest/"
  def drawSceneLabel(X: Int, Y: Int, cells: List[(OccupyBox, Color)], points: List[(List[OccupyPoint], Color)], magnificationFactor: Double, fileName: String, label: String) = {
    println(label)
    stage = new PrimaryStage {
      title = fileName
      val myScene = new Scene(X, Y) {
        val canvas = new Canvas(X, Y)
        val gc = canvas.graphicsContext2D
        points.foreach {
          f => gc.fill_=(f._2); f._1.foreach { x => gc.fillRect(x.x * magnificationFactor, x.y * magnificationFactor, magnificationFactor, magnificationFactor) }
        }
        cells.foreach {
          f =>
            gc.fill_=(f._2);
            f._1 match {
              case OccupyBox(x1, y1, x2, y2) => gc.fillRect(x1 * magnificationFactor, y1 * magnificationFactor, (x2 - x1 + 1) * magnificationFactor, (y2 - y1 + 1) * magnificationFactor)
            }
        }
        gc.fill_=(Color.Black)
        gc.fillText(label, 0, Y)
        content = canvas
      }
      scene = myScene
      var myImage: WritableImage = new WritableImage(X, Y)
      myImage = myScene.snapshot(myImage)
      ImageIO.write(SwingFXUtils.fromFXImage(myImage, null), "png", new java.io.File(filePath + fileName + ".png"))
    }
  }
}
package template

import BeSpaceDCore._
import Bresenham._

object BresenhamVis extends PointVis{
  def main(args: Array[String]): Unit = {
    val a: OccupyPoint = OccupyPoint(98,31)
    val b: OccupyPoint = OccupyPoint(87,1)
    val points: List[OccupyPoint] = Bresenham.BresenhamAlgorithm(a, b)
    Vis(a::Nil, scalafx.scene.paint.Color.Red)
    Vis(b::Nil, scalafx.scene.paint.Color.Red)
    Vis(points, scalafx.scene.paint.Color.Blue)
    Draw()
  }
}
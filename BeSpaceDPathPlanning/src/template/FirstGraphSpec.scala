package template
import BeSpaceDCore.Log._
import BeSpaceDCore._

class FirstGraphSpec extends UnitSpec{
  "addNodes" should "not alter the list if the node is already included" in
  {
    var myGraph : FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]()
    myGraph.addNode(OccupyPoint(3,5))
    myGraph.addNode(OccupyPoint(3,5))
    val result = myGraph.getNodes().size
    assertResult(expected = 1)(actual = result )
  }
}
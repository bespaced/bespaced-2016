package template

import BeSpaceDCore._



/**
 * @author Carl
 *
 * Not strictly functional and therefore hard to test.
 */
class BinaryMinHeapSpec extends UnitSpec {
  
  "insert 87321 to an empty list and then peak" should "return Some(87321)" in {
    val testHeap: BinaryMinHeap[Int] = new BinaryMinHeap[Int](_ - _)
    testHeap.insert(87321)
    val result = testHeap.peak
    assertResult(expected = Some(87321))(actual = result)
  }
  "dequeueAll" should "return elements in order" in {
    val testHeap: BinaryMinHeap[Int] = new BinaryMinHeap[Int](_ - _)
    val input: List[Int] = List.fill(5000)((Math.random() * 100000).toInt).distinct//List(77,17,26,81,27,42,49,12,38,84,19,78,41)//List.fill(15)((Math.random() * 100).toInt).distinct
    val sortedInput: List[Int] = input.sortWith(_ < _)
    testHeap.addAll(input)
    val output: List[Int] = testHeap.dequeueAll
    assertResult(expected = sortedInput)(actual = output)
  }
}
/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 *  J. O. Blech 2015
 * 
 *  Extracted from CoreDefinitions Feb 2015
 *
 *  licensed under Apache 2.0 (  )
 */

package BeSpaceDCore


// -------------------------------------------------------------------------------------------------------------- Foundational constructs


/*
 * Our Invariants
*/


trait Invariant extends Ordered[Invariant]
{
  def compare(that: Invariant) = this.toString.compare(that.toString)
} 

trait ATOM extends Invariant



// -------------------------------------------------------------------------------------------------------------- Numerical constructs

// TODO: Should these extend ATOM ?

// Symbolic Scalar
trait SS[+N <: Number] extends Jsonable
{
  def myType: String
  def myJsonExpression: String
  
  override def toJson = 
  {
    val jsonType       = s"""{"type": "$myType""""
    val jsonExpression = s""", "expression": $myJsonExpression"""
    val jsonEnd        = "}"
    
    jsonType + jsonExpression + jsonEnd
  }
}

case class SS_C[+N <: Number] (c: N) extends SS[N]
{
  def myType = "SS Constant"
  def myJsonExpression = c.toString
}

case class SS_Var [+N <: Number, +V](v : V) extends SS[N]
{
  def myType = "SS Variable"
  def myJsonExpression = s"""${BeSpaceDCore.toJson(v)}"""
}

case class SS_Add[+N <: Number] (i1 : SS[N], i2 : SS[N]) extends SS[N]
{
  def myType = "SS Addition"
  def myJsonExpression =  s"""[${i1.toJson}, ${i2.toJson}]"""
}

// Symbolic Int
trait SI extends SS[Integer]

class SI_C (c:Int) extends SS_C(new Integer(c)) with SI
{
  override def myType = "SI Constant"
  override def myJsonExpression = super.myJsonExpression
}
object SI_C { def apply(c:Integer): SI_C = new SI_C(c); def unapply(obj: SI_C) = Some((obj.c)) }

class SI_Var[+V](v : V) extends SS_Var[Integer, V](v) with SI
{
  override def myType = "SI Variable"
  override def myJsonExpression = super.myJsonExpression
}
object SI_Var { def apply[V](v: V): SI_Var[V] = new SI_Var(v); def unapply[V](obj: SI_Var[V]) = Some((obj.v)) }

class SI_Add (i1 : SI, i2 : SI) extends SS_Add(i1, i2) with SI
{
  override def myType = "SI Addition"
  override def myJsonExpression = super.myJsonExpression
}
object SI_Add {
  def apply(i1 : SI, i2 : SI): SI =
  {
    var result: SI = null
  
    if (i2.isInstanceOf[SI_C])
    {
      val constant2: Int = i2.asInstanceOf[SI_C].c.toInt
      if (constant2 < 0)
      {
        result = SI_Sub( i1, SI_C( - constant2 ) )
      }
    }
    
    if (result == null) result = new SI_Add(i1, i2)

    result
  }
  
  def unapply[V](obj: SI_Add):Option[(SI,SI)] = Some((obj.i1.asInstanceOf[SI], obj.i2.asInstanceOf[SI]))
}

case class SI_Sub (i1 : SI, i2 : SI) extends SI
{
  def myType = "SI Subtraction"
  def myJsonExpression =  s"""{"left": ${i1.toJson}, "right": ${i2.toJson}}""" 

  def apply(i1 : SI, i2 : SI): SI =
  {
    var result: SI = null
  
    if (i2.isInstanceOf[SI_C])
    {
      val constant2: Int = i2.asInstanceOf[SI_C].c.toInt
      if (constant2 < 0)
      {
        result = SI_Add( i1, SI_C( - constant2 ) )
      }
    }
    
    if (result == null) result = new SI_Sub(i1, i2)

    result
  }
}

case class SI_Times (i1 : SI, i2 :SI) extends SI
{
  def myType = "SI Multiplication"
  def myJsonExpression =  s"""[${i1.toJson}, ${i2.toJson}]""" 
}



// -------------------------------------------------------------------------------------------------------------- Time constructs

// ---------------------------------------------------------------- Points

@deprecated case class IntTimeStamp (timestamp : Int) extends ATOM
@deprecated case class TimeStamp [T] (timestamp : T) extends ATOM // the generalized version of IntTimeStamp; new terminology: time point?
@deprecated case class TimePeriod [T](timestamp1 : T, timestamp2 : T) extends ATOM // new terminology: time interval?

// Points
case class TimePoint [+T] (timepoint : T) extends ATOM // new terminology, but not yet implemented 04/02/2014 !
object TimePoint
{
  def apply() = new TimePoint(System.currentTimeMillis())
}


// ---------------------------------------------------------------- Intervals
case class TimeInterval[+T](timepoint1 : T, timepoint2 : T) extends ATOM // new terminology, but not yet implemented 04/02/2014 !

// Ways to specify time for time interval and time point
abstract class Time;
case class TStandardGMTDay (hours : Int, minutes : Int, seconds : Int) extends Time;


// ---------------------------------------------------------------- Durations

private object TimeStartPoint extends SS_Var[Integer, String]("TimeStartPoint")

class TimeDuration[+E] (startPoint: SS_Var[Integer, E], scalarValue: Integer) extends TimeInterval(startPoint, SS_Add(startPoint, SS_C(scalarValue)))
object TimeDuration 
{
  def unapply[E](obj: TimeDuration[E]): Option[(SS_Var[Integer,E], SS_Add[Integer])] = Some((obj.timepoint1.asInstanceOf[SS_Var[Integer, E]], obj.timepoint2.asInstanceOf[SS_Add[Integer]]))

  def apply(scalarValue: Integer): TimeDuration[String] = new TimeDuration(TimeStartPoint, scalarValue)
  
  // IDEA: I think the ERTP should be retained in the object graph.
  //       This would require a train TimeDurationLike that abstracts both TimeDuration and TERTP
  def apply[E](ertp: TERTP[E,Integer]): TimeDuration[E] = new TimeDuration(SS_Var[Integer,E](ertp.event), ertp.offset)
}

case class TimeDurationRange[+E] (minDuration: TimeDuration[E], maxDuration: TimeDuration[E]) extends ATOM


// --------------------------------------------------------------- Natural Time

case class TotalTimeOrdered[S](symbol: S)(implicit ev: S => Ordering[S]) extends Ordered[TotalTimeOrdered[S]]
{
  override def compare(that: TotalTimeOrdered[S]): Int = this.symbol.compare(this.symbol, that.symbol)
}


// --------------------------------------------------------------- Time2

trait Time2[+ConcreteTime] extends PartiallyOrdered[ConcreteTime]


// --------------------------------------------------------------- Event Relative Time

/*
 * Time Points may or may not be chosen.
 * Event relative time points are used to describe space/time behavior relative to an event
 */
abstract class ERTP extends Time2[ERTP]; // Event Relative Time Point

class TERTP[+E, +T](val event: E, val offset: T)(implicit ev: T => Ordering[T]) extends ERTP
{
  override def tryCompareTo [B >: ERTP](that: B)(implicit ev1: B => PartiallyOrdered[B]): Option[Int] =
  {
    that match {
      case tertp: TERTP[E, T] =>
        if (this.event != tertp.event)
        {
          None
        }
        else {
          Some(tertp.offset.compare(tertp.offset, this.offset))
        }
      case _ => None
    }
  }
}

//object TERTP {
//  def apply[E, T]( event: E,  offset: T)(implicit ev: T => Ordering[T]): TERTP[E, T] = { new TERTP[E, T](event, offset) }
//}

case class IntERTP[E](override val event: E,  intOffset : Int) extends TERTP[E, Integer](event, new Integer(intOffset))(ev = { i:Integer => Ordering[Integer] })
//object IntERTP { def apply[E](event: E, offset : Int): IntERTP[E] = new IntERTP(event, offset) }


// -------------------------------------------------------------------------------------------------------------- Geographic constructs

/*
 * Earth Geo Coordinates
 * added Jul 2015
 */

case class LatLong (latdegs : Int, latmins : Int, latsecs : Int, longdegs: Int, longmins : Int, longsecs : Int) // Latitude, Longitude coordinates



// -------------------------------------------------------------------------------------------------------------- Boolean constructs

// TODO: Change TRUE and FALSE classes to singleton objects and remove () from all references.
abstract class BOOL extends ATOM
{
  def not: BOOL = if(this == TRUE()) FALSE() else TRUE()
  def ! = not _
}
case class TRUE() extends BOOL
case class FALSE() extends BOOL



// -------------------------------------------------------------------------------------------------------------- Predicate constructs

case class NOT (t : Invariant) extends Invariant
case class IMPLIES[+P <: Invariant, +C <: Invariant] (premise : P, conclusion : C)  extends Invariant

// Disjunction
case class OR[+E <: Invariant] (t1 : E, t2 : E) extends Invariant

case class BIGOR[+E <: Invariant]  (terms : List[E]) extends Invariant
object BIGOR {
  def apply[E <: Invariant](terms: E*) = { new BIGOR[E](terms.toList) }
}

case class XOR[+E <: Invariant]  (terms : List[E]) extends Invariant
{
  def map[R <: Invariant](f: (E) => R): XOR[R] = { XOR(terms map f) }
}
object XOR {
  def apply[E <: Invariant](terms: E*) = { new XOR[E](terms.toList) }
}

// Conjunction
case class AND[+E <: Invariant,+F <: Invariant] (t1 : E, t2 : F)  extends Invariant

class BIGAND[+E <: Invariant] (val terms : List[E]) extends Invariant
{
    override def toString = s"BIGAND($terms)"
    override def equals(that: Any): Boolean =
    {
      if (! that.isInstanceOf[BIGAND[E]]) false
      else this.terms == that.asInstanceOf[BIGAND[E]].terms
    }
}
object BIGAND {
  def apply[E <: Invariant](terms : List[E]): BIGAND[E]        = { new BIGAND[E](terms) }
  def apply[E <: Invariant](terms: E*): BIGAND[E]              = { new BIGAND[E](terms.toList) }
  def unapply[E <: Invariant](obj: BIGAND[E]): Option[List[E]] = { Some(obj.terms.toList) }
}



// -------------------------------------------------------------------------------------------------------------- Dynamic constructs

case class Status[+O, +V] (obj: O, value: V) extends ATOM

trait EventLike  [+E] extends ATOM { val event: E }
case class Event [+E] (override val event  : E) extends EventLike[E]
case class Transition[+N,+E] (source : N, even : E, target : N) extends ATOM //for describing state transitions

//class INSTATE[+O, +T, +S] (val owner : O, val timepoint : TimePoint[T], val state :S) extends Event[(O,TimePoint[T],S)]((owner, timepoint, state))
//object INSTATE
//{ 
//  def apply[O, T, V] (owner : O, timepoint : TimePoint[T], value :V): INSTATE[O, T, V] = new INSTATE(owner, timepoint, value)
//  def unapply[O, T, V] (instate: INSTATE[O, T, V]): Option[(O, TimePoint[T], V)] = Some(instate.owner, instate.timepoint, instate.state)
//}

class INSTATE[+J <: Invariant, +T, +S <: Invariant] (val subject : J, val timepoint : TimePoint[T], val state :S) extends IMPLIES[AND[J,TimePoint[T]],S](AND(subject, timepoint), state) with EventLike[INSTATE[J,T,S]]
{
  override val event = this
}
object INSTATE
{ 
  def apply[J <: Invariant, T, S <: Invariant] (subject : J, timepoint : TimePoint[T], state :S): INSTATE[J, T, S] = new INSTATE(subject, timepoint, state)
  def unapply[J <: Invariant, T, S <: Invariant] (instate: INSTATE[J, T, S]): Option[(J, TimePoint[T], S)] = Some(instate.subject, instate.timepoint, instate.state)
}

case class Value[+V](value : V) extends ATOM { override def toString = s"Value($value)" }



// -------------------------------------------------------------------------------------------------------------- Structural constructs

// Components
case class Component[+I] (id : I) extends ATOM
class ComponentValue[+V] (val value : V) extends ATOM
object ComponentValue {
  def apply[V] (value : V)  = new ComponentValue(value)
  def unapply[V] (obj: ComponentValue[V]): Option[V] = Some(obj.value)
}

// Dynamic Value
case class ComponentState[+S] (state : S) extends ComponentValue[S](state)

// BeMaps

class BeMap[+P <: Invariant, +C <: Invariant](terms: List[IMPLIES[P,C]]) extends BIGAND[IMPLIES[P,C]](terms)
{
	lazy val premises    = ( this.terms map { _.premise } ).toSet[Any]
	lazy val conclusions = ( this.terms map { _.conclusion } ).toSet[Any]
	lazy val elements    = premises.toSet[Any] union conclusions.toSet[Any]

	def apply() = new BeMap(Nil)
	def apply(key: Invariant): Option[C] =
	{
		val optionalImplication: Option[IMPLIES[P,C]] = this.terms find { i: IMPLIES[P,C] => i.premise == key }

		val result = optionalImplication match
		{
			case None                 => None
			case Some(implication: IMPLIES[P,C]) => Some(implication.conclusion)
		}

		result
	}
}

object BeMap  // Companion Object
{
	//def apply[T <: Invariant](): BeMap[T]                                  = BeMap[T](Nil)
	//def apply[T <: Invariant](terms: Set[IMPLIES[Invariant,T]]):  BeMap[T] = BeMap(terms.toList)
	def apply[P <: Invariant, C <: Invariant](terms: List[IMPLIES[P,C]] = Nil): BeMap[P,C] = new BeMap(terms)
	def apply[P <: Invariant, C <: Invariant](bigand: BIGAND[IMPLIES[P,C]]): BeMap[P,C] = new BeMap(bigand.terms)
	def apply[P <: Invariant, C <: Invariant](and: AND[IMPLIES[P,C],IMPLIES[P,C]]): BeMap[P,C] = new BeMap(and.t1 :: and.t2 :: Nil)
	//def apply[P <: Invariant, C <: Invariant](and: AND): BeMap[P,C] = new BeMap(List(and.t1, and.t2))
}


// Meta-data of a component (static)
//case class ComponentDescription[+C <: Component[Any], +V <: ComponentValue[Any]] (override val terms: List[IMPLIES[C,V]])
//extends BIGAND[IMPLIES[C,V]](terms)
//object ComponentDescription {
//  def apply[C <: Component[Any], V <: ComponentValue[Any]](sequence: IMPLIES[C,V]*) = new ComponentDescription(sequence.toList) 
//  def apply[C <: Component[Any], V <: ComponentValue[Any]](entries : BIGAND[IMPLIES[C,V]]) = new ComponentDescription(entries.terms) 
//}

case class ComponentDescription[+C <: Component[Any], +V <: ComponentValue[Any]] (override val terms: List[IMPLIES[C,V]])
extends BeMap[C,V](terms)
object ComponentDescription {
  def apply[C <: Component[Any], V <: ComponentValue[Any]](sequence: IMPLIES[C,V]*) = new ComponentDescription(sequence.toList) 
  def apply[C <: Component[Any], V <: ComponentValue[Any]](entries : BIGAND[IMPLIES[C,V]]) = new ComponentDescription(entries.terms) 
}




// -------------------------------------------------------------------------------------------------------------- Spatial and ownership constructs

case class Owner[+O] (owner : O) extends ATOM { override def toString = s"Owner2(${owner.toString})"}

case class OccupyBox (x1 : Int,y1 : Int,x2 : Int,y2 : Int) extends ATOM
case class OccupyBoxDouble (x1 : Double,y1 : Double,x2 : Double,y2 : Double) extends ATOM
case class OccupyBoxSI (x1 : SI,y1 : SI,x2 : SI,y2 : SI) extends ATOM // symbolic coordinates, alternative concept to EROccupyBox
case class EROccupyBox (x1 : (ERTP => Int),y1 : (ERTP => Int),x2 : (ERTP => Int),y2 : (ERTP => Int)) extends ATOM // Coordinates depending on an event relative time point
case class OwnBox[+C] (owningcomponent : C,x1 : Int,y1 : Int,x2 : Int,y2 : Int) extends ATOM // a box that is owned by a component. Both OccupyBox (for collision) and OwnBox (for identification in communitcation) are needed.

// infinitely large boxes
case class OccupyInfiniteBox () extends ATOM 
case class OwnInfiniteBox[+C] (owningcomponent : C) extends ATOM


case class OccupyFreeBox (x1 : Int,y1 : Int,x2 : Int,y2 : Int, x3 : Int,y3 : Int,x4 : Int,y4 : Int) extends ATOM
case class Occupy3DBox (x1 : Int, y1: Int, z1 : Int, x2 : Int, y2 : Int, z2 : Int) extends ATOM
case class Occupy3DBoxDouble (x1 : Double, y1: Double, z1 : Double, x2 : Double, y2 : Double, z2 : Double) extends ATOM
case class CommRadius (x :Int, y :Int, r : Int) extends ATOM

// Points
trait Point extends ATOM with Jsonable

trait Point2D extends Point { val x,y: AnyVal; def toJson = s""""\"${x} ${y}\"""" }
case class OccupyPoint (x:Int, y:Int) extends Point2D
case class OccupyPointFloat(x: Float, y: Float) extends Point2D
case class OccupyPointDouble (x:Double, y:Double) extends Point2D
case class OwnPoint[+C] (owningcomponent : C,x:Int, y:Int) extends Point2D

trait Point3D extends Point { val x,y,z: AnyVal; def toJson = s"""\"${x} ${y} ${z}\""""  } 
case class Occupy3DPoint (x:Int, y:Int, z: Int) extends Point3D  
case class Occupy3DPointFloat(x: Float, y: Float, z: Float) extends Point3D
case class Occupy3DPointDouble (x:Double, y:Double, z: Double) extends Point3D

case class Test()

case class OccupySegment (x1 : Int, y1 : Int, x2 : Int, y2 :Int, radius : Int) extends ATOM
case class OccupySegment3D (x1 : Int, y1 : Int, z1 : Int, x2 : Int, y2 :Int, z2 : Int, radius : Int) extends ATOM
case class OwnSegment[+C] (owningcomponent : C, x1 : Int, y1 : Int, x2 : Int, y2 :Int, radius : Int) extends ATOM

case class OccupyCircle (x1 : Int, y1 : Int, radius : Int) extends ATOM // a 2D circle
case class OwnCircle[+C] (owningcomponent : C, x1 : Int, y1 : Int, radius : Int) extends ATOM



// -------------------------------------------------------------------------------------------------------------- Topological constructs

case class OccupyNode[+N]   (node : N) extends ATOM
case class OwnNode[+C,+N]   (owningcomponent : C, node : N) extends ATOM
case class BetweenNodes[+N] (source : N, target : N) extends ATOM // for describing that a component is in transition between two nodes

class EdgeAnnotated[+N, +A] (val source : N, val target : N, val annotation: Option[A]) extends ATOM
object EdgeAnnotated 
{ 
  def apply[N, A](source : N, target : N, annotation: Option[A]) =  new EdgeAnnotated(source, target, annotation) 
  def apply[N, A](source : N, target : N, annotation: A) =  new EdgeAnnotated(source, target, Some(annotation)) 
  def apply[N](source : N, target : N) =  new EdgeAnnotated(source, target, None)
  def unapply[N, A](ea: EdgeAnnotated[N, A]): Option[(N, N, Option[A])] = Some((ea.source, ea.target, ea.annotation))
}

case class Edge[+N] (override val source : N, override val target : N) extends EdgeAnnotated[N,Nothing](source, target, None) //for describing graph topological conditions conditions like: IF Edge THEN ...


// ------------ Behavioral Graphs

// Annotation

trait Annotation { val label: String }
trait Annotation2 extends Annotation { val labelSource: String; val labelTarget: String; val label = labelSource + ":" + labelTarget }
trait Annotation3 extends Annotation { val labelSource: String; val labelTarget: String; val labelEdge: String; val label = labelSource + "/" + labelEdge + "/" + labelTarget }

// Annotated Version
// Note: Use of the BeSPaceD Annotation abstraction is optional.
class BeGraphAnnotated[+N, +A](terms: List[EdgeAnnotated[N, A]]) extends BIGAND[EdgeAnnotated[N, A]](terms)
{
	lazy val sources: List[N] = (terms map { _.source }) 
	lazy val targets: List[N] = (terms map { _.target })
	lazy val nodes:   List[N] = sources union targets

	/**
   * Finds all the destination nodes connected to a given source node.
   * 
   * @typeParam N is the type of the nodes.
   * 
   * @param source is the source node.
   * @returns a list of the destination nodes.
	 */
	def apply(source: Any): List[N] =
	{
		val optionalEdge: List[EdgeAnnotated[N, A]] = this.terms filter { _.source == source }

		val result = optionalEdge match
		{
			case Nil  => Nil
			case list => list map { _.target }
		}

		result
	}
	
}

object BeGraphAnnotated  // Companion Object
{
  val core = standardDefinitions; import core._
  
	//def apply[T <: Invariant](): BeMap[T]                                  = BeMap[T](Nil)
	//def apply[T <: Invariant](terms: Set[IMPLIES[Invariant,T]]):  BeMap[T] = BeMap(terms.toList)
	def apply[N,A](terms: List[EdgeAnnotated[N,A]] = Nil) = new BeGraphAnnotated(terms)
	def apply[N,A](bigand: BIGAND[EdgeAnnotated[N,A]]) = new BeGraphAnnotated(bigand.terms)
	//def apply[N](and: AND): BeGraph[N] = new BeGraph(List(and.t1, and.t2))
}


// Un-annotated version
class BeGraph[+N](terms: List[Edge[N]]) extends BeGraphAnnotated[N, Nothing](terms)

object BeGraph  // Companion Object
{    
	def apply[N,A](terms: List[Edge[N]] = Nil) = new BeGraph(terms)
	def apply[N,A](bigand: BIGAND[Edge[N]]) = new BeGraph(bigand.terms)
}

	
	
// -------------------------------------------------------------------------------------------------------------- Temporal constructs

case class TemporalCorrelation[+E, +T <: Number] (cause: EventLike[E], duration: TimeDuration[E],      effect: EventLike[E]) extends Invariant

/**
 * Temporal Constraints declare that two events (or one event and the lack of a second event in the inverse case) must occur
 * within a specified time duration relative to the first event.
 * The first event is called the cause and the second is called the effect.
 * 
 * @param cause any event
 * @param duraton a period of time relative to the time point of the cause in which the effect (or inverse) should hold.
 * @param effect any other event
 * @param inverse A value of true means the inverse of the effect should be true (i.e. the event should not occur in the time frame defined).
 */
case class TemporalConstraint [+E, +T <: Number] (cause: EventLike[E], range:    TimeDurationRange[E], effect: EventLike[E], inverse: Boolean = false) extends Invariant



// -------------------------------------------------------------------------------------------------------------- Probabilistic constructs

// These should appear in clauses : Event /\ Owner /\ Prob ... -> Space
case class Prob (probability : Double) extends ATOM //probability 

case class PROBIMPLIES (probability: Double, t1 : Invariant, t2 : Invariant)  extends Invariant; // t1 implies t2 with a probability







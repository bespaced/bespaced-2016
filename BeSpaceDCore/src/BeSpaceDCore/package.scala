/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import java.io._

import scala.io._
import scala.compat.Platform.EOL

import org.scalatest._

import scala.util.Either



/**
 * @author Keith Foster
 */

import java.util.Date



package object BeSpaceDCore {


  //-------------------------------- Standard Package includes

  import HazyTypes._



  //-------------------------------- Global Definitions
  private
  val CoreDefinitions = new CoreDefinitions()

  private
  val GraphOperations = new GraphOperations()



  //-------------------------------- Inversion of Control

  //TODO: Add some factory functions for core definitions here
  //      A factory function should be passed as a parameter to Apps and test suites
  //      so they can instantiate a core definition object to use.

  def standardDefinitions = CoreDefinitions
  val core  = standardDefinitions; import core._
  
  
  
  
  //-------------------------------- Pretty Printing
  
  abstract trait PrettyPrintable
  {
      def pp: String
  }
  
  
  
  //-------------------------------- JSON Serialisation
  
    abstract trait Jsonable
    {
      def toJson: String
    }
  
    def toJson(any: Any): String =
    {
      if(any.isInstanceOf[Jsonable])
      {
        any.asInstanceOf[Jsonable].toJson
      }
      else if (any.isInstanceOf[Tuple2[Any, Any]])
      {
        val first = any.asInstanceOf[Tuple2[Any,Any]]._1
        val second = any.asInstanceOf[Tuple2[Any,Any]]._2
        
        toJson(first) + EOL + toJson(second)
      }
      else if (any.isInstanceOf[Invariant])
      {
        invariantToJson(any.asInstanceOf[Invariant])
      }
      else if(any.isInstanceOf[Date])
      {
        s"""${any.asInstanceOf[Date].getTime()}"""
      }
      else if(any.isInstanceOf[Either[Any, Any]])
      {
        s"""${val e = any.asInstanceOf[Either[Any,Any]]; toJson(e.merge)}"""
      }
      
      // UNWNOWN TYPES
      else
      {
        s""""${any.toString}""""
      }
    }
  
    def edgeToJson[N, A](edge: EdgeAnnotated[N, A]): String =
		{
      val annotate: Boolean        = edge.annotation.isDefined
      val annotationString: String = if (annotate) 
      {
        val annotation: A = edge.annotation.get
        s""",
                        "annotation" : ${toJson(annotation)}
        """ 
      }
      else
        ""
        
		  return s"""
		      {
		              "type" : "EdgeAnnotated",
		            "source" : ${toJson(edge.source)},
		            "target" : ${toJson(edge.target)}${annotationString}
		      }
		    """
		}

    def graphToJson[N, A](graph: BeGraphAnnotated[N, A]): String =
		{
		  import scala.compat.Platform.EOL
		  
		  val edges: List[String] = (graph.terms map {e: EdgeAnnotated[N,A] => edgeToJson(e)})
		  
		  return s"""
		    {
		      "type" : "BeGraphAnnotated"
		      "edges" : [
		        ${edges mkString s"$EOL      "}
		]
		    }
		    """
		}

  
  
  // ------------------------------- Implicits
  
  // Time Orderings
  implicit val intTimeOrdering = { _: Int => Ordering[Int] } 
  //implicit val stringOrdering = { _: String => Ordering[String] } 

  
  
  // ------------------------------- Implicit Operators

  implicit class InvariantOps[L <: N, N >: L <: Invariant](private val left: L) extends AnyVal
  {
    // IMPLIES Construction
    @inline def implies[R <: Invariant] (right : R) = IMPLIES[L,R](left, right)
    @inline def ==>[R <: Invariant] (right : R) = implies(right)
    @inline def →[R <: Invariant] (right : R) = implies(right)
    
    // AND
    @inline def and[R <: N] (right : R) = BIGAND[N](left, right)
    @inline def ^[R <: N] (right : R) = and(right)

    // OR
    @inline def or[R <: Invariant] (right : R) = OR(left, right)
    @inline def v[R <: Invariant] (right : R) = or(right)
    
    // Exclusive OR
    @inline def xor[R <: Invariant] (right : R) = XOR(left, right)
    //@inline def +[R <: Invariant] (right : R) = xor(right)
    
    // Edges
    @inline def edge[R <: N] (right : R) = Edge[N](left, right)
    @inline def -->[R <: N] (right : R) = edge(right)
        

    // MORE TODO...
  }

  implicit class AndOps[L <: AND[Invariant,Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(left.t1, left.t2, right)).asInstanceOf[BIGAND[R]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left, right)).asInstanceOf[BIGOR[Invariant]]
  }

  implicit class BeGraphOps[N, A, E <: EdgeAnnotated[N,A], LN <: N, L <: BeGraph[LN]](private val left: L) extends AnyVal
  {
    // JOIN
    @inline def ++[RN <: N, R <: BeGraphAnnotated[RN,A]] (right : R) = BeGraphAnnotated[N,A](standardDefinitions.flattenAndList(left.terms ::: right.terms).asInstanceOf[List[E]])
  }


  implicit class BigandOps[L <: BIGAND[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : BIGAND[R]) = standardDefinitions.flatten(BIGAND(left.terms ::: right.terms)).asInstanceOf[BIGAND[R]]
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(left.terms ::: List(right))).asInstanceOf[BIGAND[R]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left, right)).asInstanceOf[BIGOR[Invariant]]
  }

  implicit class OrOps[L <: OR[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(AND(left, right)).asInstanceOf[AND[Invariant,Invariant]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left.t1, left.t2, right)).asInstanceOf[BIGOR[R]]
  }

  implicit class BigorOps[L <: BIGOR[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(left, right)).asInstanceOf[BIGAND[Invariant]]

    // OR
    @inline def v[R <: Invariant] (right : BIGOR[R]) = standardDefinitions.flatten(BIGOR(left.terms ::: right.terms)).asInstanceOf[BIGOR[R]]
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left.terms ::: List(right))).asInstanceOf[BIGOR[R]]
  }

  
  implicit class XorOps[L <: XOR[Invariant]](private val left: L) extends AnyVal
  {
    // AND
    @inline def ^[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGAND(left, right)).asInstanceOf[BIGAND[Invariant]]

    // OR
    @inline def v[R <: Invariant] (right : R) = standardDefinitions.flatten(BIGOR(left, right)).asInstanceOf[BIGOR[Invariant]]
    
    // XOR
    @inline def +[R <: Invariant] (right : R) = standardDefinitions.flatten(XOR(left.terms ::: List(right))).asInstanceOf[XOR[R]]
  }

  
  // ------------------------------- Obsolete

  // Flatten and Unwrap the terms of conjunctions

  @Deprecated
  def flattenAnds(presumedAnd: Invariant): Invariant = CoreDefinitions.simplifyInvariant(BIGAND(unwrapAnds(presumedAnd)))

  @Deprecated
  def unwrapAnds(presumedAnd: Invariant): List[Invariant] =
  {
    presumedAnd match
    {
      case AND(t1, t2)                   => unwrapAnds(t1) ++ unwrapAnds(t2)
      case BIGAND(list: List[Invariant]) => list flatMap unwrapAnds
      case _                             => List(presumedAnd)
    }
  }



  //-------------------------------- Scala Language Improvements

  // Union Types
  type or[L, R] = Either[L, R] // Move this to a common module
  implicit def l2Or[L, R](l: L): L or R = Left(l)
  implicit def r2Or[L, R](r: R): L or R = Right(r)

  // Poor Man's DbC
  /** Tests an expression, throwing an `IllegalArgumentException` if false.
   *  This method is similar to `assert`, but blames the caller of the method
   *  for violating the condition.
   *
   *  @param postCondition   the expression to test
   */
  def ensure(postCondition: Boolean) = require(postCondition)

  /** Tests an expression, throwing an `IllegalArgumentException` if false.
   *  This method is similar to `assert`, but blames the caller of the method
   *  for violating the post condition.
   *
   *  @param postCondition   the expression to test
   *  @param message         a String to include in the failure message
   */
  @inline final def ensure(postCondition: Boolean, message: => Any) {
    if (!postCondition)
      throw new IllegalArgumentException("ensure failed: "+ message)
  }

  /** Tests an expression, throwing an `IllegalArgumentException` if false.
   *  This method is similar to `assert`, but blames the caller of the method
   *  for violating the condition.
   *
   *  @param condition   the expression to test
   */
  def check(condition: Boolean) = require(condition)

  /** Tests an expression, throwing an `IllegalArgumentException` if false.
   *  This method is similar to `assert`, but blames the caller of the method
   *  for violating the post condition.
   *
   *  @param condition   the expression to test
   *  @param message     a String to include in the failure message
   */
  @inline final def check(condition: Boolean, message: => Any) {
    if (!condition)
      throw new IllegalArgumentException("check failed: "+ message)
  }
  
  // ------------------------------- Serialization
  
  def write(data: String, path: String) = new PrintWriter(path) { write(data); close }
  def read(path: String): String        = Source.fromFile(path).mkString

  def serialize[D](data: D, path: String) =
  {
    val fileOS = new FileOutputStream(s"$path")
    val objectOS = new ObjectOutputStream(fileOS)
    
    objectOS.writeObject(data)
    objectOS.close()
  }

  def deserialize[D](path: String): Option[D] =
  {
   try
   {
    val fileIS = new FileInputStream(s"$path")
    val objectIS = new ObjectInputStream(fileIS)
    
    Some(objectIS.readObject().asInstanceOf[D]) 
   }
   catch
   {
     case e: FileNotFoundException => None
   }
  }


  //-------------------------------- ScalaTest
  
  // --------- Base classes
  
  // Unit Testing
  abstract class UnitSpec extends FlatSpec with Matchers with OptionValues with Inside with Inspectors
  
  // --------- Helpers
  
  def timed[R](f: () => R): R =
  {
    val start = System.currentTimeMillis()
    
    val result = f()
    
    val finish = System.currentTimeMillis()
    
    val duration = finish - start
    
    println (s"DURATION: $duration")
    
    return result
  }

}
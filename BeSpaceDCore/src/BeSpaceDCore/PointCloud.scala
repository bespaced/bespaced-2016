package BeSpaceDCore

import java.io.InputStream
import java.io.DataInputStream
import java.io.DataOutputStream

import scala.collection.mutable.HashSet



/**
 * This module is for point cloud data structures that have a focus on efficiency and serialization.
 */
object PointCloud {  
  
  trait PointCloud[+E <: Point3D] extends Invariant
  {
    def contains(p: Point3D): Boolean
    
    def toList: List[E]
  }
  
  trait PointCloudSource[E <: Point3D]
  {
    def inputStream:  InputStream
    def iterator: Iterator[E]
    
    def toPointCloud(): PointCloud[E]
  }
  
    
  trait PointCloudTarget[E <: Point3D]
  {
    def add(p: E)
    def outputStream: DataOutputStream
    def clear()

    def finish(): PointCloud[E]
  }
  
//  // Make a PointCloud as a Source
//  
//  def Source[E](pc: PointCloud[E]): PointCloudSource[E] =
//  {
//    PipedPointCloudSource(pc)
//  }
//  
//  case class PipedPointCloudSource[E <: Point3D](pc: PointCloud[E]) extends PointCloudSource[E]
//  {
//    def inputStream = ???  // TODO: Keith - Use the iterator
//    
//    def iterator: Iterator[Occupy3DPointFloat] = hashSet.iterator
//    
//    def toPointCloud = HashPointCloud(hashSet)
//        
//    lazy val toList = hashSet.toArray.toList
//  }
  
  // --------------------------------------------------------------------------------- In Memory PointClouds
  
  case class HashPointCloudTarget() extends PointCloudTarget[Occupy3DPointFloat]
  {
    val hashSet = HashSet[Occupy3DPointFloat]()
    
    override def clear() = hashSet.clear()
    override def add(p: Occupy3DPointFloat): Unit = hashSet.add(p.asInstanceOf[Occupy3DPointFloat])
    
    override def outputStream: DataOutputStream = ??? // TODO: Keith

    override def finish() = HashPointCloud(hashSet)
  }
  
  case class HashPointCloudSource(hashSet: HashSet[Occupy3DPointFloat]) extends PointCloudSource[Occupy3DPointFloat]
  {
    def inputStream = ??? // TODO: Keith
    
    def iterator: Iterator[Occupy3DPointFloat] = hashSet.iterator
    
    def toPointCloud = HashPointCloud(hashSet)
        
    lazy val toList = hashSet.toArray.toList
  }
  
  case class HashPointCloud(hashSet: HashSet[Occupy3DPointFloat]) extends PointCloud[Occupy3DPointFloat]
  {
    override def contains(p: Point3D): Boolean = if (p.isInstanceOf[Occupy3DPointFloat]) hashSet.contains(p.asInstanceOf[Occupy3DPointFloat]) else false
    
    lazy val toList = hashSet.toArray.toList
  }
  
}
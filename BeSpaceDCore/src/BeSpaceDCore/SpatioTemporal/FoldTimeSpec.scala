package BeSpaceDCore.SpatioTemporal

/**
 * Created by keith on 11/01/16.
 */

import BeSpaceDCore._
import BeSpaceDCore.Log._


class FoldTimeSpec extends UnitSpec {

  val core = standardDefinitions; import core._

  // Time
  val t1 = new IntegerTime(1)
  val t2 = new IntegerTime(2)
  val t3 = new IntegerTime(3)

  def calculateArea(box: OccupyBox): Int = Math.abs((box.x2 - box.x1 + 1) * (box.y2 - box.y1 + 1))

  "Spatio-Temporal Operator: foldTime" should "be able to fold time with a simple example" in {

    // Overlapping Boxes
    val b1 = OccupyBox(1, 1, 10, 10) // Area 100
    val b2 = OccupyBox(5, 5, 15, 15) // Area 121
    val b3 = OccupyBox(10, 10, 20, 20) // Area 121

    // Time occupied
    val to1 = IMPLIES(TimePoint(t1), b1)
    val to2 = IMPLIES(TimePoint(t2), b2)
    val to3 = IMPLIES(TimePoint(t3), b3)

    val timeSeries = List(to1, to2, to3)
    val conjunction = BIGAND(timeSeries)
    val initialValue = 0

    def addAreaOccupied(total: Int, item: Invariant): Int = {
      val area = item match {
        case IMPLIES(imp, box: OccupyBox) => calculateArea(box)
        case _ => 0
      }

      val result = total + area

      testOff {
        println(s"time addAreaOccupied()...")
        println(s"    previous total = $total")
        println(s"              item = $item")
        println(s"              area = $area")
        println(s"   resulting total = $result")
      }

      result
    }

    val foldedTime = foldTime[Int](
      conjunction,
      initialValue,
      t1, t3,
      1,
      addAreaOccupied
    )

    assertResult(expected = 342)(actual = foldedTime)
  }



  it should "be able to fold time with a simple example (new filter algorithm foldTime2)" in {
    // Overlapping Boxes
    val b1 = OccupyBox(1, 1, 10, 10) // Area 100
    val b2 = OccupyBox(5, 5, 15, 15) // Area 121
    val b3 = OccupyBox(10, 10, 20, 20) // Area 121

    // Time occupied
    val to1 = IMPLIES(TimePoint(t1), b1)
    val to2 = IMPLIES(TimePoint(t2), b2)
    val to3 = IMPLIES(TimePoint(t3), b3)

    val timeSeries = List(to1, to2, to3)
    val conjunction = BIGAND(timeSeries)
    val initialValue = 0

    def func(total: Int, item: Invariant): Int = {
      val area = item match {
        case IMPLIES(TimePoint(TotalTimeOrdered(_: Int)), _: OccupyPoint) => 1
        case IMPLIES(TimePoint(TotalTimeOrdered(_: Int)), box: OccupyBox) => calculateArea(box)
        case _ => 0
      }

      val result = total + area

      testOn {
        println(s"time func()...")
        println(s"    previous total = $total")
        println(s"              item = $item")
        println(s"              area = $area")
        println(s"   resulting total = $result")
      }

      result
    }

    val foldedTime = foldTime2[Int](
      conjunction,
      initialValue,
      t1, t3,
      1,
      func
    )

    assertResult(expected = 342)(actual = foldedTime)
  }



  it should "be able to fold time and count time periods where spacial points are occupied in more than one time point." in {
    // Overlapping Boxes
    val b1 = OccupyBox(1, 1, 10, 10) // Area 81
    val b2 = OccupyBox(5, 5, 15, 15) // Area 100
    val b3 = OccupyBox(10, 10, 20, 20) // Area 100

    // Time occupied
    val to1 = IMPLIES(TimePoint(t1), b1)
    val to2 = IMPLIES(TimePoint(t2), b2)
    val to3 = IMPLIES(TimePoint(t3), b1) // Go back to the first box at time 3

    val timeSeries = List(to1, to2, to3)
    val conjunction = BIGAND(timeSeries)
    val initialValue = BeMap[OccupyPoint, Counter]()

    // Adding counters (aggregation function)
    type Counter = ComponentState[Int]
    type OccupationCounts = BeMap[OccupyPoint, Counter]

    def add(countA: Counter, countB: Counter): Counter = ComponentState(countA.state + countB.state)

    def periodOccupied(item: Invariant): OccupationCounts =
    {
      debugOff(s"item = $item")

      val flatItem = flattenAnds(item)

      debugOff(s"flatItem = $flatItem")

      val pointCount: OccupationCounts =
        flatItem match
        {
          case IMPLIES(TimePoint(TotalTimeOrdered(_: Int)), box: OccupyBox) => BeMap[OccupyPoint, Counter](unfoldBoxGeneric(box) map { op: OccupyPoint => IMPLIES(op, ComponentState(1)) })

          case IMPLIES(TimePoint(TotalTimeOrdered(_: Int)), point: OccupyPoint) => BeMap[OccupyPoint, Counter](List(IMPLIES(point, ComponentState(1))))

          case AND(t1, t2) => unionMaps(periodOccupied(t1), periodOccupied(t2), add)

          case BIGAND(sublist: List[Invariant]) => unionMapList(sublist map periodOccupied, add)

          case _ => BeMap[OccupyPoint, Counter]()
        }

      debugOn(s"pointCount = $pointCount")

      pointCount
    }

    def aggregatePeriodOccupied(accumulator: OccupationCounts, item: Invariant): OccupationCounts =
    {
      val occupied: OccupationCounts = periodOccupied(item)

      debugOn(s"occupied = $occupied")

      val pointCount = unionMaps(accumulator, occupied, add)

      pointCount
    }

    val result = foldTime2[OccupationCounts](
      conjunction,
      initialValue,
      startTime = t1, stopTime = t3,
      step = 1,
      f = aggregatePeriodOccupied
    )

    def pointsInNTimePeriods(foldedTime: OccupationCounts)(n: Int): List[Invariant] = foldedTime match
    {
      case bm: OccupationCounts => bm.terms filter { _.conclusion.state == n} map { _.premise }
      case _ => List()
    }

    val pointsInNTimePeriodsForResult = pointsInNTimePeriods(result) _

    val pointsInOneTimePeriod    = pointsInNTimePeriodsForResult(1).sorted
    val pointsInTwoTimePeriods   = pointsInNTimePeriodsForResult(2).sorted
    val pointsInThreeTimePeriods = pointsInNTimePeriodsForResult(3).sorted

    /**
     * Answer: OccupyBox(5,5,10,10) is the overlapping area that will be occupied in all three time points
     */

    // Expect These points in three time periods
    val pointsOverlappingInThreeAreas = unfoldBox(OccupyBox(5, 5, 10, 10))

    testOn
    {
      println(s"pointsOverlappingInThreeAreas = $pointsOverlappingInThreeAreas")
      println(s"pointsInThreeTimePeriods = $pointsInThreeTimePeriods")
    }

    assertResult(expected = pointsOverlappingInThreeAreas)(actual = pointsInThreeTimePeriods)
    
  }
}



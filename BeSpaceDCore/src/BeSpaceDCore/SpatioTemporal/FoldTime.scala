package BeSpaceDCore.SpatioTemporal

/**
 * Created by keith on 11/01/16.
 */

import BeSpaceDCore._
import BeSpaceDCore.Log._



object FoldTime {

  val core = standardDefinitions; import core._

  // ---------------------------------------------------------------- Folding Time

  def foldTime[A](
                   invariant: Invariant,
                   accumulator: A,
                   startTime: IntegerTime,
                   stopTime: IntegerTime,
                   step: IntegerTime.Step,
                   f: (A, Invariant) => A
                   ): A =
  {
    if (startTime <= stopTime)
    {
      val Never = TRUE()
      val nextStepTime: IntegerTime = startTime + step
      val filteredInvariant: Invariant = filterTime(startTime, nextStepTime, defaultReturn = Never)(invariant)
      val simplifiedInvariant: Invariant = simplifyInvariant(filteredInvariant)

      debugOff {
        println(s"invariants...")
        println(s"  invariant = $invariant")
        println(s"  filteredInvariant = $filteredInvariant")
        println(s"  simplifiedInvariant = $simplifiedInvariant")
      }

      val accumulation: A = f(accumulator, simplifiedInvariant)

      debugOff(s"f(): $accumulator -> $accumulation")

      debugOff {
        println(s"foldTime()...")
        println(s"  startArea = $nextStepTime")
        println(s"   stopArea = $stopTime")
        println(s"       step = $step")
      }

      val result =
        foldTime[A](
          invariant,
          accumulation,
          startTime = nextStepTime,
          stopTime,
          step,
          f
        )

      debugOff(s"     result =  $result")

      result
    }
    else
    {
      accumulator
    }
  }



  def foldTime2[A](
                   invariant: Invariant,
                   accumulator: A,
                   startTime: IntegerTime,
                   stopTime: IntegerTime,
                   step: Int,
                   f: (A, Invariant) => A
                   ): A =
  {
    if (startTime <= stopTime)
    {
      val nextStepTime: IntegerTime = startTime + step
      val filteredInvariant: Invariant = filterTime2(startTime, nextStepTime, successReturn = TRUE())(invariant)

      debugOn {
        println(s"invariants(2)...")
        println(s"          invariant = $invariant")
        println(s"          startTime = $startTime")
        println(s"       nextStepTime = $nextStepTime")
        println(s"  filteredInvariant = $filteredInvariant")
      }

      val accumulation: A = f(accumulator, filteredInvariant)

      debugOn(s"f(): $accumulator -> $accumulation")

      debugOn {
        println(s"foldTime2()...")
        println(s"  startArea = $nextStepTime")
        println(s"   stopArea = $stopTime")
        println(s"       step = $step")
      }

      val result =
        foldTime2[A](
          invariant,
          accumulation,
          startTime = nextStepTime,
          stopTime,
          step,
          f
        )

      debugOff(s"     result(2) =  $result")

      result
    }
    else
    {
      accumulator
    }
  }

}

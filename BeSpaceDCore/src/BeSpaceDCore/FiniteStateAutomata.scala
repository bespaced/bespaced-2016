/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDCore

import scala.compat.Platform.EOL  

import BeSpaceDCore.Log._


/**
 * This module defines well known finite state machines and ties the inputs to BeSpaceD events.
 * This makes the FSMs very useful as INSTATE and many domain specific refinements of INSTATE
 * (e.g. FestoEvents) become compatible FSM inputs.
 * 
 * We use Design By Contract (DbC) to enforce contracts that the states, inputs and outputs
 * must exist in their respective sets and alphabets.
 * 
 * So far we have only implemented transducers using the Mealy machine model.
 * 
 * Note: An "Action" transducer refines a generic transducer to use actions as outputs
 *       which causes the machine to execute the actions.
 */

object FiniteStateAutomata {
  
  trait Named
  {
    def name: String
  }
  trait State extends Named
  type  Input[I] = EventLike[I]
  trait Output extends Named
  trait Action extends Output
  {
    val execute: () => Unit
  }

  type InputAlphabet   [E <: EventLike[Any]] = Set[E]
  type OutputAlphabet      [O <: Output] = Set[O]
  type States                        [S] = Set[S]
  
  type MealyFunction     [S <: State, E <: EventLike[Any]] = (S, E) => Named
  type TransitionFunction[S <: State, E <: EventLike[Any]] = (S, E) => S
  
  object MealyMachine
  {
    type OutputFunction[S <: State, E <: EventLike[Any], O] = (S, E) => O
    
    abstract class Transducer[S <: State, E <: EventLike[Any], O <: Output] (
        inputAlphabet:      InputAlphabet[E], 
        outputAlphabet:     OutputAlphabet[O],
        states:             States[S],
        initialState:       S,
        transitionFunction: TransitionFunction[S, E],
        outputFunction:     OutputFunction[S, E, O]
        )
        {
          import Transducer._
          
          var currentState: S = initialState
          
          def initialize() = { currentState = initialState }
          
          def trigger(input: E): O =
          {
            require(inputAlphabet contains input,     s"Input symbol $input is not in the alphabet.")
            
            val nextState = transitionFunction(currentState, input)
            val output = outputFunction(currentState, input)
            
            ensure(states         contains nextState, s"State $nextState is not in the set of valid states.")
            ensure(outputAlphabet contains output,    s"Output symbol $output is not in the alphabet.")
            
            currentState = nextState
            return output
          }
          
          // TODO: Allow new FSMs to be saved and loaded dynamically.
          //       This will avoid having to deploy large JAR files which
          //       reduces debugging turn-around times during tight prototyping cycles.
          def save(filename: String, eventNameFunction: (E) => String) = write(this.symbolicFunctions(eventNameFunction), s"$FSMFolder/$filename")
          
          private
          val SECrossProduct = states zip inputAlphabet
          
          private
          def symbolicFunctions(eventNameFunction: (E) => String): String =
          {            
            def generateSymbolicMap(f: MealyFunction[S, E]): String =
            {
              val symbolicMap = SECrossProduct map {
                stateAndEvent: (S, E) => 
                  val s: S = stateAndEvent._1
                  val e: E = stateAndEvent._2
                  
                  val result = s"${s.name}, ${eventNameFunction(e)} => ${f(s, e).name}"
                  debugOff(result)
                  
                  result
              }
              
              symbolicMap.mkString(EOL)
            }
           
            val symbolicTransitionMap = generateSymbolicMap(transitionFunction)
            val symbolicOutputMap     = generateSymbolicMap(outputFunction)
            
            symbolicTransitionMap + EOL + DIVIDER + symbolicOutputMap + EOL
          }
        }
    
    object Transducer
    {      
      val DIVIDER = s"---$EOL"
    }
    
        
    case class ActionTransducer[S <: State, E <: EventLike[Any], A <: Action]
        (
        inputAlphabet:      InputAlphabet[E], 
        actionAlphabet:     OutputAlphabet[A],
        states:             States[S],
        initialState:       S,
        transitionFunction: TransitionFunction[S, E],
        outputFunction:     OutputFunction[S, E, A]
        )
        extends Transducer[S, E, A] (inputAlphabet, actionAlphabet, states, initialState, transitionFunction, outputFunction)
        {
          import Transducer._
          
          override def trigger(input: E): A =
          {
            require(inputAlphabet contains input,     s"Input symbol $input is not in the alphabet.")
            
            val nextState = transitionFunction(currentState, input)
            val action = outputFunction(currentState, input)
            
            ensure(states         contains nextState, s"State $nextState is not in the set of valid states.")
            ensure(actionAlphabet contains action,    s"Output action $action is not in the alphabet.")
            
            action.execute()
            currentState = nextState
            return action
          }
          
          // TODO: Allow new FSMs to be saved and loaded dynamically.
          //       This will avoid having to deploy large JAR files which
          //       reduces debugging turn-around times during tight prototyping cycles.
          def load[S <: State, E <: EventLike[Any], A <: Action](
            inputAlphabet:        InputAlphabet[E], 
            outputAlphabet:       OutputAlphabet[A],
            states:               States[S],
            initialState:         S,
            filename:             String,
            createStateFunction:  (String) => S,
            createActionFunction: (String) => A,
            eventNameFunction: (E) => String
            ): Option[Transducer[S, E, A]] = 
            {
              def convertLineToTransition(line: String): ((String,String),S) =
                {
                  val TRANSITION = """(\w+), (.+) => (\w+)""".r
                  val TRANSITION(currentStateName, eventName, nextStateName) = line
                  
                  val nextState : S = createStateFunction(nextStateName)
                        
                  ((currentStateName, eventName), nextState)
                }
  
              def convertLineToOutput(line: String): ((String,String),A) =
                {
                  val OUTPUT = """(\w+), (.+) => (\w+)""".r
                  val OUTPUT(currentStateName, eventName, actionName) = line
                  
                  val action: A = createActionFunction(actionName)
                      
                  ((currentStateName, eventName), action)
                }
                              
              val symbolicMachine = read(s"$FSMFolder/$filename")
              val twoMaps = symbolicMachine.split(DIVIDER)
              
              val symbolicTransitionMap       = twoMaps(0)
              val symbolicOutputMap           = twoMaps(1)
              val transitionMap: Map[(String,String),S] = (symbolicTransitionMap.split(EOL).toList map convertLineToTransition).toMap
              val outputMap: Map[(String,String),A]     = (symbolicOutputMap.split(EOL).toList map convertLineToOutput).toMap
              
              val transitionFunction = { (s: S, e: E) => transitionMap((s.name, eventNameFunction(e))) }
              val outputFunction     = { (s: S, e: E) => outputMap((s.name, eventNameFunction(e))) }
              
              val transducer = ActionTransducer(inputAlphabet, outputAlphabet, states, initialState, transitionFunction, outputFunction)
              
              Some(transducer)
            }

        }
  }
  
  val FSMFolder = "."
}
package BeSpaceDCore

/**
 * Created by keith on 11/01/16.
 */
class LanguageSpec extends UnitSpec {

  //--------------------------------------- Basic Definitions

  // Primitive Atoms
  "Language" should "be able to create the primitive data atoms" in
    {
      val True: ATOM = TRUE()
      val False: ATOM = FALSE()
//      val TrueSingleton: ATOM = TRUE
//      val FalseSingleton: ATOM = FALSE
//      
//      assertResult(TrueSingleton)(True)
//      assert(TrueSingleton ne True, "A constructed TRUE should be a different instance to the singleton TRUE")
//      assertResult(FalseSingleton)(False)
//      assert(FalseSingleton ne False, "A constructed FALSE should be a different instance to the singleton FALSE")

      val op: OccupyPoint  = OccupyPoint(1,1)
      val o3p: Occupy3DPoint  = Occupy3DPoint(1,1,1)

      val ob: OccupyBox  = OccupyBox(1,1,1,1)
      val o3b: OccupyBox  = OccupyBox(1,1,1,1)

      // TODO: The rest...
    }

  // Logical Invariants
  it should "be able to create basic logical invariants" in
    {
      val imp1 = IMPLIES(TRUE(), OccupyPoint(1,2))
      val imp2 = IMPLIES(TimePoint("now"), OccupyPoint(5,6))

      val conj = BIGAND(List(imp1, imp2))
      val disj = BIGOR(List(imp1, imp2))

      // TODO: The rest...

      val redundant = AND(TRUE(), TRUE())

    }


  // Implies operator
  "The -> operator" should "be able to implicitly construct an IMPLIES" in
    {
      val imp = OccupyPoint(9,9) ==> ComponentState("Done")

      println(imp)
      assert(imp.isInstanceOf[IMPLIES[OccupyPoint, ComponentState[String]]], "Expected type IMPLIES[OccupyPoint, ComponentState[String]]")
    }
  
  
  // Conjunction operators
  "The ^ operator" should "be able to implicitly construct a binary conjunction as an AND" in
    {
    // NOTE: A limitation of Scala lower type bounds is that the left most terms must be the same of more general types.
      val conj = OccupyPoint(1,1) ^ OccupyPoint(2,1)

      println(conj)
      assert(conj.isInstanceOf[AND[Invariant,Invariant]], "AND expected")
    }
  
  it should "be able to implicitly construct a ternery conjunction as an BIGAND" in
    {
    // NOTE: A limitation of Scala lower type bounds is that the left most terms must be the same of more general types.
    val mid_conj = OccupyPoint(1,1) ^ OccupyPoint(3,1) ^ FALSE()

    println(mid_conj)
    assert(mid_conj.isInstanceOf[BIGAND[Invariant]], "BIGAND expected")
    assertResult(3)(mid_conj.terms.length)
    }
  
  it should "be able to implicitly construct a quadernary conjunction as a BIGAND" in
    {
    // NOTE: A limitation of Scala lower type bounds is that the left most terms must be the same of more general types.
    val big_conj = OccupyPoint(1,1) ^ OccupyPoint(4,1) ^ FALSE() ^ TRUE()
      
    println(big_conj)
    assert(big_conj.isInstanceOf[BIGAND[Invariant]], "BIGAND expected")
    assertResult(4)(big_conj.terms.length)
    }
  
  it should "be able to implicitly construct a conjunction of two ANDs as a BIGAND of cardinality four" in
    {
    val conj_two_and = AND[Invariant,Invariant](TRUE(), OccupyPoint(7,7)) ^ AND(FALSE(), Component("Test"))
      
    println(conj_two_and)
    assert(conj_two_and.isInstanceOf[BIGAND[Invariant]], "BIGAND expected")
    assertResult(4)(conj_two_and.terms.length)
    }
  
  it should "be able to implicitly construct a conjunction of two BIGAND3s of cardinality six" in
    {
    val conj_two_bigand = BIGAND(TRUE(), OccupyPoint(7,7), FALSE()) ^ BIGAND(FALSE(), Component("Test2"), TRUE())
      
    println(conj_two_bigand)
    assert(conj_two_bigand.isInstanceOf[BIGAND[Invariant]], "BIGAND expected")
    assertResult(6)(conj_two_bigand.terms.length)
    }
  
  // Disjunction operators
  "The v operator" should "be able to implicitly construct a binary disjunction as an OR" in
    {
    val disj = TRUE() v OccupyPoint(2,2)

    println(disj)
    assert(disj.isInstanceOf[OR[Invariant]], "OR expected")
}
  
  it should "be able to implicitly construct a ternery disjunction as a BIGOR" in
    {
    val mid_disj = TRUE() v OccupyPoint(3,2) v FALSE()
    
    println(mid_disj)
    assert(mid_disj.isInstanceOf[BIGOR[Invariant]], "BIGOR expected")
    assertResult(3)(mid_disj.terms.length)
    }
  
  it should "be able to implicitly construct a quadernary disjunction as a BIGOR" in
    {
    val big_disj = TRUE() v OccupyPoint(4,2) v FALSE() v TRUE()
    
    println(big_disj)
    assert(big_disj.isInstanceOf[BIGOR[Invariant]], "BIGOR expected")
    assertResult(4)(big_disj.terms.length)
    }

    it should "be able to implicitly construct a disjunction of two ORs as a BIGOR of cardinality four" in
    {
    val disj_two_or = OR(TRUE(), OccupyPoint(7,7)) v OR(FALSE(), Component("Test"))
      
    println(disj_two_or)
    assert(disj_two_or.isInstanceOf[BIGOR[Invariant]], "BIGOR expected")
    assertResult(4)(disj_two_or.terms.length)
    }
  
  it should "be able to implicitly construct a disjunction of two BIGOR3s of cardinality six" in
    {
    val disj_two_bigor = BIGOR(TRUE(), OccupyPoint(7,7), FALSE()) v BIGOR(FALSE(), Component("Test2"), TRUE())
      
    println(disj_two_bigor)
    assert(disj_two_bigor.isInstanceOf[BIGOR[Invariant]], "BIGOR expected")
    assertResult(6)(disj_two_bigor.terms.length)
    }
  

}



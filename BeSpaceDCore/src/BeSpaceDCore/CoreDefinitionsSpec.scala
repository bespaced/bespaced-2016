/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDCore

import Log._

class CoreDefinitionsSpec extends UnitSpec {

  val core = standardDefinitions; import core._

  
  
  //========================================= Simplify

  // These are just some arbitrary complex invariants that mean nothing for testing purposes
  val anything = BIGOR(List(AND(OccupyNode(3), ComponentState("any")), IMPLIES(TimePoint(2345), OccupyPoint(1,2))))
  val something = BIGAND(List(IMPLIES(TimePoint(2345), ComponentState("any")), OR(OccupyNode(44), OccupyPoint(1,2))))
  
  
  // AND
  "SimplifyInvariant" should "reduce a BIGAND of TRUEs to TRUE" in
    {
      val Bigand = BIGAND(List(TRUE(), TRUE(), TRUE(), TRUE(), TRUE(), TRUE(), TRUE()))

      val result = simplifyInvariant(Bigand)

      assertResult(expected = TRUE())(actual = result )
    }
  
  it should "reduce a BIGAND with one FALSE to FALSE" in
    {
      val Bigand = BIGAND(List(TRUE(), TRUE(), TRUE(), TRUE(), FALSE(), TRUE(), TRUE()))

      val result = simplifyInvariant(Bigand)

      assertResult(expected = FALSE())(actual = result )
    }

  it should "reduce an AND with two TRUEs to TRUE" in
    {
      val And = AND(TRUE(), TRUE())

      val result = simplifyInvariant(And)

      assertResult(expected = TRUE())(actual = result )
    }

  it should "reduce an AND with one FALSE to FALSE" in
    {
      val And = AND(anything, FALSE())

      val result = simplifyInvariant(And)

      assertResult(expected = FALSE())(actual = result )
    }

  // OR
  it should "reduce a BIGOR of FALSEs to FALSE" in
    {
      val Bigor = BIGOR(List(FALSE(), FALSE(), FALSE(), FALSE(), FALSE(), FALSE(), FALSE()))

      val result = simplifyInvariant(Bigor)

      assertResult(expected = FALSE())(actual = result )
    }
  
  it should "reduce a BIGOR with one TRUE to TRUE" in
    {
      val Bigor = BIGOR(List(anything, FALSE(), TRUE(), anything, anything, FALSE(), FALSE()))

      val result = simplifyInvariant(Bigor)

      assertResult(expected = TRUE())(actual = result )
    }

  it should "reduce an OR with two FALSEs to FALSE" in
    {
      val Or = OR(FALSE(), FALSE())

      val result = simplifyInvariant(Or)

      assertResult(expected = FALSE())(actual = result )
    }

  it should "reduce an OR with one TRUE to TRUE" in
    {
      val Or = OR(anything, TRUE())

      val result = simplifyInvariant(Or)

      assertResult(expected = TRUE())(actual = result )
    }

  // IMPLIES
  it should "reduce an IMPLIES with a TRUE premise to the conclusion" in
    {
      val implication = IMPLIES(TRUE(), something)

      val result = simplifyInvariant(implication)

      assertResult(expected = simplifyInvariant(something))(actual = result )
    }

  it should "reduce an IMPLIES with a FALSE premise to TRUE" in
    {
      val implication = IMPLIES(FALSE(), anything)

      val result = simplifyInvariant(implication)

      assertResult(expected = TRUE())(actual = result )
    }

  
  
  // ====================================== UNFOLD
  
  //--------------------------------------- unfoldInvariant
  
  "unfoldInvariant" should "expand an OccupyBox into multiple points" in
  {
    val box = OccupyBox(100, 200, 104, 204)
    
    // Expecting 16 points : (100..104, 200..204)
    val xRange = (100 to 104).toList
    val yRange = (200 to 204).toList
    
    val expectedPoints: List[(Int,Int)] = for { x <- xRange; y <- yRange } yield (x, y)
    val expectedInvariants = expectedPoints map { t => OccupyPoint(t._1, t._2) }
    val expectedInvariant = BIGAND(expectedInvariants)
    
    def f = { unfoldInvariant(box) }
    val actualResult = timed[Invariant] ( f _ )
    
    val actual = order(simplifyInvariant(actualResult)).asInstanceOf[BIGAND[OccupyPoint]]
    val expected = order(expectedInvariant).asInstanceOf[BIGAND[OccupyPoint]]
    
    debugIf (actual != expected) {
      println( s"actual = ${pp(actual)}" )
      println( s"actual = ${actual.terms}" )
      println( s"expected = ${pp(expected)}" )
      println( s"expected = ${expected.terms}" )
      
      val compareList = actual.terms zip expected.terms map { p => p._1 == p._2 }
      println( s"compareList = $compareList" )
      
      println( actual.terms == expected.terms )
      assert( actual.terms == expected.terms )
      
      println( "actual == expected: " + actual == expected )
      println(s"actual.toString: "   + actual.toString)
      println(s"expected.toString: " + expected.toString)
      println(s"actual.compare(expected): " + actual.compare(expected))
      
      ensure( actual == expected, "Equality is not working for invariants of the BIGAND type." )
    }
    
    //assertResult(expected = order(expectedInvariant))(actual = order(simplifyInvariant(actualResult)))
    assertResult(expected)(actual)
  }


  "unfoldInvariant" should "expand conjunctions of OccupyBoxes into multiple points" in
  {
    fail("KNOWN FAILURE: unfoldInvariant does not deduplicate the union of points")
    
    val box1 = OccupyBox(100, 200, 104, 204)
    val box2 = OccupyBox(103, 203, 106, 206)
    
    // Expecting 16 points : (100..104, 200..204)
    // Plus 12 points: (105..106, 203..206) + (103..104, 205..206)
    val xRangeA = (100 to 104).toList
    val yRangeA = (200 to 204).toList
    val expectedPointsA: List[(Int,Int)] = for { x <- xRangeA; y <- yRangeA } yield (x, y)
    
    val xRangeB = (105 to 106).toList
    val yRangeB = (203 to 206).toList
    val expectedPointsB: List[(Int,Int)] = for { x <- xRangeB; y <- yRangeB } yield (x, y)
    
    val xRangeC = (103 to 104).toList
    val yRangeC = (205 to 206).toList
    val expectedPointsC: List[(Int,Int)] = for { x <- xRangeC; y <- yRangeC } yield (x, y)
    
    val expectedPoints = expectedPointsA ::: expectedPointsB ::: expectedPointsC
    val expectedInvariants = expectedPoints map { t => OccupyPoint(t._1, t._2) }
    val expectedInvariant = BIGAND(expectedInvariants)
    
    def f = { unfoldInvariant(AND(box1, box2)) }
    val actualResult = timed[Invariant] ( f _ )
    
    assertResult(expected = order(expectedInvariant))(actual = order(simplifyInvariant(actualResult)))
  }


  //--------------------------------------- unfoldInvariant 2

  "unfoldInvariant2" should "expand an OccupyBox into multiple points" in
  {
    val box = OccupyBox(100, 200, 104, 204)
    
    // Expecting 16 points : (100..104, 200..204)
    val xRange = (100 to 104).toList
    val yRange = (200 to 204).toList
    
    val expectedPoints: List[(Int,Int)] = for { x <- xRange; y <- yRange } yield (x, y)
    val expectedInvariants = expectedPoints map { t => OccupyPoint(t._1, t._2) }
    val expectedInvariant = BIGAND(expectedInvariants)
    
    def f = { unfoldInvariant2(box) }
    val actualResult = timed[Invariant] ( f _ )
    
    assertResult(expected = order(expectedInvariant))(actual = order(simplifyInvariant(actualResult)))
  }


  "unfoldInvariant2" should "expand conjunctions of OccupyBoxes into multiple points" in
  {
    val box1 = OccupyBox(100, 200, 104, 204)
    val box2 = OccupyBox(103, 203, 106, 206)
    
    // Expecting 16 points : (100..104, 200..204)
    // Plus 12 points: (105..106, 203..206) + (103..104, 205..206)
    val xRangeA = (100 to 104).toList
    val yRangeA = (200 to 204).toList
    val expectedPointsA: List[(Int,Int)] = for { x <- xRangeA; y <- yRangeA } yield (x, y)
    
    val xRangeB = (105 to 106).toList
    val yRangeB = (203 to 206).toList
    val expectedPointsB: List[(Int,Int)] = for { x <- xRangeB; y <- yRangeB } yield (x, y)
    
    val xRangeC = (103 to 104).toList
    val yRangeC = (205 to 206).toList
    val expectedPointsC: List[(Int,Int)] = for { x <- xRangeC; y <- yRangeC } yield (x, y)
    
    val expectedPoints = expectedPointsA ::: expectedPointsB ::: expectedPointsC
    val expectedInvariants = expectedPoints map { t => OccupyPoint(t._1, t._2) }
    val expectedInvariant = BIGAND(expectedInvariants)
    
    def f = { unfoldInvariant2(AND(box1, box2)) }
    val actualResult = timed[Invariant] ( f _ )
    
    assertResult(expected = order(expectedInvariant))(actual = order(simplifyInvariant(actualResult)))
  }
  
  
  
  // ====================================== COLLISION

    val box10 = OccupyBox(10, 10, 20, 20)
    val box20 = OccupyBox(20, 20, 30, 30)
    val box21 = OccupyBox(21, 21, 30, 30)
    val box15 = OccupyBox(15, 15, 25, 25)
    val box21a = OccupyBox(21, 10, 30, 20)

    val box100 = OccupyBox(100, 100, 200, 200)
    val box200 = OccupyBox(200, 200, 300, 300)
    val box201 = OccupyBox(201, 201, 300, 300)
    val box150 = OccupyBox(150, 150, 250, 250)
    val box201a = OccupyBox(201, 100, 300, 200)
    
    type ObjectList = List[Invariant]
    type Fun = (ObjectList, ObjectList) => Boolean
    
    @inline
    def testCollisionTests(list1: ObjectList, list2: ObjectList, expectValue: Boolean)(f: Fun)
    {
      val actualResult = f(list1, list2)
    
      assertResult(expected = expectValue)(actual = actualResult)
    }
    
    val testSmallBoxSingleCommonPoint =        testCollisionTests(box10::Nil, box20::Nil,   true) _
    val testSmallBoxNoCommonPointCornerTouch = testCollisionTests(box10::Nil, box21::Nil,  false) _
    val testSmallBoxNoCommonPointEdgeTouch =   testCollisionTests(box10::Nil, box21a::Nil, false) _

    val testLargeBoxSingleCommonPoint =        testCollisionTests(box100::Nil, box200::Nil,   true) _
    val testLargeBoxNoCommonPointCornerTouch = testCollisionTests(box100::Nil, box201::Nil,  false) _
    val testLargeBoxNoCommonPointEdgeTouch =   testCollisionTests(box100::Nil, box201a::Nil, false) _


  //--------------------------------------- collisionTests
   
 
  
  
  
  //--------------------------------------- collisionTestsAlt1
  
  
  "collisionTestsAlt1" should "detect collision between two small boxes with single point in common" in
  {
    fail("KNOWN FAILURE: This is because encodeAsSat does not handle OccupyBox.")
    
    testSmallBoxSingleCommonPoint(collisionTestsAlt1(debug = true))
  }
  
  it should "detect no collision between two small boxes with no point in common - corners touching" in
  {
    testLargeBoxNoCommonPointCornerTouch(collisionTestsAlt1)
  }
  
  it should "detect no collision between two small boxes with no point in common - vertical edge touching" in
  {
    testLargeBoxNoCommonPointEdgeTouch(collisionTestsAlt1)
  }
  
  
  it should "detect collision between two large boxes with single point in common" in
  {
    fail("KNOWN FAILURE: This is because encodeAsSat does not handle OccupyBox.")
    
    testLargeBoxSingleCommonPoint(collisionTestsAlt1)
  }
  
  it should "detect no collision between two large boxes with no point in common - corners touching" in
  {
    testLargeBoxNoCommonPointCornerTouch(collisionTestsAlt1)
  }
  
  it should "detect no collision between two large boxes with no point in common - vertical edge touching" in
  {
    testLargeBoxNoCommonPointEdgeTouch(collisionTestsAlt1)
  }
  
  
  
  
  //--------------------------------------- collisionTestsBig
  
  // Note: You need a VM argument of -Xss8m to prevent stack overflow.
  
  "collisionTestsBig" should "detect collision between two small boxes with single point in common" in
  {
    testSmallBoxSingleCommonPoint(collisionTestsBig)
  }
  
  it should "detect no collision between two small boxes with no point in common - corners touching" in
  {
    testLargeBoxNoCommonPointCornerTouch(collisionTestsBig)
  }
  
  it should "detect no collision between two small boxes with no point in common - vertical edge touching" in
  {
    testLargeBoxNoCommonPointEdgeTouch(collisionTestsBig)
  }

  it should "detect collision between two large boxes with single point in common" in
  {
    testLargeBoxSingleCommonPoint(collisionTestsBig)
  }
  
  it should "detect no collision between two large boxes with no point in common - corners touching" in
  {
    testLargeBoxNoCommonPointCornerTouch(collisionTestsBig)
  }
  
  it should "detect no collision between two large boxes with no point in common - vertical edge touching" in
  {
    testLargeBoxNoCommonPointEdgeTouch(collisionTestsBig)
  }
  
  
  
  // ====================================== INCLUSION

  //--------------------------------------- inclusionBox

  "inclusionBox" should "detect a simple included box" in
  {
   val superBox = OccupyBox(1, 1, 10, 10)
   val subBox = OccupyBox(3, 3, 7, 7)
   
   val expectedResult = true
   val actualResult = inclusionBox(subBox, superBox)
   
   assertResult(expected = expectedResult)(actual = actualResult)
  }

  it should "detect overlapping boxes" in
  {
   val superBox = OccupyBox(1, 1, 10, 10)
   val subBox = OccupyBox(7, 7, 15, 15)
   
   val expectedResult = false
   val actualResult = inclusionBox(subBox, superBox)
   
   assertResult(expected = expectedResult)(actual = actualResult)
  }

  it should "detect a non-overlapping boxes" in
  {
   val superBox = OccupyBox(1, 1, 10, 10)
   val subBox = OccupyBox(11, 7, 15, 15)
   
   val expectedResult = false
   val actualResult = inclusionBox(subBox, superBox)
   
   assertResult(expected = expectedResult)(actual = actualResult)
  }

  // KF: I'm happy this this trivial method works
  // KF: I intend to use it to validate more complex inclusions for testing inclusion tests below.
  
  
  
  //--------------------------------------- inclusionTestsBig
  
  "inclusionTestsBig" should "detect a simple included box" in
  {
   val superBox = OccupyBox(1, 1, 10, 10)
   val subBox = OccupyBox(3, 3, 7, 7)
   
   val expectedResult = true
   val actualResult = inclusionTestsBig(subBox::Nil, superBox::Nil)
   
   assertResult(expected = expectedResult)(actual = actualResult)
  }

  it should "detect overlapping boxes" in
  {
   val superBox = OccupyBox(1, 1, 10, 10)
   val subBox = OccupyBox(7, 7, 15, 15)
   
   val expectedResult = false
   val actualResult = inclusionTestsBig(subBox::Nil, superBox::Nil)
   
   assertResult(expected = expectedResult)(actual = actualResult)
  }

  it should "detect a non-overlapping boxes" in
  {
   val superBox = OccupyBox(1, 1, 10, 10)
   val subBox = OccupyBox(11, 7, 15, 15)
   
   val expectedResult = false
   val actualResult = inclusionTestsBig(subBox::Nil, superBox::Nil)
   
   assertResult(expected = expectedResult)(actual = actualResult)
  }

  
  
  
  
  


}
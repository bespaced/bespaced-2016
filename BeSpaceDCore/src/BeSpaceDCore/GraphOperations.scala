/*
 * J O Blech 2015
 */

package BeSpaceDCore

class GraphOperations {

 
 // added 12/02/2015 not yet tested
  
  def tuplelist2Invariant [A] (l : List[(A,A)]) : Invariant ={
      var invl : List[Invariant] = Nil   
      for ((n1,n2) <-l) {
        invl ::= Edge(n1,n2)
      }  
      return (BIGAND(invl))
  }

  //added 18/02/2015, testing on 25/02/2015
  
    def invariant2Tuplelist (inv : Invariant) : List[(Any,Any)] ={
      inv match {
        case BIGAND(a::restl) => invariant2Tuplelist(a)++invariant2Tuplelist(BIGAND(restl))
        case IMPLIES(_,inv) => invariant2Tuplelist(inv) //JO: this may be controversial
        case Edge(a,b) => (a,b)::Nil
        case _ => Nil
      }
  }
    
    
  //added 20/02/2015, first testing and correction on 25/02/2015, testing and bugfix: 27/02/2015
    
  def transitiveHull [A] (l : List[(A,A)]) : Set[(A,A)] ={
    
    var changed : Boolean = true  
    var retset : Set[(A,A)]= l.toSet // should create a new list, since list objects are immutable in Scala
    
    
    while (changed) {
        changed = false
        for ((x,y) <- l) {
          for ((x2,y2) <- retset) { //this may be implementation dependend, take a close look, seems to work with some more sophisticated examples
              if (y.equals(x2)) {
                if (! retset.contains((x,y2))) {
                  changed = true
                  retset += ((x,y2)) // try this out
                }
                  
              } 
          }
          
        }
    }
    
    return retset
  }
  
}
/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap and Vasileios Dimitrakopoulos 2016
 */

package view;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

import model.ArrayUtilities;
import model.FrameProcessor;

/**
 * Observer Design Pattern.
 */
@SuppressWarnings("serial")
public class DepthPanel extends AbstractDisplayPanel implements Observer {

    // Official Kinect 2 range, in millimetres. Change as needed
    private int DEFAULT_MIN_DRAW_DEPTH = 50;
    private static int DEFAULT_MAX_DRAW_DEPTH = 4500;

    /**
     * Draws a depth image on the window.
     * @param z The depth values of each pixel.
     * @param width The width of the image.
     * @param height The height of the image.
     * @param maxZ The maximum distance to draw to.
     */
    public void drawArea(int[] z, int width, int height, int maxZ, boolean color) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        /* Draw a depth image with color. */
        if(color) {
            FrameProcessor processor = FrameProcessor.getInstance();
            int[] colors = processor.getDepthToColorMap();

            for (int y = 0, i = 0; y < height; y++) {
                for (int x = 0; x < width; x++, i++) {
                    img.setRGB(x, y, colors[i]);
                }
            }
        }
        /* Draw a simple black/white depth image. */
        else {
            int black = 0x000000;
            int white = 0xFFFFFF;

            for (int y = 0, i = 0; y < height; y++) {
                for (int x = 0; x < width; x++, i++) {
                    if (z[i] >= DEFAULT_MIN_DRAW_DEPTH && z[i] <= maxZ) {
                        img.setRGB(x, y, black);
                    } else {
                        img.setRGB(x, y, white);
                    }
                }
            }
        }
        drawer = getGraphics();
        drawer.drawImage(img, 0, 0, width, height, Color.WHITE, null);
        drawer.dispose();
    }
    
    

    /**
     * Updates the window's content.
     */
    @Override
    public void update(Observable o, Object arg) {
        FrameProcessor processor = FrameProcessor.getInstance();
        removeAll();
        /* Update whole depth image. */
        drawArea(ArrayUtilities.floatsToInts(processor.getDepthMap().realZ, 3),
                App.DEPTH_WIDTH, App.DEPTH_HEIGHT, DEFAULT_MAX_DRAW_DEPTH, false);
    }
}
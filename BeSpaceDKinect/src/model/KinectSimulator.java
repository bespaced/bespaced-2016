/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 2016
 */

package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;



import BeSpaceDCore.*;
import model.ImportExport$.*;



public
class KinectSimulator {
	
	public static void main(String[] args)
	{
		//KinectSimulator app = new KinectSimulator();
		KinectScan data = KinectSimulator.loadSimulatedKinectData("res/data/");
		
		System.out.println("data = " + data);
		System.out.println("   #data.xyz        = " + data.xyz().length);
		System.out.println("   #data.uv         = " + data.uv().length);
		System.out.println("   #data.color_data = " + data.color_data().length);
		
		// Convert data to BeSpaceD Invariant
		
		TimePoint<Date> now = new TimePoint<Date>(new Date());
		
//		// XYZ Values
//		int[] xyzValues = ImportExport$.MODULE$.floatsToInts(data.xyz(), 0);
//		Invariant xyzInvariant = ImportExport$.MODULE$.import3DSpaceTime(xyzValues, now);
//		
//		// UV Values
//		int[] uvValues = ImportExport$.MODULE$.floatsToInts(data.uv(), 0);
//		Invariant uvInvariant = ImportExport$.MODULE$.importUVSpaceTime(uvValues, now);
//		
//		// Color Values
//		int[] colorValues = ImportExport$.MODULE$.floatsToInts(data.uv(), 0);
//		Invariant colorInvariant = ImportExport$.MODULE$.importColorSpaceTime(uvValues, now);
		
		
		
		// ================================================================== SCAN: Festo Bottle
		
		// Read All Values from CSV files
//		Invariant invariant = ImportExport$.MODULE$.importKinectScanTime(data, now);
		
		// Create data set from
//		BeSpaceDData.package$.MODULE$.save(invariant, "aicause.kinect.scan.bottle");
		
		// TODO: Make resuable constants out of these data set IDs
		
		// TEST CSV Writing by writing the bottle scan data to a file
//		floatArrToCsv(data.xyz(), 3, "res/data/xyz.csv");
//		floatArrToCsv(data.uv(), 2, "res/data/uv.csv");
//		byteArrToCsv(data.color_data(), 3, "res/data/color.csv");
		
		
		// ================================================================== SCAN: Obstacles on Floor 1
		
		floatArrToCsv(data.xyz(), 3, "res/data/obstacles1/xyz.csv");
		floatArrToCsv(data.uv(), 2, "res/data/obstacles1/uv.csv");
		byteArrToCsv(data.color_data(), 3, "res/data/obstacles1/color.csv");
		
	}

	// TODO change design to accept string instead of using hardcoded values
	public static final String XYZ = "xyz.txt";
	public static final String UV = "uv.txt";
	public static final String COLOR = "color.txt";
	
	
	/**
	 * Simulates a real Kinect sensor.
	 * @param dataBasepath The location of the sample files on the disk.
	 * @return Weather the simulation started successfully.
	 */
	public static boolean simulateKinect(String dataBasepath) {
		float[] xyz = csvToFloatArr(dataBasepath + XYZ);
		float[] uv = csvToFloatArr(dataBasepath + UV);
		float[] temp = csvToFloatArr(dataBasepath + COLOR);  // This takes a while, look into optimisation of method
		byte[] color_data = floatArrToByteArr(temp);
		
		if (xyz == null || uv == null || color_data == null ) {
			
			return false;
		}
		
//        KinectSensor.getInstance().onColorFrameEvent(color_data);
//        KinectSensor.getInstance().onDepthFrameEvent(null, null, xyz, uv);
		
		return true;
	}
	

	/**
	 * Reads in the simulated Kinect data.
	 * @param dataBasepath The location of the sample files on the disk.
	 * @return The data.
	 */
	public static KinectScan loadSimulatedKinectData(String dataBasepath)
	{
		float[] xyz = csvToFloatArr(dataBasepath + XYZ);
		float[] uv = csvToFloatArr(dataBasepath + UV);
		float[] temp = csvToFloatArr(dataBasepath + COLOR);  // This takes a while, look into optimisation of method
		byte[] color_data = floatArrToByteArr(temp);
		
		if (xyz == null || uv == null || color_data == null )
		{	
			return null;
		}
		
		// Aggregate data
		KinectScan result = new KinectScan(xyz, uv, temp, color_data);
		
		return result;
	}
	
	
	/**
	 * Converts a CSV to a float array.
	 * @param path The location of the CSV array on the disk.
	 * @return The new float array.
	 */
	public static float[] csvToFloatArr(String path) {
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(new FileReader(path));
			List<String> lines = new ArrayList<String>();
			String line;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
			reader.close();

			int lineLen = lines.get(0).split(",").length;
			float[] data = new float[lines.size() * lineLen];

			int counter = 0;

			for (String l : lines) {
				String[] vals = l.split(",");
				for (String v : vals) {
					data[counter] = Float.parseFloat(v);
					counter++;
				}
			}

			return data;
		} 
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			return null;
		}
	}
	
	
	/**
	 * Writes a float array to a CSV file.
	 * @param data The array of floats.
	 * @param columnCount The number of columns in the data.
	 * @param path The location of the CSV file on the disk.
	 */
	public static void floatArrToCsv(float[] data, int columnCount, String path)
	{
		BufferedWriter writer;
		
		try
		{
			writer = new BufferedWriter(new FileWriter(path));
			
			int column = 1;
			
			for (float f: data)
			{
				// Output the float with a trailing comma
				writer.write(f + ",");
				
				// Start a new line after last column
				if (column == columnCount) writer.newLine();
				
				// Column rotates between 1,2,3,  1,2,3 (e.g. columnCount = 3)
				if (column == columnCount) column = 1; else column++;
			}
						
			writer.flush();
			writer.close();

			return;
		} 
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			return;
		}
	}
	
	
	/**
	 * Writes a byte array to a CSV file.
	 * @param data The array of bytes.
	 * @param columnCount The number of columns in the data.
	 * @param path The location of the CSV file on the disk.
	 */
	public static void byteArrToCsv(byte[] data, int columnCount, String path)
	{
		BufferedWriter writer;
		
		try
		{
			writer = new BufferedWriter(new FileWriter(path));
			
			int column = 1;
			
			for (byte b: data)
			{
				// Output the float with a trailing comma
				writer.write(b + ",");
				
				// Start a new line after last column
				if (column == columnCount) writer.newLine();
				
				// Column rotates between 1,2,3,  1,2,3 (e.g. columnCount = 3)
				if (column == columnCount) column = 1; else column++;
			}
						
			writer.flush();
			writer.close();

			return;
		} 
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			return;
		}
	}
	
	
	/**
	 * Converts a float array to a byte array.
	 * @param floats The float array to be converted.
	 * @return The new byte array.
	 */
	public static byte[] floatArrToByteArr(float[] floats) {
		if (floats == null ) return null;
		
		ByteBuffer buf = ByteBuffer.allocate(floats.length);

		for (int i = 0; i < floats.length; ++i) {
			buf.put((byte)(floats[i]));
		}
		
		return buf.array();
	}
	
	
	/**
	 * Converts a byte array to a float array.
	 * @param bytes The byte array to be converted.
	 * @return The new float array.
	 */
	public static float[] byteArrToFloatArr(byte[] bytes)
	{
		if (bytes == null ) return null;
		
		float[] buf = new float[(bytes.length)];

		for (int i = 0; i < bytes.length; ++i)
		{
			buf[i] = (float)bytes[i];
		}
		
		return buf;
	}
}
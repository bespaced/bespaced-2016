package model

import java.io.DataInputStream

import BeSpaceDCore._
import BeSpaceDCore.PointCloud._
import java.io._



object PointCloud {
  
  case class KinectPointCloud(kinectScan: Array[Float]) extends PointCloud[Occupy3DPointFloat]
  {
    override def contains(p: Point3D): Boolean = if (p.isInstanceOf[Occupy3DPointFloat]) kinectScanContains(p.asInstanceOf[Occupy3DPointFloat]) else false
    
    lazy val toList = makeListFromKinectScan(kinectScan)
    
    
    // PRIVATE
    
    private val componentLength =kinectScan.length
    private val pointLength = componentLength / 3

    private
    def kinectScanContains(p: Occupy3DPointFloat): Boolean =
    {
      for(i <- 0 to (pointLength - 1); x = kinectScan(i*3); y = kinectScan(i*3 + 1); z = kinectScan(i*3 + 2))
      {
        if (x == p.x && y == p.y && z == p.z) return true
      }
      
      return false
    }
    
    private
    def makeListFromKinectScan(kinectScan: Array[Float]): List[Occupy3DPointFloat] =
    {      
      val result =
        for(i <- (pointLength - 1) to 0; x = kinectScan(i*3); y = kinectScan(i*3 + 1); z = kinectScan(i*3 + 2)) yield Occupy3DPointFloat(x, y, z)
      
      return result.toList
    }
  }
  
  
  case class KinectPointCloudSource(kinectScan: Array[Float]) extends PointCloudSource[Occupy3DPointFloat]
  {
    // IDEA is no storage of cloud in memory transfer from float array to input stream efficiently
    
    override def inputStream: InputStream =
    {
      val buffOutStream = new ByteArrayOutputStream();
      val streamWriter  = new ObjectOutputStream(buffOutStream);
      
      while(iterator.hasNext){
         streamWriter.writeObject(iterator.next)
      }
      
      streamWriter.flush()
      streamWriter.close()
      
      return new ByteArrayInputStream(buffOutStream.toByteArray());
    }
    
    // Implemented as a different function to compare efficiency.
    def inputStreamJson: InputStream = 
    {
      val buffOutStream = new ByteArrayOutputStream();
      val streamWriter  = new ObjectOutputStream(buffOutStream);
      
      while(iterator.hasNext){
        streamWriter.writeBytes(toJson(iterator.next))
      }
      
      streamWriter.flush()
      streamWriter.close()
      
      return new ByteArrayInputStream(buffOutStream.toByteArray());
    }
    // Iterate through the Depth array and pull out all xyz values.
    // write the three floats into the stream in XYZ order.
    
    override def iterator: Iterator[Occupy3DPointFloat] = KinectIterator
    
    override def toPointCloud = KinectPointCloud(kinectScan)
    
    // PRIVATE

    private
    object KinectIterator extends Iterator[Occupy3DPointFloat]
      {
        val componentLength = kinectScan.length 
        require(componentLength % 3 == 0, "length must be a factor of three")
        val pointLength = kinectScan.length / 3
        
        // Index
        val sentinel = pointLength
        var current = 0
        
        def hasNext: Boolean = (current < sentinel)
        
        def next(): Occupy3DPointFloat =
        {
          val x = kinectScan(current * 3)
          val y = kinectScan(current * 3 + 1)
          val z = kinectScan(current * 3 + 2)
          
          current += 1
          
          Occupy3DPointFloat(x, y, z)
        }
      }
          
  }

   
  
  // --------------------------------------------------------------------------------- Sample Code
  
 
  @deprecated
  object KinectExmple1
  {
    // import BeSpaceDCore.PointCloud._
    
    val myBigand = BIGAND(TRUE(), IMPLIES(TRUE(), FALSE()),  Occupy3DPointFloat(1,2,3))
    val myBigand2 = TRUE() ==> FALSE() ^ TRUE() ==> FALSE() ^  TRUE() ==> FALSE()
    
    // Read floats from Kinect raw data arrays.
    
    // Create your points
    val p1 = Occupy3DPointFloat(1.6F, 2.1F, 3.7F)
    val p2 = Occupy3DPointFloat(1.5F, 2.0F, 3.23F)
    
    // Normally iterate through many
    val builder = HashPointCloudTarget()
    
    // Inside the loop add each point
    builder.add(p1)
    builder.add(p2)
    
    // Outside the loop - lock it down - so it can't be modified
    val cloud: PointCloud[Occupy3DPointFloat] = builder.finish()
    
    // Get a nice Java input stream
    // DOES NOT WORK AFTER REFACTOR
    //val is: DataInputStream = builder.inputStream
    
    // And give it to CloudStor
    ???
  }
  
  object KinectExmple2
  {
    // import BeSpaceDCore.PointCloud._
        
    // Get Kinect raw data arrays. It contains all three ZXY values alternating in sequence.
    val kinectScan: Array[Float] = Array(1,1,1, 2,2,2, 3,3,3)
    
    // Create a point cloud source
    val source = KinectPointCloudSource(kinectScan)
        
    // Get a nice Java input stream
    val is: InputStream = source.inputStream
    
    // And give it to CloudStor
    // You can call is.getFloat()
    ???
  }
  
  def readMyPointCloud(): Invariant = 
  {
    println("Hi, I'm in Scala code here!")
    println("I hope I'm inside a reactive block, yes?")
    
    TRUE()
  }

}